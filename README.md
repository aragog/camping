## Projet M1 GIL ##

### Camping domestique en ligne ###

Développement d'un site de mise en relation de particuliers sur le thème du camping.

Certaines personnes proposent une partie de leur terrain et divers services, d'autres recherchent une zone où camper pour une ou plusieurs nuits.

Le but de ce site est de favoriser le rapprochement de ces deux publics.

#### Rôles ####

Nous trouverons quatre "types" de personnes sur le site :

* __Les accueillants__: ces derniers doivent posséder un compte. Une fois connecté,
un accueillant propose un ou plusieurs lieux qu'ils mettent à disposition des
campeurs.

* __Les visiteurs__: les visiteurs accèdent à toutes les offres de locations disponibles. Une fois un choix retenu, ils prennent directement contact avec l'accueillant.

* __Les administrateurs__: ils ont à leur disposition tout un panel d'outils afin de
vérifier la validité des offres de locations et d'encarts publicitaires.
Enfin, ils sont à même de bannir un accueillant du site.

* __Les annonceurs__: les annonceurs devront s'inscrire sur le site afin de se
connecter et ainsi proposer une annonce publicitaire.
Le règlement se fait hors site. 

#### Liens utiles ####

* [Site officiel](http://git-scm.com/ "http://git-scm.com/") de Git
* [Essayer](http://try.github.io/levels/1/challenges/1 "http://try.github.io/levels/1/challenges/1") Git en ligne (tutoriel-jeu très didactique et pragmatique)
* [Documentation](http://doc.ubuntu-fr.org/git "http://doc.ubuntu-fr.org/git") Ubuntu de Git
