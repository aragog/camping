\select@language {french}
\contentsline {chapter}{\numberline {1}Pr\IeC {\'e}sentation}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{3}{section.1.1}
\contentsline {paragraph}{}{3}{section*.2}
\contentsline {section}{\numberline {1.2}Pr\IeC {\'e}sentation du groupe de travail}{3}{section.1.2}
\contentsline {paragraph}{}{3}{section*.3}
\contentsline {paragraph}{}{3}{section*.4}
\contentsline {subsection}{\numberline {1.2.1}Chef de projet : Simon Duclos}{3}{subsection.1.2.1}
\contentsline {paragraph}{}{3}{section*.6}
\contentsline {paragraph}{}{3}{section*.8}
\contentsline {paragraph}{}{4}{section*.9}
\contentsline {paragraph}{}{4}{section*.10}
\contentsline {paragraph}{}{4}{section*.12}
\contentsline {subsection}{\numberline {1.2.2}Responsable technique : Jean-Charles Legras assist\IeC {\'e} par Sabrina Sahli}{4}{subsection.1.2.2}
\contentsline {paragraph}{}{4}{section*.14}
\contentsline {paragraph}{}{4}{section*.16}
\contentsline {subsubsection}{Conseils}{4}{section*.17}
\contentsline {paragraph}{}{4}{section*.18}
\contentsline {subsection}{\numberline {1.2.3}Responsable ma\IeC {\^\i }trise d'ouvrage : Aimad Takhtoukh}{4}{subsection.1.2.3}
\contentsline {paragraph}{}{4}{section*.20}
\contentsline {paragraph}{}{5}{section*.22}
\contentsline {subsection}{\numberline {1.2.4}Responsable des tests : Edwin Oursel}{5}{subsection.1.2.4}
\contentsline {paragraph}{}{5}{section*.24}
\contentsline {paragraph}{}{5}{section*.26}
\contentsline {paragraph}{}{5}{section*.28}
\contentsline {section}{\numberline {1.3}Rappel de la demande}{5}{section.1.3}
\contentsline {paragraph}{}{5}{section*.29}
\contentsline {paragraph}{}{5}{section*.30}
\contentsline {paragraph}{}{5}{section*.31}
\contentsline {paragraph}{}{5}{section*.32}
\contentsline {paragraph}{}{5}{section*.33}
\contentsline {subsection}{\numberline {1.3.1}R\IeC {\^o}les}{6}{subsection.1.3.1}
\contentsline {paragraph}{}{6}{section*.34}
\contentsline {paragraph}{}{6}{section*.35}
\contentsline {paragraph}{}{6}{section*.36}
\contentsline {paragraph}{}{6}{section*.37}
\contentsline {paragraph}{}{6}{section*.38}
\contentsline {section}{\numberline {1.4}Travail effectu\IeC {\'e}}{6}{section.1.4}
\contentsline {paragraph}{}{6}{section*.39}
\contentsline {paragraph}{}{6}{section*.40}
\contentsline {subsection}{\numberline {1.4.1}Liste des fonctionnalit\IeC {\'e}s}{6}{subsection.1.4.1}
\contentsline {paragraph}{}{6}{section*.41}
\contentsline {paragraph}{}{6}{section*.42}
\contentsline {paragraph}{}{6}{section*.43}
\contentsline {paragraph}{}{6}{section*.44}
\contentsline {paragraph}{}{7}{section*.45}
\contentsline {paragraph}{}{7}{section*.46}
\contentsline {paragraph}{}{7}{section*.47}
\contentsline {paragraph}{}{7}{section*.48}
\contentsline {paragraph}{}{7}{section*.49}
\contentsline {paragraph}{}{7}{section*.50}
\contentsline {paragraph}{}{7}{section*.51}
\contentsline {subsection}{\numberline {1.4.2}M\IeC {\'e}triques du projet}{7}{subsection.1.4.2}
\contentsline {paragraph}{}{7}{section*.52}
\contentsline {paragraph}{}{8}{section*.53}
\contentsline {subsection}{\numberline {1.4.3}Technologies employ\IeC {\'e}es}{8}{subsection.1.4.3}
\contentsline {subsubsection}{Projet}{8}{section*.54}
\contentsline {paragraph}{}{8}{section*.55}
\contentsline {paragraph}{}{8}{section*.56}
\contentsline {paragraph}{}{8}{section*.57}
\contentsline {paragraph}{}{8}{section*.58}
\contentsline {subsubsection}{Organisation}{8}{section*.59}
\contentsline {paragraph}{}{8}{section*.60}
\contentsline {paragraph}{}{8}{section*.61}
\contentsline {paragraph}{}{8}{section*.62}
\contentsline {subsection}{\numberline {1.4.4}Justifications techniques}{8}{subsection.1.4.4}
\contentsline {paragraph}{}{8}{section*.63}
\contentsline {paragraph}{}{8}{section*.64}
\contentsline {paragraph}{}{9}{section*.65}
\contentsline {section}{\numberline {1.5}Probl\IeC {\`e}mes rencontr\IeC {\'e}s}{9}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Probl\IeC {\`e}mes logiciels}{9}{subsection.1.5.1}
\contentsline {paragraph}{}{9}{section*.66}
\contentsline {paragraph}{}{9}{section*.67}
\contentsline {paragraph}{}{9}{section*.68}
\contentsline {paragraph}{}{9}{section*.69}
\contentsline {paragraph}{}{9}{section*.70}
\contentsline {subsection}{\numberline {1.5.2}Probl\IeC {\`e}mes d'organisation}{9}{subsection.1.5.2}
\contentsline {paragraph}{}{9}{section*.71}
\contentsline {paragraph}{}{9}{section*.72}
\contentsline {paragraph}{}{9}{section*.73}
\contentsline {paragraph}{}{10}{section*.74}
\contentsline {paragraph}{}{10}{section*.75}
\contentsline {section}{\numberline {1.6}Synth\IeC {\`e}se de la r\IeC {\'e}partition des t\IeC {\^a}ches}{10}{section.1.6}
\contentsline {paragraph}{}{10}{section*.76}
\contentsline {section}{\numberline {1.7}Conclusion}{10}{section.1.7}
\contentsline {paragraph}{}{10}{section*.77}
\contentsline {paragraph}{}{10}{section*.78}
\contentsline {paragraph}{}{10}{section*.79}
\contentsline {paragraph}{}{10}{section*.80}
\contentsline {paragraph}{}{10}{section*.81}
