title Répondre à un questionnaire

Client mail du visiteur->Vue: suivre le lien
Vue->Vue: remplir le questionnaire
Vue->QuestionnaireCtrl: soumettre(questionnaire)
opt questionnaire non déjà rempli
 QuestionnaireCtrl->AvisQuestionnaireController: update(questionnaire)
 AvisQuestionnaireController->AvisQuestionnaireService: update(questionnaire)
 AvisQuestionnaireService->AvisQuestionnaireDAO: update(questionnaire)
 AvisQuestionnaireDAO->Session: update(questionnaire)
 alt erreur
   QuestionnaireCtrl->Vue: afficher un message d'erreur
 else
   QuestionnaireCtrl->Vue: afficher un message de confirmation
 end
end