title Bannissement d'un accueillant par un administrateur

Vue->BannirAccueillantController: bannir(accueillant)
BannirAccueillantController->AccueillantController: update(accueillant)
AccueillantController->AccueillantService: update(accueillant)
AccueillantService->AccueillantDAO: update(accueillant)
AccueillantDAO->Session: update(accueillant)

alt MAJ réussie
 BannirAccueillantController->Mail: send(email)
 Mail->MailSender: sendMessage(email)
 BannirAccueillantController->Vue: message de confirmation
else
 BannirAccueillantController->Vue: message d'erreur
end
