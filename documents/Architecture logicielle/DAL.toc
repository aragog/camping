\select@language {french}
\contentsline {section}{\numberline {1}Objet}{4}{section.1}
\contentsline {section}{\numberline {2}Documents applicables et de r\IeC {\'e}f\IeC {\'e}rence}{4}{section.2}
\contentsline {section}{\numberline {3}Terminologie et sigles utilis\IeC {\'e}s}{4}{section.3}
\contentsline {section}{\numberline {4}Configuration requise}{5}{section.4}
\contentsline {section}{\numberline {5}D\IeC {\'e}ploiement}{5}{section.5}
\contentsline {section}{\numberline {6}Architecture statique}{6}{section.6}
\contentsline {subsection}{\numberline {6.1}Structure}{6}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Explications}{7}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Description du constituant Vue}{7}{subsection.6.3}
\contentsline {paragraph}{Description}{7}{section*.3}
\contentsline {subsubsection}{\numberline {6.3.1}Proc\IeC {\'e}d\IeC {\'e} de d\IeC {\'e}veloppement}{7}{subsubsection.6.3.1}
\contentsline {paragraph}{Langages de programmation}{7}{section*.4}
\contentsline {subsection}{\numberline {6.4}Description du constituant Contr\IeC {\^o}leur}{7}{subsection.6.4}
\contentsline {paragraph}{Description}{7}{section*.5}
\contentsline {subsubsection}{\numberline {6.4.1}Proc\IeC {\'e}d\IeC {\'e} de d\IeC {\'e}veloppement}{7}{subsubsection.6.4.1}
\contentsline {paragraph}{Langage de programmation}{7}{section*.6}
\contentsline {paragraph}{Outil}{8}{section*.7}
\contentsline {subsection}{\numberline {6.5}Description du constituant Mod\IeC {\`e}le}{9}{subsection.6.5}
\contentsline {paragraph}{Description}{9}{section*.8}
\contentsline {subsubsection}{\numberline {6.5.1}Description des entit\IeC {\'e}s}{9}{subsubsection.6.5.1}
\contentsline {paragraph}{}{9}{section*.11}
\contentsline {subsubsection}{\numberline {6.5.2}Proc\IeC {\'e}d\IeC {\'e} de d\IeC {\'e}veloppement}{12}{subsubsection.6.5.2}
\contentsline {subsection}{\numberline {6.6}Justifications techniques}{12}{subsection.6.6}
\contentsline {section}{\numberline {7}La g\IeC {\'e}olocalisation}{13}{section.7}
\contentsline {subsection}{\numberline {7.1}Description}{13}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Proc\IeC {\'e}d\IeC {\'e}}{13}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}API}{13}{subsection.7.3}
\contentsline {section}{\numberline {8}Planning d'une offre de location}{14}{section.8}
\contentsline {subsection}{\numberline {8.1}Description}{14}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}API}{14}{subsection.8.2}
\contentsline {section}{\numberline {9}Fonctionnement dynamique}{15}{section.9}
\contentsline {subsection}{\numberline {9.1}D\IeC {\'e}poser une nouvelle offre de location}{15}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Filtrer une offre de location}{16}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}R\IeC {\'e}pondre \IeC {\`a} un questionnaire}{17}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}D\IeC {\'e}poser une nouvelle annonce publicitaire}{18}{subsection.9.4}
\contentsline {subsection}{\numberline {9.5}Examiner une offre de location en attente de validation}{19}{subsection.9.5}
\contentsline {subsection}{\numberline {9.6}Examiner une annonce publicitaire en attente de validation}{20}{subsection.9.6}
\contentsline {subsection}{\numberline {9.7}D\IeC {\'e}p\IeC {\^o}t d'un avis informel}{21}{subsection.9.7}
\contentsline {subsection}{\numberline {9.8}Bannissement d'un accueillant par un administrateur}{22}{subsection.9.8}
\contentsline {subsection}{\numberline {9.9}Afficher les locations sur une carte de France}{23}{subsection.9.9}
\contentsline {subsection}{\numberline {9.10}Utiliser le planning d'une offre de location}{24}{subsection.9.10}
\contentsline {subsection}{\numberline {9.11}Cr\IeC {\'e}er une session pour un accueillant}{25}{subsection.9.11}
