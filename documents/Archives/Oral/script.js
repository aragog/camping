$(function () {
        $('#container515').highcharts({
            title: {
                text: 'Mots de longueur comprise entre 5 et 15',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                title: {
                    text: 'Taille de l\'alphabet'
                },
                categories: ['2', '4', '20', '70']
            },
            yAxis: {
                title: {
                    text: 'Temps d\'exécution (s)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 's'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Matrice',
                data: [0.173, 0.190, 0.221, 0.224]
            }, {
                name: 'Liste',
                data: [0.175, 0.252, 0.548, 1.104]
            }, {
                name: 'Mixte',
                data: [0.161, 0.189, 0.224, 0.188]
            }]
        });
    
        $('#container1530').highcharts({
            title: {
                text: 'Mots de longueur comprise entre 15 et 30',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                title: {
                    text: 'Taille de l\'alphabet'
                },
                categories: ['2', '4', '20', '70']
            },
            yAxis: {
                title: {
                    text: 'Temps d\'exécution (s)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 's'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Matrice',
                data: [0.174, 0.194, 0.222, 0.225]
            }, {
                name: 'Liste',
                data: [0.175, 0.248, 0.553, 1.068]
            }, {
                name: 'Mixte',
                data: [0.162, 0.191, 0.222, 0.191]
            }]
        });
    
       $('#container3060').highcharts({
            title: {
                text: 'Mots de longueur comprise entre 30 et 60',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                title: {
                    text: 'Taille de l\'alphabet'
                },
                categories: ['2', '4', '20', '70']
            },
            yAxis: {
                title: {
                    text: 'Temps d\'exécution (s)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 's'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Matrice',
                data: [0.177, 0.193, 0.223, 0.227]
            }, {
                name: 'Liste',
                data: [0.179, 0.247, 0.540, 1.097]
            }, {
                name: 'Mixte',
                data: [0.164, 0.192, 0.217, 0.195]
            }]
        });
        $('#containerAny').highcharts({
            title: {
                text: 'Moyenne',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                title: {
                    text: 'Taille de l\'alphabet'
                },
                categories: ['2', '4', '20', '70']
            },
            yAxis: {
                title: {
                    text: 'Temps d\'exécution (s)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 's'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Matrice',
                data: [0.175, 0.192, 0.222, 0.225]
            }, {
                name: 'Liste',
                data: [0.176, 0.249, 0.547, 1.090]
            }, {
                name: 'Mixte',
                data: [0.162, 0.191, 0.221, 0.191]
            }]
        });
});
    

