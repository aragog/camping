== Consultation des avis des campeurs ==

Code: consultAvis0
Description: Le syst�me propose aux administateurs et aux visiteurs d'�tudier les avis de visiteurs, une fois les locaux d'un accueillant visit�.
Etat: obligatoire

Code: consultAvis1
Description: Le syst�me permet aux administrateurs et aux visiteurs d'�tudier les avis au cas par cas, ou par moyenne sur les r�ponses aux questions d�finies dans avisCamp3.
Etat: Important

Code: consultAvis2
Description: Le syst�me permet � un administrateur de choisir si un endroit ou un accueillant doit �tre banni, selon le nombre et la qualit� des avis.
Etat: Important

Code: consultAvis3
Description: Les avis doivent �tre accessibles depuis la page de description d'un endroit mis � disposition par un accueillant.
Etat : Important