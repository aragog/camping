== Modification des tarifs des publicit�s ==

Code: modifTarifPub0
Description: Le syst�me doit �tre con�u pour permettre aux administrateurs de cr�er et modifier la grille tarifaire des publicit�s.
Etat: obligatoire

Code: modifTarifPub1
Description: Les administrateurs ont acc�s aux statistiques de clic sur l'ensemble du site pour faire un choix inform� sur la modification des tarifs. (cf consultStat)
Etat: obligatoire

Code: modifTarifPub2
Description: Les administrateurs doivent pouvoir arr�ter le d�pot de nouvelles publicit�s pendant la modification de la grille tarifaire, pour �viter les erreurs.
Etat: Important

