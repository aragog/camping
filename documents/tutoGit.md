# Cycle habituel #

La première fois:

* `git clone https://aragog@bitbucket.org/aragog/camping.git`

Ensuite:

1. je travaille;
2. je récupère le travail des copains : `git fetch origin` voire `git fetch`;
3. je fusionne avec mon dépôt local: `git merge origin/master`;
4. si conflits de fusion, je ne panique pas et les règle manuellement;
5. je fais suivre mon travail : `git add <mon/mes fichier(s)>`;
6. je marque mon travail : `git commit -m "ma modification"`;
7. je partage mon travail : `git push origin master` voire `git push`;
8. je recommence à l'étape une.


### Commandes utiles ###

`git clone <url> [local_name]`  
  Initialiser un dépôt local à partir d'un dépôt distant avec pour nom "local_name"

`git add <file> ...`  
  Suivre les changements d'un ou plusieurs fichiers

`git push -u <remote name> <branch name>`  
  Pousser les changements locals vers le dépôt distant  
  Exemple : `git push origin master` (envoie le commit dans la branche principale du dépôt)

`git status`  
   Montrer le statut du dépôt (nouveaux fichiers non encore ajoutés comme étant suivis avec `git add`, fichiers modifiés mais non encore commités avec `git commit`...)

`git commit`   
  Créer un instantané (snapshot) du dépôt local

`git commit -m "message"`  
  Tout pareil mais avec un message de publication

`git commit -am`  
  Tout pareil mais en faisant un git add automatique sur des fichiers déjà suivis

`git fetch <remote name>`  
  Récupérer les nouvelles données du dépôt distant  
  Exemple: `git fetch origin`

`git log`  
  Accéder aux fichiers journaux de Git

`git diff HEAD`  
  Voir les changements apportés par le commit le plus récent

`git checkout <filename>`  
  Réinitialiser le fichier de nom "filename" à son contenu tel que présent sur le dépôt distant

`git merge <branch name>`  
  Fusionner la branche courante avec la branche de nom "branch name"

`git branch <branch name>`  
  Créer une branche de nom "branch name"

`git branch -d <branch name>`  
  Supprimer la branche de nom "branch name"

`git branch -df <branch name>`  
  Supprimer une branche non encore fusionnée

`git branch [--list]`  
  Lister les branches courantes

`git checkout <branch name>`  
  Passer sur la branche de nom "branch name"

`git checkout -b <branch name>`  
  Créer puis passer sur la branche de nom "branch name"

`git checkout <remote name> <:branch name>`
  Supprimer la branche distante de nom "branch name" du dépôt "remote name"

#### Rappels ####

Le nom origin fait par défaut référence à l'URL du dépôt courant.
