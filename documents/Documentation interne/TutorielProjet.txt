﻿Tutoriel d’utilisation des technologies pour le projet Camping


Organisation générale d’une application AngularJS.


AngularJS s’articule autour d’une architecture MVC, architecture 3-tiers très courante.
Nous utilisons donc ce framework pour pouvoir découpler les responsabilités, non plus seulement côté serveur, mais aussi, côté client.
De fait, et pragmatiquement, on associe à une vue donnée un contrôleur doté d’un modèle.


Pour commencer, cette vue est une simple page HTML agrémentée, en son sein, de directives (préfixées par ng) AngularJS.


De plus, on lie une vue à un contrôleur (simple fonction Javascript). Ce dernier aura pour paramètres les ressources logicielles sur lesquelles je m’appuie, qu’elles soient intégrées par défaut au framework ou venant de ma propre boîte à outils. Cette façon de procéder est appelée en architecture logicielle : injection de dépendances (DI, Dependency Injection).


Enfin, mon modèle est contenu dans un “scope”, ce dernier est lié à un contrôleur lors de la DI, qui lui-même est lié à une vue ; mon modèle est donc lié à une page bien précise. Le scope est bien entendu unique : chaque client possède son propre espace de données.


Conclusion, j’assure correctement et sans beaucoup plus de travail la réalisation de mon pattern MVC, et ce, côté client.


Vue (AngularJS, Twitter Bootstrap, CSS3, JS, JQuery, HTML5)


- AngularJS permet donc d’ajouter une surcouche supplémentaire au HTML habituel (ng-*).
- Pour les plus curieux d’entre vous, il permet de mettre en place, à sa façon, la future spécification du W3C en cours d’écriture : les Web Components.


Quelques directives intéressantes sont bonnes à connaître et à avoir sous la main, elles sont résumées, plus que partiellement, ici. Une minorité d’entre elles sont utilisées effectivement dans src/main/webapp/partials/test.html.


Le reste dans la documentation :
http://docs.angularjs.org/api/


formulaire : AngularJS permet de placer des attributs HTML mais aussi les siens aux éléments HTML standards (balises) pour une validation dynamique des champs qui le composent.
        - required (sur un champ, ie <input> [HTML5]) : le champ renvoie une erreur s’il est vide.
        - novalidate (sur un <form> [HTML]) : désactive la validation standard de formulaire du navigateur. On évite ainsi des conflits entre la validation HTML standard et celle d’AngularJS.
- $valid : renvoie vrai si le form ne renvoie pas d’erreur.
Un formulaire est valide si tous les champs qui le composent sont valides.
- $invalid : renvoie vrai si le form renvoie une erreur.
Un formulaire est invalide si tous les champs qui le composent sont invalides.
- $pristine : renvoie vrai si le champ concerné n’a pas été touché (faux à la moindre entrée clavier).
- $dirty : renvoie vrai si le champ concerné a été modifié (faux jusqu’à la première entrée clavier).
- $error : permet d’accéder aux erreurs. Si un attribut ng-minlength a été défini dans l’input de nom “lastname” du formulaire de nom “form” (côté HTML, donc), on peut vérifier si sa condition est vérifiée en testant “form.lastname.$error.minlength” qui renvoie un booléen (vrai si OK, faux sinon) [toujours côté HTML).


On récupère ces valeurs par le biais d’une syntaxe AngularJS. Par exemple, si nous voulons la valeur “dirty” du champ “lastname” du formulaire “normform”, on écrit “normform.lastname.$dirty”. 


Ces petits booléens sont utiles pour fournir une validation automatique d’un formulaire avec couleurs, bulles d’aide, indications, traitements en cas d’erreur ou de succès etc, et le tout, côté client.
Les indications et bulles d’aide étaient une demande du client.


http://docs.angularjs.org/api/ng.directive:form
http://docs.angularjs.org/api/ng.directive:input.number
http://docs.angularjs.org/guide/expression


ng-class : À utiliser sur une balise HTML. Il permet de mapper des classes CSS selon le contexte.


Citons la documentation : la directive ngClass vous permet de modifier dynamiquement les classes CSS d’un élément HTML en liant, par le biais de la liaison bidirectionnelle, une expression qui représente toutes les classes à ajouter.


Quand l’expression change, les classes précédemment ajoutées sont enlevées et seulement ensuite, les nouvelles classes sont ajoutées.


Exemple :
J’ai deux classes dans mon Bootstrap : text-error et text-warning et un paragraphe de texte dans mon HTML.


<p ng-class=”myClass”>Le camping, c’est trop de la balle, sa mère !</p>


myClass est, rappellons-le, dans le scope du contrôleur (voir plus haut si doute).


Soient deux boutons :


<button ng-click=”myClass = ‘text-error’”>Erreur</button>
<button ng-click=”myClass = ‘text-warning’”>Danger</button>
<button ng-click=”myClass = ‘’”>Réinitialiser</button>


En cliquant sur mes différents boutons, je fixe, dynamiquement et sans effort, le style de mon paragraphe.


Pour de plus amples informations : http://docs.angularjs.org/api/ng.directive:ngClass


ng-show : L’élément ne sera visible que si l’évaluation de la condition vaut vrai.


ng-minlength : Définit une longueur d’entrée minimale dans un input, sous laquelle (stricte) l’élément renvoie une erreur.


ng-maxlength : Définit une longueur d’entrée maximale dans un input, au delà de laquelle (stricte) l’élément renvoie une erreur.


ng-pattern : Permet de définir une expression rationnelle qui doit être respectée, ou l’élément renvoie une erreur.


Exemple :


<input type="text" ng-pattern="/a-zA-Z/" />


Les / sont obligatoires (on évitera ainsi des comportements hasardeux et accessoirement d’avoir envie de manger son clavier).


ng-click : Utilise une fonction Javascript lorsque l’élément est cliqué (la fonction doit être déclarée et définie dans le contrôleur attaché à la page courante). 


En toute généralité, ng-click, tout comme les autres directives, accepte toute expression AngularJS valide.




Résumé


Soyez curieux !


Quant le curseur de l’utilisateur entre sur mon <div>, je voudrais faire telle autre action : http://docs.angularjs.org/api/ng.directive:ngMouseenter


Je veux désactiver ou non tel élément en fonction de si mon client est connecté ou pas : http://docs.angularjs.org/api/ng.directive:ngDisabled


J’ai une liste de photos récupérées depuis mon scope et je voudrais boucler dessus à la manière d’un foreach : http://docs.angularjs.org/api/ng.directive:ngRepeat


Pour conclure, l’API de Google sait faire beaucoup de choses en s’appuyant sur ce qu’on peut déjà faire avec du Javascript. De fait, si vous pouviez écouter tel ou tel événement en Javascript, vous pouvez aussi le faire avec AngularJS.
Si par le plus grand des hasards ce n’était pas le cas, il doit y avoir une autre façon de faire ce que vous désirez, à coup sûr.


Bootstrap


Petite info qui décidément a du mal à filtrer.


Nous utilisons exclusivement les classes CSS prédéfinies de Bootstrap pour le design général.
Ces classes se trouvent dans la documentation officielle de la boîte à outils. Cette dernière est déployée, officiellement, dans deux versions différentes : la 2.3.2 et la 3.0.3.


Dans le cadre du projet, nous utilisons la toute dernière mouture de Bootstrap, donc la 3.0.3.
On notera au passage la raison de ne pas avoir pris en charge une version davantage mature et ayant fait ses preuves, ie la 2.3.2 : la version n’est bientôt plus supportée par Twitter.


http://getbootstrap.com/css/


Bootstrap et mon style à moi


On peut toujours créer nos feuilles de style personnelles à partir du moment où on ne touche pas au style de Bootstrap.


Cependant, j’émets une grosse réserve sur cette façon de faire pour ces raisons :


- aucun intérêt, le client n’a fait aucune demande à ce sujet;
- tâche chronophage;
- le style écrit sera moindre et fragilisera le cross browsing qui lui, est une demande du client;
- par principe : les fioritures c’est comme les finitions, c’est quand on a le temps avec une base solide derrière;
- augmentation du travail du testeur qui ne peut assurément pas se fier à ces styles personnels et devra donc les tester avec des moyen techniques très lourds et pas toujours très fiables.


Voici la raison pour laquelle Bootstrap a été choisi : s’éviter ce genre d’ennuis.


Spring MVC


Nous noterons en guise d’introduction que Spring MVC a été choisi pour son intégration naturelle avec AngularJS. En effet, ce framework expose des ressources REST et AngularJS en consomme avidement afin de remplir ses scopes.
Enfin, pour couper court à toute confusion, hypothétique mais vraisemblable, nous parlerons ici de termes en rapport avec l’architecture MVC. Ce MVC là se situe côté serveur.
Nous veillerons donc bien lors de nos futurs échanges à faire la distinction entre, par exemple, le contrôleur côté serveur et le contrôleur côté client. Ce sont deux entités ayant des rôles similaires mais dans des couches totalement différentes.


- Spring MVC permet de décomposer l’architecture selon le modèle MVC. La vue est gérée par HTML, JS et AngularJS. Le modèle est réalisé en Java, par le biais de Java beans.
- Il gère la partie contrôleur par le biais de services REST (en ouvrant une URI qui peut recevoir des messages et renvoyer des ressources de type objet).
- Les objets manipulés sont les mêmes que ceux utilisés par la BDD (Hibernate).
- Par le biais d’annotations Java, et par la définition d’accès aux objets Java du côté Javascript, on peut accéder aux fonctions Java depuis Javascript et inversement.
- Afin de traduire facilement d’un langage à un autre, Spring MVC use et abuse des beans. Les variables d’un bean Java et le nom des champs d’un formulaire doivent impérativement correspondre.


Les annotations Java (présentes dans AcceuillantController.java) :


        @Controller : permet de définir des services REST.
        @Autowired : permet une instantiation automatique (patrons Factory et Singleton)
        @RequestMapping(value, method, produces) : Lie la méthode définie juste après à l’URI value par la méthode HTTP method et qui envoie un résultat de type produces.
        @ResponseBody : traduit le résultat d’une méthode Java en le corps de la réponse HTTP reçue.
        @PathVariable : récupère une variable définie dans l’URI.
@ResponseStatus(HttpStatus.NO_CONTENT) : Renvoie un code de retour 204 (Ok, No Content). A utiliser avec les méthodes renvoyant void.
        
Les annotations Java (présentes dans AcceuillantService.java) :


        @Service : permet de définir une classe service.
        @Autowired : ici, il initialise aussi l’objet DAO.
        @Transactional : permet de définir la portée d’action.


http://docs.spring.io/spring/docs/3.0.x/reference/mvc.html
http://www.mkyong.com/spring-mvc/spring-mvc-hello-world-annotation-example/
http://viralpatel.net/blogs/spring3-mvc-hibernate-maven-tutorial-eclipse-example/


Résumé :


Si je veux créer un LocationController, je copie AccueillantController en remplaçant les occurrences de Accueillant en Location et en changeant le type de mon service (AccueillantService devient LocationService).        


Enfin, je crée un nouveau service côté AngularJS à l’image de (src/main/webapp/js/app/services.js) : 


myModule.factory('Accueillant', function($resource) {
    return $resource('rest/accueillant/:id', {}, {
            'update' : { method:'PUT'}
    });
});


qui deviendrait :


myModule.factory(‘Location’, function($resource) {
    return $resource('rest/location/:id', {}, {
            'update' : { method:'PUT'}
    });
});


Pour les plus curieux, AngularJS se sert, idéalement, de services pour communiquer sur le protocole HTTP. On évite ainsi (c’est ce qu’essaye de nous expliquer, pas toujours très clairement, je le conçois, l’API) la ressource bas-niveau $http (elle-même surcouche de XMLHttpRequest, voir les cours de Langage Web si vous vous ne rappelez plus de cette chose).


Ce qui nous permet de faire la différence entre toutes les méthodes disponibles dans le contrôleur côté Java c’est, au final, les méthodes HTTP (les verbes si vous préférez une appellation moins ambigüe).


Or, le service, à l’image d’Accueillant, permet de dialoguer en HTTP sans passer par tout ce charabia vieillissant (comprenez, bas-niveau). 


Exemple


Je veux créer un nouvel accueillant :


$scope.create = function(user) {
   Accueillant.save(user, function(data) {
            // je gère mon succès
    }, function(err) {
            // je gère mon erreur [data.err contient l’erreur]
    });
};


On est d’accord que j’aurais pu changer le nom de la fonction attachée à mon scope hein :


$scope.sauvegardeMoiUneNouvelleInstanceDAccueillant = function(user) { […] }


Et si je veux modifier, supprimer… ?!


Et bien voici la liste des méthodes (qui nous intéressent) à invoquer :


save, update, delete, get, query.


save pour sauvegarder, update pour MAJ, delete pour supprimer, get pour récupérer un seul élément (en fonction, par exemple, de son identifant), query pour récupérer une liste d’éléments de même type (correspondrait à une List en Java).


http://docs.angularjs.org/api/ngResource.$resource (attention, un peu daubé du cul)


Hibernate


Tout d’abord, au risque de se répéter : on ne manipulera pas d’instructions SQL directement.
Hibernate s’occupe de tout, automatiquement, ou presque (automagiquement me soufflait Aimad qui l’avait entendu d’un barbu avec un blog chelou).
Il suffit de donner quelques annotations (et oué encore des annotations, profitez, c’est à la mode !) aux JavaBeans afin que l’ORM retrouve ces petits.




Exemple


Si dans mon Accueillant, j’ai une propriété, privée, du style :


private List<Location> locations;


Lors d’une récupération, ultérieure, de mon accueillant, le fais-je de façon lazy, paresseuse ou de façon gloutonne ? En d’autres termes, est-ce que je récupère aussi, et systématiquement, les locations qu’il possède, ou est-ce que je souhaite simplement récupérer des informations sur l’accueillant, et c’est tout ?!


Ce sera certainement une question à se poser, celle-ci ou une autre, et nous y répondrons par le biais des annotations Hibernate (pas souvent très simples, mais toujours très puissantes).


--------------------------------------


De fait, Hibernate se charge donc aussi de créer une table à chaque nouveau JavaBean.
Au passage, un JavaBean est une classe Java avec des propriétés privées et des getters/setters publiques (les deux ne sont pas obligatoirement présents, l’un ou l’autre peut suffire en fonction, par exemple, d’un attribut en lecture seul [ici, pas de getter]), avec au moins un constructeur par défaut.


Hibernate retrouve ses petits de cette façon.


Pour ceux qui ont eu le bonheur mais aussi le privilège de rencontrer M. Andary, ce dernier attribuait le nom de requête pour un getter et de commande pour un setter.


Mise en place du pattern DAO.


==


ATTENTION


On parle également de service dans le pattern DAO. Ne confondez pas avec les services créés grâce à une factory par AngularJS dans, je le rappelle, src/main/webapp/js/app/services.js.


==


Rapidement, le pattern DAO permet de découpler la responsabilité d’accès à la BDD (soit en SQL directement, soit en passant par Hibernate, dans notre cas) et celle lié à la couche métier.
Ici cette fameuse couche métier (terme à la con que j’exècre) fait simplement référence aux méthode du contrôleur (ou des contrôleurs si on se fie aux annotations @Controller de notre bon vieux Spring MVC).


En gros, je sépare les traitements faits avant ou après l’accès à la BDD et l’accès effectif à cette BDD. Conclusion, les accès à la BDD ne se font pas directement depuis un contrôleur mais par le biais d’un service qui se chargera, de son côté, d’utiliser un DAO qui lui, enfin diront certains, enverra directement des message à la BDD (même si dans notre cas, le DAO doit d’abord passer par Hibernate, gardien ultime).


Note


Le nom que vous donnez aux champs constituant les formulaires doivent correspondre aux noms des propriétés déclarées dans les JavaBeans.
Spring MVC renvoie des objets Java. Ces objets Java sont traduits en JSON par une API fabuleuse : http://jackson.codehaus.org/ (automatiquement)
C’est de cette façon qu’AngularJS peut “consommer” ce qu’on lui donne.
Le procédé inverse est immédiat (AngularJS vers SpringMVC). On comprend dès lors mieux le pourquoi de cette contrainte : assurer une correspondance (mapping) parfaite.


Conclusion : Hibernate est notre Cerbère.


PS : faites-moi penser à rajouter une entrée pour une figure de pattern dans la DAL.




Conclusion


Cette documentation a pour but de devenir une documentation interne modifiable par tout un chacun lorqu’une personne estimera qu’un point manquant pourrait être ajouté afin d’aider une autre personne qui serait en droit de se poser la même question dû à un problème similaire.


Nous remercions Aimad pour avoir insufflé la vie à ces pages et espérons que nous lui apprendrons les rouages de la vie, ensemble.


Profitez de ces nouvelles technologies, il y a, AMHA, très peu de chances pour que dans un avenir proche on puisse utiliser les technologies qui nous font envie.


Bons hacks.