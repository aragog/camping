RDV Client (Bruno Patrou).
Pr�sents : Simon, Edwin, Aimad et Sabrina.
Points �claircis :
 - Avis formels : Il faut que les accueillants souhaitant des avis formels enregistrent les adresse m�l des campeurs, pour que les campeurs puissent �tre redirig�s vers un formulaire de satisfaction.

 - G�olocalisation : Si le pointage GPS du lieu d'accueil est tr�s diff�rent de sa localisation r�el, il faudrait pouvoir le pointer � la main sur la carte, laissant � l'accueillant toute responsabilit� en cas d'erreur.