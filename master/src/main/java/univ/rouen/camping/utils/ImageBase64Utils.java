package univ.rouen.camping.utils;

import org.imgscalr.Scalr;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageBase64Utils {
    /**
     * Format par défaut utilisé pour une image.
     */
    public static final String DEFAULT_FORMAT = "png";

    /**
     * Décode la chaîne de caractères en image.
     * @param imageString La chaîne de caractères à décoder
     * @return L'image décodée
     * @throws IOException Erreur I/O
     */
    public static BufferedImage decodeToImage(String imageString) throws IOException {
        byte[] imageByte;
        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        BufferedImage image;

        try (ByteArrayInputStream bis = new ByteArrayInputStream(imageByte)) {
            image = ImageIO.read(bis);
        }

        return image;
    }

    /**
     * Encode l'image en chaîne de caractères.
     * @param image l'image à encoder
     * @param type jpeg, png, bmp...
     * @return Chaîne de caractères encodé
     * @throws IOException Erreur I/O
     */
    public static String encodeToString(BufferedImage image, String type) throws IOException {
        String imageString;

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();
            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);
        }

        return imageString;
    }

    /**
     * Retaille l'image codée en URI base 64.
     * @param imageString Image codée en base 64
     * @param width Largeur du résultat
     * @param height Hauteur du résultat
     * @param type Type du résultat
     *  Si non spécifié, fixé à <code>DEFAULT_FORMAT</code>
     * @return L'image retaillée et encodée en base 64
     * @throws IOException Erreur I/O
     */
    public static String resizeBase64URI(String imageString, int width, int height, String type) throws IOException {
        // @see http://stackoverflow.com/questions/7375635/xhr-send-base64-string-and-decode-it-in-the-server-to-a-file
        String realBase = imageString.replaceFirst("^[^,]+,", "");

        BufferedImage image = Scalr.resize(
                decodeToImage(realBase),
                Scalr.Method.ULTRA_QUALITY,
                Scalr.Mode.FIT_EXACT,
                width,
                height,
                Scalr.OP_ANTIALIAS);

        type = (type != null) ? type : DEFAULT_FORMAT;

        return headerForURI(type) + encodeToString(image, type);
    }

    /*
     * Retourne le header pour une URI en base 64 en fonction du type.
     * Le type doit être dans un format court (png, jpg, gif...).
     */
    private static String headerForURI(String type) {
        return "data:image/" + type + ";base64,";
    }
}
