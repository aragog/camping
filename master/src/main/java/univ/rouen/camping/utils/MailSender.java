package univ.rouen.camping.utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


public class MailSender {

    public enum Host {
        GMAIL {
            @Override
            public Properties getProperties() {
                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");
                return props;
            }

            @Override
            public String getUsername() {
                return "campingm1gil@gmail.com";
            }

            @Override
            public String getPassword() {
                return "h%sLgv5fm1B.vvh";
            }
        };

        public abstract Properties getProperties();

        public abstract String getUsername();

        public abstract String getPassword();
    }

	public static void sendMessage(String subject, String text, String destinataire) throws MessagingException {
        // 1 -> Création de la session
        Session session = Session.getInstance(Host.GMAIL.getProperties(), new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Host.GMAIL.getUsername(), Host.GMAIL.getPassword());
            }
        });
        // 2 -> Création du message
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(Host.GMAIL.getUsername()));
        InternetAddress internetAddress = InternetAddress.parse(destinataire)[0];
        message.setRecipient(Message.RecipientType.TO, internetAddress);
        message.setSubject(subject);
        message.setText(text);
        // 3 -> Envoi du message
        // un bug peut survenir ici si GMAIL a bloqué le compte
        Transport.send(message);
	}
}