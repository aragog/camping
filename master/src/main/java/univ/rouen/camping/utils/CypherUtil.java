package univ.rouen.camping.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class CypherUtil {
    // Nombre d'itération par défaut lors des différents calculs
    private static final int DEFAULT_ITERATION_NUMBER = 1000;
    // Mot de passe 'digéré' factice
    private static final String DUMMY_DIGEST = "000000000000000000000000000=";
    // Clé de 'salage' factice
    private static final String DUMMY_SALT = "00000000000=";

    /**
     * Crée un chiffreur de mot de passe.
     * @param password Mot de passe à chiffrer
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static CipherItem makeCiphering(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] bSalt = new byte[8];
        random.nextBytes(bSalt);
        byte[] bDigest = getHash(DEFAULT_ITERATION_NUMBER, password, bSalt);

        return new CipherItem(byteToBase64(bDigest), byteToBase64(bSalt));
    }

    /**
     * Retourne un déchiffreur en fonction des mots de passes, et clé de salage.
     *  Si <code>userExists</code> est faux, alors un calcul factice est créé.
     * Après l'appel à cette fonction CipherItem.getProposedDigest() contient le
     *  digéré de <code>password</code>.
     * Ainsi, les deux mots de passe sont identiques ssi getDigest() et
     *  getProposedDigest() le sont.
     * @param password Mot de passe digéré
     * @param salt Clé de salage
     * @param password Mot de passe candidat
     * @param userExists Indique si l'utilisateur existe
     * @return Le déchiffreur correspondant
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static DecipherItem makeDeciphering(String digest, String salt, String password, boolean userExists) throws IOException, NoSuchAlgorithmException {
        CipherItem cipherItem;
        if (userExists) {
            if ((digest == null) || (salt == null)) {
                // Database inconsistant Salt or Digested Password altered
                throw new univ.rouen.camping.exception.InternalError();
            }
            cipherItem = new CipherItem(digest, salt);
        } else {
            // TIME RESISTANT ATTACK (Even if the user does not exist the
            // Computation time is equal to the time needed for a legitimate user)
            cipherItem = new CipherItem(DUMMY_DIGEST, DUMMY_SALT);
        }

        DecipherItem decipherItem = new DecipherItem();
        decipherItem.setDigest(base64ToByte(cipherItem.getDigest()));
        decipherItem.setSalt(base64ToByte(cipherItem.getSalt()));
        decipherItem.setProposedDigest(
                CypherUtil.getHash(CypherUtil.DEFAULT_ITERATION_NUMBER, password, decipherItem.getSalt()));


        return new DecipherItem(
                base64ToByte(cipherItem.getDigest()),
                base64ToByte(cipherItem.getSalt()),
                getHash(DEFAULT_ITERATION_NUMBER, password, decipherItem.getSalt()));
    }

    /*
     * Retourne une représentation en base 64 des données passées en paramètre.
     * @param data Données
     * @return La représentation en base 64
     * @throws Exception
     */
    private static String byteToBase64(byte[] data) {
        BASE64Encoder endecoder = new BASE64Encoder();
        return endecoder.encode(data);
    }

    /*
     * Retourne un tableau d'octets à partir d'une chaîne de données codée
     *  en base 64.
     * @param data Données
     * @return Le tableau d'octets
     * @throws IOException
     */
    private static byte[] base64ToByte(String data) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        return decoder.decodeBuffer(data);
    }

    /*
     * Retourne le 'digest' correspondant aux mot de passe, nombre d'itérations
     *  et valeur de 'salage'.
     * @param iterationNb Nombre d'itérations pour le calcul
     * @param password Mot de passe à chiffrer
     * @param salt Valeur de salage
     * @return Le mot de passe 'digéré'
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    private static byte[] getHash(int iterationNb, String password,
                                  byte[] salt) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update(salt);
        byte[] input = digest.digest(password.getBytes("UTF-8"));
        for (int i = 0; i < iterationNb; i++) {
            digest.reset();
            input = digest.digest(input);
        }
        return input;
    }

    /**
     * Modélise un chiffreur de mot de passe.
     */
    public static class CipherItem {
        private String digest;
        private String salt;

        public CipherItem() {
        }

        public CipherItem(String digest, String salt) {
            this.digest = digest;
            this.salt = salt;
        }

        public String getDigest() {
            return digest;
        }

        public void setDigest(String digest) {
            this.digest = digest;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("CipherItem{");
            sb.append("digest='").append(digest).append('\'');
            sb.append(", salt='").append(salt).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    /**
     * Modélise un déchiffreur de mot de passe.
     */
    public static class DecipherItem {
        private byte[] digest;
        private byte[] salt;
        private byte[] proposedDigest;

        public DecipherItem() {
        }

        public DecipherItem(byte[] digest, byte[] salt) {
            this.digest = digest;
            this.salt = salt;
        }

        public DecipherItem(byte[] digest, byte[] salt, byte[] proposedDigest) {
            this.digest = digest;
            this.salt = salt;
            this.proposedDigest = proposedDigest;
        }

        public byte[] getDigest() {
            return digest;
        }

        public void setDigest(byte[] digest) {
            this.digest = digest;
        }

        public byte[] getSalt() {
            return salt;
        }

        public void setSalt(byte[] salt) {
            this.salt = salt;
        }

        public byte[] getProposedDigest() {
            return proposedDigest;
        }

        public void setProposedDigest(byte[] proposedDigest) {
            this.proposedDigest = proposedDigest;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("DecipherItem{");
            sb.append("digest=").append(Arrays.toString(digest));
            sb.append(", salt=").append(Arrays.toString(salt));
            sb.append(", proposedDigest=").append(Arrays.toString(proposedDigest));
            sb.append('}');
            return sb.toString();
        }
    }
}
