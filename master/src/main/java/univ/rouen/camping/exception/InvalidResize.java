package univ.rouen.camping.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Impossible to resize")  // 500
public class InvalidResize extends RuntimeException {
}
