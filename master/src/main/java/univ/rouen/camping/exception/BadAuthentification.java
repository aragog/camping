package univ.rouen.camping.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Bad user or password")  // 404
public class BadAuthentification extends RuntimeException {
}
