package univ.rouen.camping.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Unauthorized subscription")  // 401
public class UsernameNotFree extends RuntimeException {
}