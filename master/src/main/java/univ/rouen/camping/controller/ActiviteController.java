package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Activite;
import univ.rouen.camping.service.ActiviteService;

import java.util.List;

@Controller
public class ActiviteController {
    @Autowired
    private ActiviteService activiteService;

    @RequestMapping(value = "/activite", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Activite> list() {
        return activiteService.getAll();
    }

    @RequestMapping(value = "/activite/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Activite getById(@PathVariable long id) {
        return activiteService.getById(id);
    }

    @RequestMapping(value = "/activite", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Activite activite) {
        activiteService.create(activite);
    }

    @RequestMapping(value = "/activite", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Activite activite) {
        activiteService.update(activite);
    }

    @RequestMapping(value = "/activite/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        activiteService.delete(id);
    }
}
