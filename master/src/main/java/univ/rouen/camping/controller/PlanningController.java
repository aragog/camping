package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Planning;
import univ.rouen.camping.service.PlanningService;

import java.util.List;

@Controller
public class PlanningController {
    @Autowired
    private PlanningService planningService;

    @RequestMapping(value = "/planning", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Planning> list() {
        return planningService.getAll();
    }

    @RequestMapping(value = "/planning/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Planning getById(@PathVariable long id) {
        return planningService.getById(id);
    }

    @RequestMapping(value = "/planning", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Planning planning) {
        planningService.create(planning);
    }

    @RequestMapping(value = "/planning", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Planning planning) {
        planningService.update(planning);
    }

    @RequestMapping(value = "/planning/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        planningService.delete(id);
    }
}
