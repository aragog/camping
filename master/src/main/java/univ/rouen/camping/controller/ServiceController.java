package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Service;
import univ.rouen.camping.service.ServiceService;

import java.util.List;

@Controller
public class ServiceController {
    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "/service", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Service> list() {
        return serviceService.getAll();
    }

    @RequestMapping(value = "/service/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Service getById(@PathVariable long id) {
        return serviceService.getById(id);
    }

    @RequestMapping(value = "/service", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Service service) {
        serviceService.create(service);
    }

    @RequestMapping(value = "/service", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Service service) {
        serviceService.update(service);
    }

    @RequestMapping(value = "/service/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        serviceService.delete(id);
    }
}
