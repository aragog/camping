package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.QuestionQuestionnaire;
import univ.rouen.camping.service.QuestionQuestionnaireService;

import java.util.List;

@Controller
public class QuestionQuestionnaireController {
    @Autowired
    private QuestionQuestionnaireService questionQuestionnaireService;

    @RequestMapping(value = "/questionQuestionnaire", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<QuestionQuestionnaire> list() {
        return questionQuestionnaireService.getAll();
    }

    @RequestMapping(value = "/questionQuestionnaire/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody QuestionQuestionnaire getById(@PathVariable long id) {
        return questionQuestionnaireService.getById(id);
    }

    @RequestMapping(value = "/questionQuestionnaire", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody QuestionQuestionnaire questionQuestionnaire) {
        questionQuestionnaireService.create(questionQuestionnaire);
    }

    @RequestMapping(value = "/questionQuestionnaire", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody QuestionQuestionnaire questionQuestionnaire) {
        questionQuestionnaireService.update(questionQuestionnaire);
    }

    @RequestMapping(value = "/questionQuestionnaire/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        questionQuestionnaireService.delete(id);
    }
}