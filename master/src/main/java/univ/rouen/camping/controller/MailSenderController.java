package univ.rouen.camping.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Mail;
import univ.rouen.camping.utils.MailSender;

import javax.mail.MessagingException;

@Controller
public class MailSenderController {

    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void send(@RequestBody Mail mail) throws MessagingException {
        MailSender.sendMessage(mail.getSubject(), mail.getText(), mail.getTo());
    }
}
