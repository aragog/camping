package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.AvisQuestionnaire;
import univ.rouen.camping.service.AvisQuestionnaireService;

import java.util.List;

@Controller
public class AvisQuestionnaireController {
    @Autowired
    private AvisQuestionnaireService avisQuestionnaireService;

    @RequestMapping(value = "/avisQuestionnaire", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AvisQuestionnaire> list() {
        return avisQuestionnaireService.getAll();
    }

    @RequestMapping(value = "/avisQuestionnaire/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody AvisQuestionnaire getById(@PathVariable long id) {
        return avisQuestionnaireService.getById(id);
    }

    @RequestMapping(value = "/avisQuestionnaire", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody AvisQuestionnaire avisQuestionnaire) {
        System.out.println(avisQuestionnaire);
        avisQuestionnaireService.create(avisQuestionnaire);
    }

    @RequestMapping(value = "/avisQuestionnaire", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody AvisQuestionnaire avisQuestionnaire) {
        avisQuestionnaireService.update(avisQuestionnaire);
    }

    @RequestMapping(value = "/avisQuestionnaire/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        avisQuestionnaireService.delete(id);
    }
}
