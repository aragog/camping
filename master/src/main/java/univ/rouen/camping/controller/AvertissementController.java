package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Avertissement;
import univ.rouen.camping.service.AvertissementService;

import java.util.List;

@Controller
public class AvertissementController {
    @Autowired
    private AvertissementService avertissementService;

    @RequestMapping(value = "/avertissement", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Avertissement> list() {
        return avertissementService.getAll();
    }

    @RequestMapping(value = "/avertissement/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Avertissement getById(@PathVariable long id) {
        return avertissementService.getById(id);
    }

    @RequestMapping(value = "/avertissement", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Avertissement avertissement) {
        avertissementService.create(avertissement);
    }

    @RequestMapping(value = "/avertissement", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Avertissement avertissement) {
        avertissementService.update(avertissement);
    }

    @RequestMapping(value = "/avertissement/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        avertissementService.delete(id);
    }
}
