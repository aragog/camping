package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.ListeNoireAccueillant;
import univ.rouen.camping.service.ListeNoireAccueillantService;

import java.util.List;

@Controller
public class ListeNoireAccueillantController {
    @Autowired
    private ListeNoireAccueillantService listeNoireAccueillantService;

    @RequestMapping(value = "/listeNoireAccueillant", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListeNoireAccueillant> list() {
        return listeNoireAccueillantService.getAll();
    }

    @RequestMapping(value = "/listeNoireAccueillant/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ListeNoireAccueillant getById(@PathVariable long id) {
        return listeNoireAccueillantService.getById(id);
    }

    @RequestMapping(value = "/listeNoireAccueillant", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody ListeNoireAccueillant listeNoireAccueillant) {
        listeNoireAccueillantService.create(listeNoireAccueillant);
    }

    @RequestMapping(value = "/listeNoireAccueillant", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody ListeNoireAccueillant listeNoireAccueillant) {
        listeNoireAccueillantService.update(listeNoireAccueillant);
    }

    @RequestMapping(value = "/listeNoireAccueillant/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        listeNoireAccueillantService.delete(id);
    }
}
