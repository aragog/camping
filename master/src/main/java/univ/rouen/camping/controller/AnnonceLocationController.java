package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.AnnonceLocation;
import univ.rouen.camping.model.AnnonceLocationFilter;
import univ.rouen.camping.model.Ville;
import univ.rouen.camping.service.AnnonceLocationService;

import java.util.List;

@Controller
public class AnnonceLocationController {
    @Autowired
    private AnnonceLocationService annonceLocationService;

    @RequestMapping(value = "/annonceLocation", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AnnonceLocation> list() {
        return annonceLocationService.getAll();
    }

    @RequestMapping(value = "/annonceLocation/etat/{etat}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AnnonceLocation> list(@PathVariable String etat) {
        return annonceLocationService.getAll(etat);
    }

    @RequestMapping(value = "/annonceLocation/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody AnnonceLocation getById(@PathVariable long id) {
        return annonceLocationService.getById(id);
    }

    @RequestMapping(value = "/annonceLocation/eager/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody AnnonceLocation getByIdEagerly(@PathVariable long id) {
        return annonceLocationService.getByIdEagerly(id);
    }

    @RequestMapping(value = "/annonceLocation/disponible", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AnnonceLocation> getAllValiderEtDisponible() {
        return annonceLocationService.getAllLocationDisponibles();
    }

    @RequestMapping(value = "/annonceLocation/ville", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody int nbLocationOfCity(@RequestBody Ville ville) {
        return annonceLocationService.nbLocationOfCity(ville);
    }

    @RequestMapping(value = "/annonceLocation/filter", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<AnnonceLocation> filteredGetAll(@RequestBody AnnonceLocationFilter annonceLocationFilter) {
        return annonceLocationService.filteredGetAll(annonceLocationFilter);
    }

    @RequestMapping(value = "/annonceLocation", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody AnnonceLocation annonceLocation) {
        annonceLocationService.create(annonceLocation);
    }

    @RequestMapping(value = "/annonceLocation", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody AnnonceLocation annonceLocation) {
        annonceLocationService.update(annonceLocation);
    }

    @RequestMapping(value = "/annonceLocation/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        annonceLocationService.delete(id);
    }

    @RequestMapping(value = "/annonceLocation/rayon/{distance}/{lat}/{lon:.+}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AnnonceLocation> getAllFromDistance(@PathVariable Double distance,
                                                                  @PathVariable Double lat,
                                                                  @PathVariable Double lon) {
        return annonceLocationService.getAllFromDistance(distance, lat, lon);
    }
}
