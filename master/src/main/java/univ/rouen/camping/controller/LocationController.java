package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Location;
import univ.rouen.camping.service.LocationService;

import java.util.List;

@Controller
public class LocationController {
    @Autowired
    private LocationService locationService;

    @RequestMapping(value = "/location", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Location> list() {
        return locationService.getAll();
    }

    @RequestMapping(value = "/location/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Location getById(@PathVariable long id) {
        return locationService.getById(id);
    }

    @RequestMapping(value = "/location", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Location location) {
        locationService.create(location);
    }

    @RequestMapping(value = "/location", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Location location) {
        locationService.update(location);
    }

    @RequestMapping(value = "/location/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        locationService.delete(id);
    }
}
