package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Etat;
import univ.rouen.camping.service.EtatService;

import java.util.List;

@Controller
public class EtatController {
    @Autowired
    private EtatService etatService;

    @RequestMapping(value = "/etat", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Etat> list() {
        return etatService.getAll();
    }

    @RequestMapping(value = "/etat/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Etat getById(@PathVariable long id) {
        return etatService.getById(id);
    }

    @RequestMapping(value = "/etat/nom/{name}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Etat getByName(@PathVariable String name) {
        return etatService.getByName(name);
    }

    @RequestMapping(value = "/etat", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Etat etat) {
        etatService.create(etat);
    }

    @RequestMapping(value = "/etat", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Etat etat) {
        etatService.update(etat);
    }

    @RequestMapping(value = "/etat/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        etatService.delete(id);
    }
}
