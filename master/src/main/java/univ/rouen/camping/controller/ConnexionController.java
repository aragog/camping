package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.exception.*;
import univ.rouen.camping.exception.InternalError;
import univ.rouen.camping.model.Role;
import univ.rouen.camping.model.SessionBean;
import univ.rouen.camping.model.SessionUnit;
import univ.rouen.camping.service.AccueillantService;
import univ.rouen.camping.service.AdministrateurService;
import univ.rouen.camping.service.PublicitaireService;
import univ.rouen.camping.utils.MailSender;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

@Controller
public class ConnexionController {
	@Autowired
    private SessionBean sessionBean;
    @Autowired
    private AccueillantService accueillantService;
    @Autowired
    private AdministrateurService administrateurService;
    @Autowired
    private PublicitaireService publicitaireService;

    @RequestMapping(value = "/connexion/{username}/{password}", method = RequestMethod.POST)
    public @ResponseBody SessionUnit connect(@PathVariable final String username, @PathVariable String password) throws IOException, NoSuchAlgorithmException {
        Long id = accueillantService.connect(username, password);
        Role role;

        try {
            if (id == null) {
                id = administrateurService.connect(username, password);
                if (id == null) {
                    id = publicitaireService.connect(username, password);
                    role = Role.PUBLICITAIRE;
                } else {
                    role = Role.ADMINISTRATEUR;
                }
            } else {
                if (accueillantService.getById(id).getListeNoireAccueillant() != null) {
                    throw new ForbiddenAuthentification();
                }
                role = Role.ACCUEILLANT;
            }
        } catch (Exception e) {
            throw new InternalError();
        }

        if ((id == null) && (!administrateurService.usernameIsFree(username))) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        MailSender.sendMessage("Camping - connexion erronée "
                                + Calendar.getInstance().getTime(),
                                "Une tentative de connexion a échouée sur l'identifiant "
                                        + "d'administrateur suivant : " + username,
                                MailSender.Host.GMAIL.getUsername());
                    } catch (MessagingException e) {
                        throw new univ.rouen.camping.exception.InternalError();
                    }
                }
            }).start();
        }

        if (id == null) {
            throw new BadAuthentification();
        }

        sessionBean.setSessionUnit(new SessionUnit(id, role));
        return sessionBean.getSessionUnit();
    }

    @RequestMapping(value = "/getSession", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody SessionUnit getSessionUnit() {
        return sessionBean.getSessionUnit();
    }

    @RequestMapping(value = "/deconnexion", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void disconnect() {
        sessionBean.setSessionUnit(null);
    }
}
