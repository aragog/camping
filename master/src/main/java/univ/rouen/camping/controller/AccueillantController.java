package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.exception.*;
import univ.rouen.camping.model.Accueillant;
import univ.rouen.camping.model.Avertissement;
import univ.rouen.camping.service.AccueillantService;
import univ.rouen.camping.service.AdministrateurService;
import univ.rouen.camping.service.PublicitaireService;

import java.util.Collection;
import java.util.List;

@Controller
public class AccueillantController {
    @Autowired
    private AccueillantService accueillantService;
    @Autowired
    private AdministrateurService administrateurService;
    @Autowired
    private PublicitaireService publicitaireService;

    @RequestMapping(value = "/accueillant", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Accueillant> list() {
        return accueillantService.getAll();
    }

    @RequestMapping(value = "/accueillant/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Accueillant getById(@PathVariable long id) {
        return accueillantService.getById(id);
    }

    @RequestMapping(value = "/accueillant", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Accueillant accueillant) throws Exception {
        if (!accueillantService.usernameIsFree(accueillant.getPseudo())
                || !administrateurService.usernameIsFree(accueillant.getPseudo())
                || ! publicitaireService.usernameIsFree(accueillant.getPseudo())) {
            throw new UsernameNotFree();
        }
        accueillantService.create(accueillant);
    }

    @RequestMapping(value = "/accueillant/profil", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@RequestBody Accueillant accueillant) {
        try {
            accueillantService.updateStatus(accueillant);
        } catch (Exception e) {
            throw new univ.rouen.camping.exception.InternalError();
        }
    }
    
    @RequestMapping(value = "/accueillant", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Accueillant accueillant) {
        accueillantService.update(accueillant);
    }

    @RequestMapping(value = "/accueillant/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        accueillantService.delete(id);
    }

    @RequestMapping(value = "/accueillant/averto/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Collection<Avertissement> getAvertos(@PathVariable Long id) {
        return accueillantService.getAvertos(id);
    }
}
