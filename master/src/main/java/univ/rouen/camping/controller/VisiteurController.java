package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Visiteur;
import univ.rouen.camping.service.VisiteurService;

import java.util.List;

@Controller
public class VisiteurController {
    @Autowired
    private VisiteurService visiteurService;

    @RequestMapping(value = "/visiteur", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Visiteur> list() {
        return visiteurService.getAll();
    }

    @RequestMapping(value = "/visiteur/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Visiteur getById(@PathVariable long id) {
        return visiteurService.getById(id);
    }

    @RequestMapping(value = "/visiteur", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Visiteur visiteur) {
        visiteurService.create(visiteur);
    }

    @RequestMapping(value = "/visiteur", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Visiteur visiteur) {
        visiteurService.update(visiteur);
    }

    @RequestMapping(value = "/visiteur/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        visiteurService.delete(id);
    }
}
