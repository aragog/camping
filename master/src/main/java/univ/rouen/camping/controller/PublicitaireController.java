package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.exception.UsernameNotFree;
import univ.rouen.camping.model.Publicitaire;
import univ.rouen.camping.service.AccueillantService;
import univ.rouen.camping.service.AdministrateurService;
import univ.rouen.camping.service.PublicitaireService;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
public class PublicitaireController {
    @Autowired
    private AccueillantService accueillantService;
    @Autowired
    private AdministrateurService administrateurService;
    @Autowired
    private PublicitaireService publicitaireService;

    @RequestMapping(value = "/publicitaire", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Publicitaire> list() {
        return publicitaireService.getAll();
    }

    @RequestMapping(value = "/publicitaire/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Publicitaire getById(@PathVariable long id) {
        return publicitaireService.getById(id);
    }

    @RequestMapping(value = "/publicitaire", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Publicitaire publicitaire) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (!accueillantService.usernameIsFree(publicitaire.getPseudo())
                || !administrateurService.usernameIsFree(publicitaire.getPseudo())
                || ! publicitaireService.usernameIsFree(publicitaire.getPseudo())) {
            throw new UsernameNotFree();
        }
        publicitaireService.create(publicitaire);
    }

    @RequestMapping(value = "/publicitaire", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Publicitaire publicitaire) {
        publicitaireService.update(publicitaire);
    }

    @RequestMapping(value = "/publicitaire/profil", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@RequestBody Publicitaire publicitaire) {
        try {
            publicitaireService.updateStatus(publicitaire);
        } catch (Exception e) {
            throw new univ.rouen.camping.exception.InternalError();
        }
    }

    @RequestMapping(value = "/publicitaire/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        publicitaireService.delete(id);
    }
}
