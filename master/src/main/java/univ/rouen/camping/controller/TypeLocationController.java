package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.TypeLocation;
import univ.rouen.camping.service.TypeLocationService;

import java.util.List;

@Controller
public class TypeLocationController {
    @Autowired
    private TypeLocationService typeLocationService;

    @RequestMapping(value = "/typeLocation", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TypeLocation> list() {
        return typeLocationService.getAll();
    }

    @RequestMapping(value = "/typeLocation/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody TypeLocation getById(@PathVariable long id) {
        return typeLocationService.getById(id);
    }

    @RequestMapping(value = "/typeLocation/nom/{name}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody TypeLocation getByName(@PathVariable String name) {
        return typeLocationService.getByName(name);
    }

    @RequestMapping(value = "/typeLocation", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody TypeLocation typeLocation) {
        typeLocationService.create(typeLocation);
    }

    @RequestMapping(value = "/typeLocation", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody TypeLocation typeLocation) {
        typeLocationService.update(typeLocation);
    }

    @RequestMapping(value = "/typeLocation/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        typeLocationService.delete(id);
    }
}
