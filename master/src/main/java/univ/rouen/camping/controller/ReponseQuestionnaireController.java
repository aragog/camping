package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.ReponseQuestionnaire;
import univ.rouen.camping.service.ReponseQuestionnaireService;

import java.util.List;

@Controller
public class ReponseQuestionnaireController {
    @Autowired
    private ReponseQuestionnaireService reponseQuestionnaireService;

    @RequestMapping(value = "/reponseQuestionnaire", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ReponseQuestionnaire> list() {
        return reponseQuestionnaireService.getAll();
    }

    @RequestMapping(value = "/reponseQuestionnaire/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ReponseQuestionnaire getById(@PathVariable long id) {
        return reponseQuestionnaireService.getById(id);
    }

    @RequestMapping(value = "/reponseQuestionnaire", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody ReponseQuestionnaire reponseQuestionnaire) {
        reponseQuestionnaireService.create(reponseQuestionnaire);
    }

    @RequestMapping(value = "/reponseQuestionnaire", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody ReponseQuestionnaire reponseQuestionnaire) {
        reponseQuestionnaireService.update(reponseQuestionnaire);
    }

    @RequestMapping(value = "/reponseQuestionnaire/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        reponseQuestionnaireService.delete(id);
    }
}
