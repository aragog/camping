package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Ville;
import univ.rouen.camping.service.VilleService;

import java.util.List;

@Controller
public class VilleController {
    @Autowired
    private VilleService villeService;

    @RequestMapping(value = "/ville", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Ville> list() {
        return villeService.getAll();
    }

    @RequestMapping(value = "/ville/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Ville getById(@PathVariable long id) {
        return villeService.getById(id);
    }

    @RequestMapping(value = "/ville", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Ville ville) {
        villeService.create(ville);
    }

    @RequestMapping(value = "/ville", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Ville ville) {
        villeService.update(ville);
    }

    @RequestMapping(value = "/ville/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        villeService.delete(id);
    }

    @RequestMapping(value = "/ville/CP/{cp}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Ville> searchByCP(@PathVariable String cp) {
        return villeService.searchByCP(cp);
    }

    @RequestMapping(value = "/ville/name/{name}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Ville> searchByName(@PathVariable String name) {
        return villeService.searchByName(name);
    }
}