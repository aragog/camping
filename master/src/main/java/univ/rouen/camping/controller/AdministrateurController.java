package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.Administrateur;
import univ.rouen.camping.service.AdministrateurService;

import java.util.List;

@Controller
public class AdministrateurController {
    @Autowired
    private AdministrateurService administrateurService;

    @RequestMapping(value = "/administrateur", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Administrateur> list() {
        return administrateurService.getAll();
    }

    @RequestMapping(value = "/administrateur/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Administrateur getById(@PathVariable long id) {
        return administrateurService.getById(id);
    }

    @RequestMapping(value = "/administrateur", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Administrateur administrateur) {
        try {
            administrateurService.create(administrateur);
        } catch (Exception e) {
            throw new univ.rouen.camping.exception.InternalError();
        }
    }

    @RequestMapping(value = "/administrateur", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Administrateur administrateur) {
        administrateurService.update(administrateur);
    }

    @RequestMapping(value = "/administrateur/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        administrateurService.delete(id);
    }
}
