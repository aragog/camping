package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.AnnoncePublicitaire;
import univ.rouen.camping.service.AnnoncePublicitaireService;

import java.util.List;

@Controller
public class AnnoncePublicitaireController {
    @Autowired
    private AnnoncePublicitaireService annoncePublicitaireService;

    @RequestMapping(value = "/annoncePublicitaire", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AnnoncePublicitaire> list() {
        return annoncePublicitaireService.getAll();
    }

    @RequestMapping(value = "/annoncePublicitaire/etat/{etat}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AnnoncePublicitaire> list(@PathVariable String etat) {
        return annoncePublicitaireService.getAll(etat);
    }

    @RequestMapping(value = "/annoncePublicitaire/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody AnnoncePublicitaire getById(@PathVariable long id) {
        return annoncePublicitaireService.getById(id);
    }

    @RequestMapping(value = "/annoncePublicitaire", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody AnnoncePublicitaire annoncePublicitaire) {
        annoncePublicitaireService.create(annoncePublicitaire);
    }

    @RequestMapping(value = "/annoncePublicitaire", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody AnnoncePublicitaire annoncePublicitaire) {
        annoncePublicitaireService.update(annoncePublicitaire);
    }

    @RequestMapping(value = "/annoncePublicitaire/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        annoncePublicitaireService.delete(id);
    }

    @RequestMapping(value = "/annoncePublicitaire/displayable", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody AnnoncePublicitaire adToDisplay() {
        return annoncePublicitaireService.adToDisplay();
    }
}
