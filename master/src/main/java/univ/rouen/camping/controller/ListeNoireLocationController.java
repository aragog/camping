package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.ListeNoireLocation;
import univ.rouen.camping.service.ListeNoireLocationService;

import java.util.List;

@Controller
public class ListeNoireLocationController {
    @Autowired
    private ListeNoireLocationService listeNoireLocationService;

    @RequestMapping(value = "/listeNoireLocation", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListeNoireLocation> list() {
        return listeNoireLocationService.getAll();
    }

    @RequestMapping(value = "/listeNoireLocation/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ListeNoireLocation getById(@PathVariable long id) {
        return listeNoireLocationService.getById(id);
    }

    @RequestMapping(value = "/listeNoireLocation", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody ListeNoireLocation listeNoireLocation) {
        listeNoireLocationService.create(listeNoireLocation);
    }

    @RequestMapping(value = "/listeNoireLocation", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody ListeNoireLocation listeNoireLocation) {
        listeNoireLocationService.update(listeNoireLocation);
    }

    @RequestMapping(value = "/listeNoireLocation/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        listeNoireLocationService.delete(id);
    }
}
