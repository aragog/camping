package univ.rouen.camping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.model.AvisLocation;
import univ.rouen.camping.service.AvisLocationService;

import java.util.List;

@Controller
public class AvisLocationController {
    @Autowired
    private AvisLocationService avislocationService;

    @RequestMapping(value = "/avislocation", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AvisLocation> list() {
        return avislocationService.getAll();
    }

    @RequestMapping(value = "/avislocation/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody AvisLocation getById(@PathVariable long id) {
        return avislocationService.getById(id);
    }

    @RequestMapping(value = "/avislocation", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody AvisLocation avislocation) {
        avislocationService.create(avislocation);
    }

    @RequestMapping(value = "/avislocation", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody AvisLocation avislocation) {
        avislocationService.update(avislocation);
    }

    @RequestMapping(value = "/avislocation/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        avislocationService.delete(id);
    }
}
