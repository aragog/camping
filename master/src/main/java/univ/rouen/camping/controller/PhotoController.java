package univ.rouen.camping.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import univ.rouen.camping.exception.InvalidResize;
import univ.rouen.camping.model.Photo;
import univ.rouen.camping.service.PhotoService;
import univ.rouen.camping.utils.ImageBase64Utils;

import java.io.IOException;
import java.util.List;

@Controller
public class PhotoController {
    @Autowired
    private PhotoService photoService;

    @RequestMapping(value = "/photo", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Photo> list() {
        return photoService.getAll();
    }

    @RequestMapping(value = "/photo/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Photo getById(@PathVariable long id) {
        return photoService.getById(id);
    }

    @RequestMapping(value = "/photo", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void create(@RequestBody Photo photo) {
        photoService.create(photo);
    }

    @RequestMapping(value = "/photo", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody Photo photo) {
        photoService.update(photo);
    }

    @RequestMapping(value = "/photo/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        photoService.delete(id);
    }

    @RequestMapping(value = "/photo/resize", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String resize(@RequestBody String stringImage) {
        final int width = 400;
        final int height = 300;

        try {
            return ImageBase64Utils.resizeBase64URI(stringImage, width, height, null);
        } catch (IOException e) {
            throw new InvalidResize();
        }
    }
}
