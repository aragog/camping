package univ.rouen.camping.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Publicitaire implements Serializable {
    private Long id;
    private String lastname;
    private String firstname;
    private String email;
    private String phonenumber;
    private String password;
    private String pseudo;
    private String salt;
    private Collection<AnnoncePublicitaire> publicites;

    public Publicitaire() {
    }

    public Publicitaire(Long id, String lastname, String firstname,
                        String email, String phonenumber, String password,
                        String pseudo) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.phonenumber = phonenumber;
        this.password = password;
        this.pseudo = pseudo;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<AnnoncePublicitaire> getPublicites() {
        return publicites;
    }

    public void setPublicites(Collection<AnnoncePublicitaire> publicites) {
        this.publicites = publicites;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Publicitaire{");
        sb.append("id=").append(id);
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", phonenumber='").append(phonenumber).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", pseudo='").append(pseudo).append('\'');
        sb.append(", salt='").append(salt).append('\'');
        sb.append(", publicites=").append(publicites);
        sb.append('}');
        return sb.toString();
    }
}
