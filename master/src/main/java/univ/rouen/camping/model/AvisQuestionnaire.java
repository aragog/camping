package univ.rouen.camping.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class AvisQuestionnaire implements Serializable {
    private Long id;
    private Collection<ReponseQuestionnaire> reponseQuestionnaire;


    public AvisQuestionnaire() {
    }

    public AvisQuestionnaire(Collection<ReponseQuestionnaire> reponseQuestionnaire) {
        this.reponseQuestionnaire = reponseQuestionnaire;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<ReponseQuestionnaire> getReponseQuestionnaire() {
        return reponseQuestionnaire;
    }

    public void setReponseQuestionnaire(Collection<ReponseQuestionnaire> reponseQuestionnaire) {
        this.reponseQuestionnaire = reponseQuestionnaire;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AvisQuestionnaire{");
        sb.append("id=").append(id);
        sb.append(", reponseQuestionnaire=").append(reponseQuestionnaire);
        sb.append('}');
        return sb.toString();
    }
}
