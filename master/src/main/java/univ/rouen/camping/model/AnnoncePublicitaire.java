package univ.rouen.camping.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class AnnoncePublicitaire {
    public Long id;
    public String url;
    private Date start;
    private Date end;
    private Photo photo;
    private Etat etat;
    private Long publicitaireId;

    public AnnoncePublicitaire() {
    }

    public AnnoncePublicitaire(String url, Date start, Date end) {
        this.url = url;
        this.start = start;
        this.end = end;
    }

    public AnnoncePublicitaire(String url, Date start, Date end, Photo photo) {
        this.url = url;
        this.start = start;
        this.end = end;
        this.photo = photo;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @OneToOne(cascade = CascadeType.ALL)
    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    @ManyToOne
    @JoinColumn(name = "etat")
    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Long getPublicitaireId() {
        return publicitaireId;
    }

    public void setPublicitaireId(Long publicitaireId) {
        this.publicitaireId = publicitaireId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AnnoncePublicitaire{");
        sb.append("id=").append(id);
        sb.append(", url='").append(url).append('\'');
        sb.append(", start=").append(start);
        sb.append(", end=").append(end);
        sb.append(", photo=").append(photo);
        sb.append(", etat=").append(etat);
        sb.append('}');
        return sb.toString();
    }
}
