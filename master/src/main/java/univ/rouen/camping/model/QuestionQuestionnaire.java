package univ.rouen.camping.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class QuestionQuestionnaire implements Serializable {
    private Long id;
    private String question;

    public QuestionQuestionnaire(String question) {
        this.question = question;
    }

    public QuestionQuestionnaire() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("QuestionQuestionnaire{");
        sb.append("id=").append(id);
        sb.append(", question='").append(question).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
