package univ.rouen.camping.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

public class SessionUnit implements Serializable {
    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Role role;

    public SessionUnit() {
    }

    public SessionUnit(Long id, Role role) {
        this.id = id;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
