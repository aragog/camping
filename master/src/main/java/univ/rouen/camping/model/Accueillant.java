package univ.rouen.camping.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Accueillant implements Serializable {
    private Long id;
    private String lastname;
    private String firstname;
    private String email;
    private String phonenumber;
    private String password;
    private String pseudo;
    private String salt;
    private Collection<AnnonceLocation> annonceLocations;
    private Collection<Avertissement> avertissements;
    private ListeNoireAccueillant listeNoireAccueillant;


    public Accueillant() {

    }

    public Accueillant(String lastname, String firstname, String email, String phonenumber, String password, String pseudo) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.phonenumber = phonenumber;
        this.password = password;
        this.pseudo = pseudo;
    }

    public Accueillant(String lastname, String firstname, String email, String phonenumber,
                       String password, String pseudo, Collection<AnnonceLocation> annonceLocations) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.phonenumber = phonenumber;
        this.password = password;
        this.pseudo = pseudo;
        this.annonceLocations = annonceLocations;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(nullable = false)
    public String getLastname() {
        return lastname;
    }
    
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    @Column(nullable = false)
    public String getFirstname() {
        return firstname;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    @Column(nullable = false)
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPhonenumber() {
        return phonenumber;
    }
    
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    
    @Column(nullable = false)
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Column(nullable = false)
    public String getPseudo() {
        return pseudo;
    }
    
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<AnnonceLocation> getAnnonceLocations() {
        return annonceLocations;
    }

    public void setAnnonceLocations(Collection<AnnonceLocation> locations) {
        this.annonceLocations = locations;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<Avertissement> getAvertissements() {
        return avertissements;
    }

    public void setAvertissements(Collection<Avertissement> avertissements) {
        this.avertissements = avertissements;
    }

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    public ListeNoireAccueillant getListeNoireAccueillant() {
        return listeNoireAccueillant;
    }

    public void setListeNoireAccueillant(ListeNoireAccueillant listeNoireAccueillant) {
        this.listeNoireAccueillant = listeNoireAccueillant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Accueillant that = (Accueillant) o;

        if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (lastname != null ? !lastname.equals(that.lastname) : that.lastname != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (pseudo != null ? !pseudo.equals(that.pseudo) : that.pseudo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (pseudo != null ? pseudo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Accueillant{");
        sb.append("id=").append(id);
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", phonenumber='").append(phonenumber).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", pseudo='").append(pseudo).append('\'');
        sb.append(", salt='").append(salt).append('\'');
        sb.append(", annonceLocations=").append(annonceLocations);
        sb.append(", avertissements=").append(avertissements);
        sb.append(", listeNoireAccueillant=").append(listeNoireAccueillant);
        sb.append('}');
        return sb.toString();
    }
}
