package univ.rouen.camping.model;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.UUID;
@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionBean implements Serializable {
	private final String id = UUID.randomUUID().toString();
    private SessionUnit sessionUnit;

    public SessionUnit getSessionUnit() {
        return sessionUnit;
    }

    public void setSessionUnit(SessionUnit sessionUnit) {
        this.sessionUnit = sessionUnit;
    }

    /**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
}
