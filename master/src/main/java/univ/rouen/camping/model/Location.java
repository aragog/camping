package univ.rouen.camping.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Location implements Serializable {
    private Long id;
    private Double prix;
    private Integer places;
    private TypeLocation type;

    public Location() {

    }

    public Location(Double prix, Integer places, TypeLocation type) {
        this.prix = prix;
        this.places = places;
        this.type = type;
    }

    public Location(TypeLocation type) {
        this.type = type;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Integer getPlaces() {
        return places;
    }

    public void setPlaces(Integer places) {
        this.places = places;
    }

    @ManyToOne
    @JoinColumn(name = "type")
    public TypeLocation getType() {
        return type;
    }

    public void setType(TypeLocation type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (id != null ? !id.equals(location.id) : location.id != null) return false;
        if (places != null ? !places.equals(location.places) : location.places != null) return false;
        if (prix != null ? !prix.equals(location.prix) : location.prix != null) return false;
        if (type != null ? !type.equals(location.type) : location.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (prix != null ? prix.hashCode() : 0);
        result = 31 * result + (places != null ? places.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
