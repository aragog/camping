package univ.rouen.camping.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class ListeNoireAccueillant implements Serializable {
    private Long id;
    private String motif;
    private Date fin;

    public ListeNoireAccueillant(String motif) {
        this.motif = motif;
    }

    public ListeNoireAccueillant() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListeNoireAccueillant that = (ListeNoireAccueillant) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (motif != null ? !motif.equals(that.motif) : that.motif != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (motif != null ? motif.hashCode() : 0);
        return result;
    }
}
