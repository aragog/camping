package univ.rouen.camping.model;

import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class AnnonceLocationFilter implements Serializable {
    private String codePostal;
    private String ville;

//    //J'ai supposé un seul type possible par requête, choisi dans l'IHM par bouton radio
//    //Utiliser une collection et modifier AnnonceLocationDAO si choix multipe par check boxes
    private TypeLocation type;

    private Double prixMin;
    private Double prixMax;

    private Collection<Service> services;

    //Les offres situées sur ces dates seront les seules à pouvoir être affichées
    private Date dateDebut;
    private Date dateFin;

    private int nbPlaces;

    public AnnonceLocationFilter() {
    }


    public AnnonceLocationFilter(String codePostal, String ville, TypeLocation type,
                                 Double prixMin, Double prixMax, Collection<Service> services,
                                 Date dateDebut, Date dateFin, int nbPlaces) {
        this.codePostal = codePostal;
        this.ville = ville;
        this.type = type;
        this.prixMin = prixMin;
        this.prixMax = prixMax;
        this.services = services;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.nbPlaces = nbPlaces;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public TypeLocation getType() {
        return type;
    }

    public void setType(TypeLocation type) {
        this.type = type;
    }


    public int getNbPlaces() {
        return nbPlaces;
    }

    public void setNbPlaces(int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }

    public Double getPrixMin() {
        return prixMin;
    }

    public void setPrixMin(Double prixMin) {
        this.prixMin = prixMin;
    }

    public Double getPrixMax() {
        return prixMax;
    }

    public void setPrixMax(Double prixMax) {
        this.prixMax = prixMax;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @ManyToMany
    public Collection<Service> getServices() {
        return services;
    }

    public void setServices(Collection<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AnnonceLocationFilter{");
        sb.append("codePostal='").append(codePostal).append('\'');
        sb.append(", ville='").append(ville).append('\'');
        sb.append(", type=").append(type);
        sb.append(", prixMin=").append(prixMin);
        sb.append(", prixMax=").append(prixMax);
        sb.append(", services=").append(services);
        sb.append(", dateDebut=").append(dateDebut);
        sb.append(", dateFin=").append(dateFin);
        sb.append(", nbPlaces=").append(nbPlaces);
        sb.append('}');
        return sb.toString();
    }
}
