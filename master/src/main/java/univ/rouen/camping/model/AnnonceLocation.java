package univ.rouen.camping.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class AnnonceLocation implements Serializable {
    private Long id;
    private String description;
    private String adresse;
    private String complementAdresse;
    private String codePostal;
    private String telephone;
    private Ville ville;
    private Etat etat;
    private Long accueillantId;
    private Collection<Location> locations;
    private Collection<Photo> photos;
    private Collection<Service> services;
    private Collection<Activite> activites;
    private Collection<Planning> plannings;
    private Collection<AvisLocation> avis;
    private Collection<AvisQuestionnaire> questionnaires;
    private ListeNoireLocation listeNoireLocation;

    public AnnonceLocation(Long id, String description, String adresse, String complementAdresse,
                           String codePostal, Ville ville, Etat etat, Long accueillantId,
                           Collection<Location> locations, Collection<Photo> photos,
                           Collection<Service> services, Collection<Activite> activites,
                           Collection<Planning> plannings, Collection<AvisLocation> avis) {
        this.id = id;
        this.description = description;
        this.adresse = adresse;
        this.complementAdresse = complementAdresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.etat = etat;
        this.accueillantId = accueillantId;
        this.locations = locations;
        this.photos = photos;
        this.services = services;
        this.activites = activites;
        this.plannings = plannings;
        this.avis = avis;
    }

    public AnnonceLocation(String description, String adresse, String complementAdresse,
                           String codePostal, Ville ville, Collection<Location> locations,
                           Collection<Photo> photos, Collection<Service> services) {
        this.description = description;
        this.adresse = adresse;
        this.complementAdresse = complementAdresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.locations = locations;
        this.photos = photos;
        this.services = services;
    }

    public AnnonceLocation(Long id, Ville ville, Collection<Service> services, Collection<Activite> activites) {
        this.id = id;
        this.ville = ville;
        this.services = services;
        this.activites = activites;
    }

    public AnnonceLocation(Collection<Location> locations, Collection<Photo> photos) {
        this.locations = locations;
        this.photos = photos;
    }

    public AnnonceLocation() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getComplementAdresse() {
        return complementAdresse;
    }

    public void setComplementAdresse(String complementAdresse) {
        this.complementAdresse = complementAdresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @ManyToOne
    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public Collection<Location> getLocations() {
        return locations;
    }

    public void setLocations(Collection<Location> locations) {
        this.locations = locations;
    }

    @ManyToOne
    @JoinColumn(name = "etat")
    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Long getAccueillantId() {
        return accueillantId;
    }

    public void setAccueillantId(Long accueillantId) {
        this.accueillantId = accueillantId;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Collection<Photo> photos) {
        this.photos = photos;
    }

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<Service> getServices() {
        return services;
    }

    public void setServices(Collection<Service> services) {
        this.services = services;
    }

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<Activite> getActivites() {
        return activites;
    }

    public void setActivites(Collection<Activite> activites) {
        this.activites = activites;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<Planning> getPlannings() {
        return plannings;
    }

    public void setPlannings(Collection<Planning> plannings) {
        this.plannings = plannings;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<AvisLocation> getAvis() {
        return avis;
    }

    public void setAvis(Collection<AvisLocation> avis) {
        this.avis = avis;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<AvisQuestionnaire> getQuestionnaires() {
        return questionnaires;
    }

    public void setQuestionnaires(Collection<AvisQuestionnaire> questionnaires) {
        this.questionnaires = questionnaires;
    }

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    public ListeNoireLocation getListeNoireLocation() {
        return listeNoireLocation;
    }

    public void setListeNoireLocation(ListeNoireLocation listeNoireLocation) {
        this.listeNoireLocation = listeNoireLocation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AnnonceLocation{");
        sb.append("id=").append(id);
        sb.append(", description='").append(description).append('\'');
        sb.append(", adresse='").append(adresse).append('\'');
        sb.append(", complementAdresse='").append(complementAdresse).append('\'');
        sb.append(", codePostal='").append(codePostal).append('\'');
        sb.append(", telephone='").append(telephone).append('\'');
        sb.append(", ville=").append(ville);
        sb.append(", etat=").append(etat);
        sb.append(", accueillantId=").append(accueillantId);
        sb.append(", locations=").append(locations);
        sb.append(", photos=").append(photos);
        sb.append(", services=").append(services);
        sb.append(", activites=").append(activites);
        sb.append(", plannings=").append(plannings);
        sb.append(", avis=").append(avis);
        sb.append(", questionnaires=").append(questionnaires);
        sb.append(", listeNoireLocation=").append(listeNoireLocation);
        sb.append('}');
        return sb.toString();
    }
}
