package univ.rouen.camping.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class ListeNoireLocation implements Serializable {
    private Long id;
    private String motif;
    private Date fin;

    public ListeNoireLocation(String motif) {
        this.motif = motif;
    }

    public ListeNoireLocation() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }
}
