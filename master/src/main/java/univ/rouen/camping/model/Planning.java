package univ.rouen.camping.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Planning implements Serializable {
    private Long id;
    private Date start;
    private Date end;

    public Planning(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public Planning() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Planning planning = (Planning) o;

        if (end != null ? !end.equals(planning.end) : planning.end != null) return false;
        if (id != null ? !id.equals(planning.id) : planning.id != null) return false;
        if (start != null ? !start.equals(planning.start) : planning.start != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }
}
