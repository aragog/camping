package univ.rouen.camping.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class ReponseQuestionnaire implements Serializable {
    private Long id;
    private Integer reponse;
    private QuestionQuestionnaire questionQuestionnaire;

    public ReponseQuestionnaire(Integer reponse, QuestionQuestionnaire questionQuestionnaire) {
        this.reponse = reponse;
        this.questionQuestionnaire = questionQuestionnaire;
    }

    public ReponseQuestionnaire() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getReponse() {
        return reponse;
    }

    public void setReponse(Integer reponse) {
        this.reponse = reponse;
    }

    @ManyToOne
    public QuestionQuestionnaire getQuestionQuestionnaire() {
        return questionQuestionnaire;
    }

    public void setQuestionQuestionnaire(QuestionQuestionnaire questionQuestionnaire) {
        this.questionQuestionnaire = questionQuestionnaire;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReponseQuestionnaire{");
        sb.append("id=").append(id);
        sb.append(", reponse=").append(reponse);
        sb.append(", questionQuestionnaire=").append(questionQuestionnaire);
        sb.append('}');
        return sb.toString();
    }
}
