package univ.rouen.camping.model;

import com.fasterxml.jackson.annotation.JsonFormat;


@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum Role {
    ACCUEILLANT, ADMINISTRATEUR, PUBLICITAIRE, VISITEUR;
}
