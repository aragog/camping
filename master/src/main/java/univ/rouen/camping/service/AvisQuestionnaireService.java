package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AvisQuestionnaireDAO;
import univ.rouen.camping.model.AvisQuestionnaire;

import java.util.List;

@Service
public class AvisQuestionnaireService {
    @Autowired
    private AvisQuestionnaireDAO simpleEntityManager;

    @Transactional
    public AvisQuestionnaire getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<AvisQuestionnaire> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(AvisQuestionnaire resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(AvisQuestionnaire resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
