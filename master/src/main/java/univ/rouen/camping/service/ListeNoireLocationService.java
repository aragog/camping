package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.ListeNoireLocationDAO;
import univ.rouen.camping.model.ListeNoireLocation;

import java.util.List;

@Service
public class ListeNoireLocationService {
    @Autowired
    private ListeNoireLocationDAO simpleEntityManager;

    @Transactional
    public ListeNoireLocation getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<ListeNoireLocation> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(ListeNoireLocation resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(ListeNoireLocation resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
