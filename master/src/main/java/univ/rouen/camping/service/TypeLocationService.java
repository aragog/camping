package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.TypeLocationDAO;
import univ.rouen.camping.model.TypeLocation;

import java.util.List;

@Service
public class TypeLocationService {
    @Autowired
    private TypeLocationDAO simpleEntityManager;

    @Transactional()
    public TypeLocation getById(Long id) {
        return simpleEntityManager.getById(id);
    }
    @Transactional
    public TypeLocation getByName(String name) {
        List<TypeLocation> types = getAll();
        for (TypeLocation type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Transactional
    public List<TypeLocation> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(TypeLocation resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(TypeLocation resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
