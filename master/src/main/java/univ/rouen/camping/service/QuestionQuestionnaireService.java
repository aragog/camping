package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.QuestionQuestionnaireDAO;
import univ.rouen.camping.model.QuestionQuestionnaire;

import java.util.List;

@Service
public class QuestionQuestionnaireService {
    @Autowired
    private QuestionQuestionnaireDAO simpleEntityManager;

    @Transactional
    public QuestionQuestionnaire getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<QuestionQuestionnaire> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(QuestionQuestionnaire resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(QuestionQuestionnaire resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
