package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AvertissementDAO;
import univ.rouen.camping.model.Avertissement;

import java.util.List;

@Service
public class AvertissementService {
    @Autowired
    private AvertissementDAO simpleEntityManager;

    @Transactional
    public Avertissement getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Avertissement> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Avertissement resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Avertissement resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
