package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AvisLocationDAO;
import univ.rouen.camping.model.AvisLocation;

import java.util.List;

@Service
public class AvisLocationService {
    @Autowired
    private AvisLocationDAO simpleEntityManager;

    @Transactional
    public AvisLocation getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<AvisLocation> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(AvisLocation resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(AvisLocation resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}