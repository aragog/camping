package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AnnoncePublicitaireDAO;
import univ.rouen.camping.model.AnnoncePublicitaire;

import java.util.List;

@Service
public class AnnoncePublicitaireService {
    @Autowired
    private AnnoncePublicitaireDAO simpleEntityManager;

    @Transactional
    public AnnoncePublicitaire getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<AnnoncePublicitaire> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public List<AnnoncePublicitaire> getAll(String etat) {
        return simpleEntityManager.getAll(etat);
    }

    @Transactional
    public void update(AnnoncePublicitaire resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(AnnoncePublicitaire resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }

    @Transactional
    public AnnoncePublicitaire adToDisplay() {
        return simpleEntityManager.adToDisplay();
    }
}
