package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AccueillantDAO;
import univ.rouen.camping.model.Accueillant;
import univ.rouen.camping.model.Avertissement;
import univ.rouen.camping.utils.CypherUtil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service
public class AccueillantService {
    @Autowired
    private AccueillantDAO simpleEntityManager;

    @Transactional
    public Accueillant getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Accueillant> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Accueillant resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public void updateStatus(Accueillant resource) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        CypherUtil.CipherItem cipherItem =
                CypherUtil.makeCiphering(resource.getPassword());

        resource.setPassword(cipherItem.getDigest());
        resource.setSalt(cipherItem.getSalt());

        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Accueillant resource) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        CypherUtil.CipherItem cipherItem =
                CypherUtil.makeCiphering(resource.getPassword());

        resource.setPassword(cipherItem.getDigest());
        resource.setSalt(cipherItem.getSalt());

        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }

    @Transactional
    public boolean usernameIsFree(String username) {
        return simpleEntityManager.usernameIsFree(username);
    }

    @Transactional
    public Long connect(String username, String password) throws IOException, NoSuchAlgorithmException {
        Accueillant accueillant = simpleEntityManager.connect(username);
        boolean userExists = (accueillant != null);
        CypherUtil.DecipherItem decipherItem;

        if (userExists) {
            decipherItem = CypherUtil.makeDeciphering(
                    accueillant.getPassword(), accueillant.getSalt(), password, true);
        } else {
            decipherItem = CypherUtil.makeDeciphering(null, null, password, false);
        }

        if (Arrays.equals(decipherItem.getProposedDigest(), decipherItem.getDigest()) && userExists) {
            return accueillant.getId();
        } else {
            return null;
        }
    }

    @Transactional
    public Collection<Avertissement> getAvertos(Long id) {
        return simpleEntityManager.getAvertos(id);
    }
}
