package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AnnonceLocationDAO;
import univ.rouen.camping.model.AnnonceLocation;
import univ.rouen.camping.model.AnnonceLocationFilter;
import univ.rouen.camping.model.Ville;

import java.util.List;

@Service
public class AnnonceLocationService {
    @Autowired
    private AnnonceLocationDAO simpleEntityManager;

    @Transactional
    public AnnonceLocation getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public AnnonceLocation getByIdEagerly(Long id) {
        return simpleEntityManager.getByIdEagerly(id);
    }

    @Transactional
    public List<AnnonceLocation> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public List<AnnonceLocation> getAll(String etat) {
        return simpleEntityManager.getAll(etat);
    }

    @Transactional
    public List<AnnonceLocation> getAllLocationDisponibles() {
        return simpleEntityManager.getAllLocationDisponibles();
    }

    @Transactional
    public int nbLocationOfCity(Ville ville) {
        return simpleEntityManager.nbLocationOfCity(ville);
    }

    @Transactional
    public void update(AnnonceLocation resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(AnnonceLocation resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }

    @Transactional
    public List<AnnonceLocation> filteredGetAll(AnnonceLocationFilter annonceLocationFilter) {
        return simpleEntityManager.filteredGetAll(annonceLocationFilter);
    }

    @Transactional
    public List<AnnonceLocation> getAllFromDistance(double distance, double lat, double lon) {
        return simpleEntityManager.getAllFromDistance(distance, lat, lon);
    }
}
