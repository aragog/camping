package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.EtatDAO;
import univ.rouen.camping.model.Etat;

import java.util.List;

@Service
public class EtatService {
    @Autowired
    private EtatDAO simpleEntityManager;

    @Transactional
    public Etat getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Etat> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public Etat getByName(String name) {
        return simpleEntityManager.getByName(name);
    }

    @Transactional
    public void update(Etat resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Etat resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
