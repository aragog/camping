package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.PhotoDAO;
import univ.rouen.camping.model.Photo;

import java.util.List;

@Service
public class PhotoService {
    @Autowired
    private PhotoDAO simpleEntityManager;

    @Transactional
    public Photo getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Photo> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Photo resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Photo resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
