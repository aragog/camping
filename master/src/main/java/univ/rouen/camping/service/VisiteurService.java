package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.VisiteurDAO;
import univ.rouen.camping.model.Visiteur;

import java.util.List;

@Service
public class VisiteurService {
    @Autowired
    private VisiteurDAO simpleEntityManager;

    @Transactional
    public Visiteur getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Visiteur> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Visiteur resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Visiteur resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
