package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.PlanningDAO;
import univ.rouen.camping.model.Planning;

import java.util.List;

@Service
public class PlanningService {
    @Autowired
    private PlanningDAO simpleEntityManager;

    @Transactional
    public Planning getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Planning> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Planning resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Planning resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
