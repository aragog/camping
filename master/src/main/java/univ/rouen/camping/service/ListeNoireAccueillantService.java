package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.ListeNoireAccueillantDAO;
import univ.rouen.camping.model.ListeNoireAccueillant;

import java.util.List;

@Service
public class ListeNoireAccueillantService {
    @Autowired
    private ListeNoireAccueillantDAO simpleEntityManager;

    @Transactional
    public ListeNoireAccueillant getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<ListeNoireAccueillant> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(ListeNoireAccueillant resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(ListeNoireAccueillant resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
