package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.AdministrateurDAO;
import univ.rouen.camping.model.Administrateur;
import univ.rouen.camping.utils.CypherUtil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

@Service
public class AdministrateurService {
    @Autowired
    private AdministrateurDAO simpleEntityManager;

    @Transactional
    public Administrateur getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Administrateur> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Administrateur resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Administrateur resource) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        CypherUtil.CipherItem cipherItem =
                CypherUtil.makeCiphering(resource.getPassword());

        resource.setPassword(cipherItem.getDigest());
        resource.setSalt(cipherItem.getSalt());

        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }

    @Transactional
    public boolean usernameIsFree(String username) {
        return simpleEntityManager.usernameIsFree(username);
    }

    @Transactional
    public Long connect(String username, String password) throws IOException, NoSuchAlgorithmException {
        Administrateur administrateur = simpleEntityManager.connect(username);
        boolean userExists = (administrateur != null);
        CypherUtil.DecipherItem decipherItem;

        if (userExists) {
            decipherItem = CypherUtil.makeDeciphering(
                    administrateur.getPassword(), administrateur.getSalt(), password, true);
        } else {
            decipherItem = CypherUtil.makeDeciphering(null, null, password, false);
        }

        if (Arrays.equals(decipherItem.getProposedDigest(), decipherItem.getDigest()) && userExists) {
            return administrateur.getId();
        } else {
            return null;
        }
    }
}

