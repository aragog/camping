package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.ReponseQuestionnaireDAO;
import univ.rouen.camping.model.ReponseQuestionnaire;

import java.util.List;

@Service
public class ReponseQuestionnaireService {
    @Autowired
    private ReponseQuestionnaireDAO simpleEntityManager;

    @Transactional
    public ReponseQuestionnaire getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<ReponseQuestionnaire> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(ReponseQuestionnaire resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(ReponseQuestionnaire resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
