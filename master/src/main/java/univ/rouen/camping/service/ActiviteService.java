package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.ActiviteDAO;
import univ.rouen.camping.model.Activite;

import java.util.List;

@Service
public class ActiviteService {
    @Autowired
    private ActiviteDAO simpleEntityManager;

    @Transactional
    public Activite getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Activite> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Activite resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Activite resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
