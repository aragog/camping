package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.PublicitaireDAO;
import univ.rouen.camping.model.Publicitaire;
import univ.rouen.camping.utils.CypherUtil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

@Service
public class PublicitaireService {
    @Autowired
    private PublicitaireDAO simpleEntityManager;

    @Transactional
    public Publicitaire getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Publicitaire> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Publicitaire resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public void updateStatus(Publicitaire resource) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        CypherUtil.CipherItem cipherItem =
                CypherUtil.makeCiphering(resource.getPassword());

        resource.setPassword(cipherItem.getDigest());
        resource.setSalt(cipherItem.getSalt());

        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Publicitaire resource) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        CypherUtil.CipherItem cipherItem =
                CypherUtil.makeCiphering(resource.getPassword());

        resource.setPassword(cipherItem.getDigest());
        resource.setSalt(cipherItem.getSalt());

        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }

    @Transactional
    public boolean usernameIsFree(String username) {
        return simpleEntityManager.usernameIsFree(username);
    }

    @Transactional
    public Long connect(String username, String password) throws IOException, NoSuchAlgorithmException {
        Publicitaire publicitaire = simpleEntityManager.connect(username);
        boolean userExists = (publicitaire != null);
        CypherUtil.DecipherItem decipherItem;

        if (userExists) {
            decipherItem = CypherUtil.makeDeciphering(
                    publicitaire.getPassword(), publicitaire.getSalt(), password, true);
        } else {
            decipherItem = CypherUtil.makeDeciphering(null, null, password, false);
        }

        if (Arrays.equals(decipherItem.getProposedDigest(), decipherItem.getDigest()) && userExists) {
            return publicitaire.getId();
        } else {
            return null;
        }
    }
}
