package univ.rouen.camping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.LocationDAO;
import univ.rouen.camping.model.Location;

import java.util.List;

@Service
public class LocationService {
    @Autowired
    private LocationDAO simpleEntityManager;

    @Transactional
    public Location getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Location> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Location resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Location resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
