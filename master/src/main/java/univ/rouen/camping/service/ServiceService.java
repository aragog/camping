package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.ServiceDAO;
import univ.rouen.camping.model.Service;

import java.util.List;

@org.springframework.stereotype.Service
public class ServiceService {
    @Autowired
    private ServiceDAO simpleEntityManager;

    @Transactional
    public Service getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Service> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Service resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Service resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }
}
