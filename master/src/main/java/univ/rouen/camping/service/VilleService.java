package univ.rouen.camping.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import univ.rouen.camping.DAO.VilleDAO;
import univ.rouen.camping.model.Ville;

import java.util.List;

@Service
public class VilleService {
    @Autowired
    private VilleDAO simpleEntityManager;

    @Transactional
    public Ville getById(Long id) {
        return simpleEntityManager.getById(id);
    }

    @Transactional
    public List<Ville> getAll() {
        return simpleEntityManager.getAll();
    }

    @Transactional
    public void update(Ville resource) {
        simpleEntityManager.update(resource);
    }

    @Transactional
    public Long create(Ville resource) {
        return (Long) simpleEntityManager.create(resource);
    }

    @Transactional
    public void delete(Long id) {
        simpleEntityManager.delete(id);
    }

    @Transactional
    public List<Ville> searchByCP(String cp) {
        return simpleEntityManager.searchByCP(cp);
    }

    @Transactional
    public List<Ville> searchByName(String nom) {
        return simpleEntityManager.searchByName(nom);
    }
}
