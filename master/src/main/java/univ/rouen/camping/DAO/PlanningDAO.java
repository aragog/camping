package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Planning;

@Repository
public class PlanningDAO extends SimpleEntityManager<Planning, Long>{

    public PlanningDAO() {
        this(Planning.class);
    }

    public PlanningDAO(Class<Planning> entityClass) {
        super(entityClass);
    }

}
