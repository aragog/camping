package univ.rouen.camping.DAO;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Publicitaire;

import java.util.List;

@Repository
public class PublicitaireDAO extends SimpleEntityManager<Publicitaire, Long>{

    public PublicitaireDAO() {
        this(Publicitaire.class);
    }

    public PublicitaireDAO(Class<Publicitaire> entityClass) {
        super(entityClass);
    }

    public boolean usernameIsFree(String username) {
        return getSession().createCriteria(Publicitaire.class)
                .add(Restrictions.eq("pseudo", username))
                .list().isEmpty();
    }

    public Publicitaire connect(String username) {
        List<Publicitaire> publicitaires = getSession().createCriteria(Publicitaire.class)
                .add(Restrictions.eq("pseudo", username))
                .list();

        return (publicitaires.size() > 0) ? publicitaires.get(0) : null;
    }
}
