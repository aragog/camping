package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.ReponseQuestionnaire;

@Repository
public class ReponseQuestionnaireDAO extends SimpleEntityManager<ReponseQuestionnaire, Long>{

    public ReponseQuestionnaireDAO() {
        this(ReponseQuestionnaire.class);
    }

    public ReponseQuestionnaireDAO(Class<ReponseQuestionnaire> entityClass) {
        super(entityClass);
    }

}
