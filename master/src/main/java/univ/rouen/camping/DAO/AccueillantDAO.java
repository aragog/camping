package univ.rouen.camping.DAO;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Accueillant;
import univ.rouen.camping.model.Avertissement;

import java.util.Collection;
import java.util.List;

@Repository
public class AccueillantDAO extends SimpleEntityManager<Accueillant, Long>{

    public AccueillantDAO() {
        this(Accueillant.class);
    }
    
    public AccueillantDAO(Class<Accueillant> entityClass) {
        super(entityClass);
    }

    public boolean usernameIsFree(String username) {
        return getSession().createCriteria(Accueillant.class)
                .add(Restrictions.eq("pseudo", username))
                .list().isEmpty();
    }

    public Accueillant connect(String username) {
        List<Accueillant> accueillants = getSession().createCriteria(Accueillant.class)
                .add(Restrictions.eq("pseudo", username))
                .list();

        return (accueillants.size() > 0) ? accueillants.get(0) : null;
    }

    public Collection<Avertissement> getAvertos(Long id) {
        return getById(id).getAvertissements();
    }
}
