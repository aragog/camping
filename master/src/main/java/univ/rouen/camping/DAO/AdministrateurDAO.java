package univ.rouen.camping.DAO;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Administrateur;

import java.util.List;

@Repository
public class AdministrateurDAO extends SimpleEntityManager<Administrateur, Long>{

    public AdministrateurDAO() {
        this(Administrateur.class);
    }

    public AdministrateurDAO(Class<Administrateur> entityClass) {
        super(entityClass);
    }

    public boolean usernameIsFree(String username) {
        return getSession().createCriteria(Administrateur.class)
                .add(Restrictions.eq("pseudo", username))
                .list().isEmpty();
    }

    public Administrateur connect(String username) {
        List<Administrateur> administrateurs = getSession().createCriteria(Administrateur.class)
                .add(Restrictions.eq("pseudo", username))
                .list();

        return (administrateurs.size() > 0) ? administrateurs.get(0): null;
    }

}