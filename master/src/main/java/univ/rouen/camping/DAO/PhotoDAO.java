package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Photo;

@Repository
public class PhotoDAO extends SimpleEntityManager<Photo, Long>{

    public PhotoDAO() {
        this(Photo.class);
    }

    public PhotoDAO(Class<Photo> entityClass) {
        super(entityClass);
    }

}
