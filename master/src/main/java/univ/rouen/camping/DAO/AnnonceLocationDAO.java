package univ.rouen.camping.DAO;


import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.*;
import univ.rouen.camping.service.AccueillantService;
import univ.rouen.camping.service.ListeNoireAccueillantService;
import univ.rouen.camping.service.ListeNoireLocationService;
import univ.rouen.camping.utils.MailSender;

import javax.mail.MessagingException;
import java.util.*;

@Repository
public class AnnonceLocationDAO extends SimpleEntityManager<AnnonceLocation, Long>{
    @Autowired
    private AccueillantService accueillantService;
    @Autowired
    private ListeNoireLocationService listeNoireLocationService;
    @Autowired
    private ListeNoireAccueillantService listeNoireAccueillantService;

    public AnnonceLocationDAO() {
        this(AnnonceLocation.class);
    }

    public AnnonceLocationDAO(Class<AnnonceLocation> entityClass) {
        super(entityClass);
    }

    public int nbLocationOfCity(Ville ville) {
        return getSession().createCriteria(AnnonceLocation.class)
                .add(Restrictions.eq("ville", ville))
                .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list()
                .size();
    }

    /**
     * Retourne tous les locations dans un rayon défini par rapport
     *  à une position donnée
     * @param rayon Rayon
     * @param lat Coord. latitude de la position donnée
     * @param lon Coord. longitude de la position donnée
     * @return L'ensemble des locations contenues dans le rayon donné
     */
    public List<AnnonceLocation> getAllFromDistance(double rayon, double lat, double lon) {
        List<AnnonceLocation> all = getAll("Valider");
        List<AnnonceLocation> res = new LinkedList<>();

        for (AnnonceLocation a : all) {
            double lat2 = Double.valueOf(a.getVille().getLatitude());
            double lon2 = Double.valueOf(a.getVille().getLongitude());
            if (rayonBetween(lat, lon, lat2, lon2) <= rayon) {
                res.add(a);
            }
        }

        return res;
    }

    public AnnonceLocation getByIdEagerly(Long id) {
        AnnonceLocation annonceLocation = this.getById(id);
        annonceLocation.getPhotos().size();
        return annonceLocation;
    }

    /**
     * Retourne l'ensemble des offres de location d'état <code>etat</code>.
     * @param etat Etat de l'offre
     * @return Les offres correspondant à l'état
     */
    public List<AnnonceLocation> getAll(String etat) {
        return getSession().createCriteria(AnnonceLocation.class)
                .createCriteria("etat", "e")
                .add(Restrictions.eq("e.name", etat))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    // http://stackoverflow.com/questions/365826/calculate-rayon-between-2-gps-coordinates
    private Double rayonBetween(double lat1, double lon1, double lat2, double lon2) {
        final double R = 6371; // km
        final double p1 = Math.toRadians(lat1);
        final double p2 = Math.toRadians(lat2);
        final double d1 = Math.toRadians(lat2 - lat1);
        final double d2 = Math.toRadians(lon2 - lon1);

        final double a = Math.sin(d1 / 2) * Math.sin(d1 / 2)
                + Math.cos(p1) * Math.cos(p2)
                * Math.sin(d2 / 2) * Math.sin(d2 / 2);

        final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c;
    }

    private void checkBannissementAccueillantDates() {
        List<Accueillant> locations = accueillantService.getAll();
        List<Accueillant> aDebannir = new ArrayList<>();
        for (Accueillant a : locations) {
            if (a.getListeNoireAccueillant() != null) {
                ListeNoireAccueillant listeNoireAccueillant = a.getListeNoireAccueillant();
                Long finBan = listeNoireAccueillant.getFin().getTime();
                Long rightNow = new Date().getTime();
                if (rightNow > finBan) {
                    aDebannir.add(a);
                }
            }
        }

        for (Accueillant a : aDebannir) {
            try {
                MailSender.sendMessage("Débannissement",
                        "Votre compte utilisateur a été débanni",
                        a.getEmail());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            Long id = a.getListeNoireAccueillant().getId();
            a.setListeNoireAccueillant(null);
            accueillantService.update(a);
            listeNoireAccueillantService.delete(id);
        }
    }

    private void checkBannissementLocationDates() {
        List<AnnonceLocation> locations = getAll();
        List<AnnonceLocation> aDebannir = new ArrayList<>();
        for (AnnonceLocation a : locations) {
            if (a.getListeNoireLocation() != null) {
                ListeNoireLocation listeNoireLocation = a.getListeNoireLocation();
                Long finBan = listeNoireLocation.getFin().getTime();
                Long rightNow = new Date().getTime();
                if (rightNow > finBan) {
                    aDebannir.add(a);
                }
            }
        }

        for (AnnonceLocation a : aDebannir) {
            Accueillant accueillant = accueillantService.getById(a.getAccueillantId());
            try {
                MailSender.sendMessage("Débanissement",
                    "Votre offre de location située à " + a.getVille().getNom()
                            + " a été débannie",
                    accueillant.getEmail());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            Long id = a.getListeNoireLocation().getId();
            a.setListeNoireLocation(null);
            update(a);
            listeNoireLocationService.delete(id);
        }
    }

    public List<AnnonceLocation> getAllLocationDisponibles() {
        checkBannissementAccueillantDates();
        checkBannissementLocationDates();
        List<AnnonceLocation> annonceLocations =  getSession().createQuery(
                "select al from AnnonceLocation as al, Accueillant as ac " +
                        "left join al.plannings as p " +
                        "where al.accueillantId = ac.id " +
                        "and al.etat.name like 'Valider' " +
                        "and ac.listeNoireAccueillant is null " +
                        "and al.listeNoireLocation is null")
//                        "and (p is null or (p is not null and current_date not between p.start and p.end))")
                .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list();

        return lazyRendering(annonceLocations);
    }

    public List<AnnonceLocation> filteredGetAll(AnnonceLocationFilter filter) {
        //Récupération des informations du filtre à propos des annonces voulues
        String codePostal = (filter.getCodePostal() != null
                && !filter.getCodePostal().equals("")) ? filter.getCodePostal() : "%";
        String ville = (filter.getVille() != null
                && !filter.getVille().equals("")) ? filter.getVille() : "%";

        String type = (filter.getType() != null) ? filter.getType().getName() : "%";
        String nbPlaces = (filter.getNbPlaces() >= 0) ? " and loc.places >= " + filter.getNbPlaces() : "";
        String prixMin = (filter.getPrixMin() != null) ? " and loc.prix >= " + filter.getPrixMin().toString() : "";
        String prixMax = (filter.getPrixMax() != null) ? " and loc.prix <= " + filter.getPrixMax().toString() : "";

        //On crée directement la requête pour les collections
        StringBuilder services = new StringBuilder();
        if (filter.getServices() != null && !filter.getServices().isEmpty()) {
            for (Service service : filter.getServices()) {
                services.append(" and '" + service.getId() + "' in elements(al.services)");
            }
        } else {
            services.append("");
        }


        List<AnnonceLocation> annonceLocations =  getSession().createQuery(
                "select al from AnnonceLocation as al, Accueillant as ac"
                        + " left join al.locations as loc"
                        + " where al.codePostal like '" + codePostal + "'"
                         + " and lower(al.ville.nom) like lower('" + ville + "')"
                         + " and loc.type.name like '" + type + "'"
                         + nbPlaces
                         + prixMin
                         + prixMax
                         + services.toString()

                        //fin requête
                         + " and al.etat.name like 'Valider' "
                         + " and ac.listeNoireAccueillant is null"
                )
                .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list();

        //Appel à la BDD via la requête contenant le filtre
        return lazyRendering(annonceLocations);
    }

    /*
     * Permet des temps de chargement côté client plus rapide (les photos sont longues à charger).
     */
    private List<AnnonceLocation> lazyRendering(List<AnnonceLocation> annonceLocations) {
        List<AnnonceLocation> result = new LinkedList<>();
        List<Photo> photos;

        for (AnnonceLocation a : annonceLocations) {
            photos = (List<Photo>) a.getPhotos();
            result.add(new AnnonceLocation(a.getId(), a.getDescription(), a.getAdresse(),
                    a.getComplementAdresse(), a.getCodePostal(), a.getVille(),
                    a.getEtat(), a.getAccueillantId(), a.getLocations(),
                    Arrays.asList(photos.get(0)), a.getServices(), a.getActivites(),
                    a.getPlannings(), a.getAvis()));
        }

        return result;
    }
}
