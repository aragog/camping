package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.ListeNoireLocation;

@Repository
public class ListeNoireLocationDAO extends SimpleEntityManager<ListeNoireLocation, Long>{

    public ListeNoireLocationDAO() {
        this(ListeNoireLocation.class);
    }

    public ListeNoireLocationDAO(Class<ListeNoireLocation> entityClass) {
        super(entityClass);
    }

}
