package univ.rouen.camping.DAO;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Etat;

@Repository
public class EtatDAO extends SimpleEntityManager<Etat, Long>{

    public EtatDAO() {
        this(Etat.class);
    }

    public EtatDAO(Class<Etat> entityClass) {
        super(entityClass);
    }

    public Etat getByName(String name) {
        return (Etat) getSession().createCriteria(Etat.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
    }

}
