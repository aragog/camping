package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.ListeNoireAccueillant;

@Repository
public class ListeNoireAccueillantDAO extends SimpleEntityManager<ListeNoireAccueillant, Long>{

    public ListeNoireAccueillantDAO() {
        this(ListeNoireAccueillant.class);
    }

    public ListeNoireAccueillantDAO(Class<ListeNoireAccueillant> entityClass) {
        super(entityClass);
    }

}
