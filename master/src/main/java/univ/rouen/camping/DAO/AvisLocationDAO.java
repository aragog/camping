package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.AvisLocation;

@Repository
public class AvisLocationDAO extends SimpleEntityManager<AvisLocation, Long>{

    public AvisLocationDAO() {
        this(AvisLocation.class);
    }

    public AvisLocationDAO(Class<AvisLocation> entityClass) {
        super(entityClass);
    }
}
