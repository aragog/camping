package univ.rouen.camping.DAO;

import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Location;

@Repository
public class LocationDAO extends SimpleEntityManager<Location, Long>{

    public LocationDAO() {
        this(Location.class);
    }

    public LocationDAO(Class<Location> entityClass) {
        super(entityClass);
    }

}
