package univ.rouen.camping.DAO;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.AnnoncePublicitaire;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Repository
public class AnnoncePublicitaireDAO extends SimpleEntityManager<AnnoncePublicitaire, Long>{

    public AnnoncePublicitaireDAO() {
        this(AnnoncePublicitaire.class);
    }

    public AnnoncePublicitaireDAO(Class<AnnoncePublicitaire> entityClass) {
        super(entityClass);
    }

    /**
     * Retourne l'ensemble des annonces publicitaires d'état <code>etat</code>.
     * @param etat Etat de l'annonce
     * @return Les annonces correspondant à l'état
     */
    public List<AnnoncePublicitaire> getAll(String etat) {
        return getSession().createCriteria(AnnoncePublicitaire.class)
                .createCriteria("etat", "e")
                .add(Restrictions.eq("e.name", etat))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    /**
     * Retourne une publicité aléatoire affichable, null sinon.
     * @return Publicité affichable
     */
    public AnnoncePublicitaire adToDisplay() {
        List<AnnoncePublicitaire> pubs = adsToDisplay();

        return pubs.isEmpty() ? null : pubs.get(getRandomNumber(0, pubs.size() - 1));
    }

    /**
     * Retourne la liste des publicités actuellement affichables.
     * @return Liste des publicités affichables
     */
    private List<AnnoncePublicitaire> adsToDisplay() {
        Date rightNow = Calendar.getInstance().getTime();
        return getSession().createCriteria(AnnoncePublicitaire.class)
                .createAlias("etat", "e")
                .add(Restrictions.and(
                        Restrictions.eq("e.name", "Valider"),
                        Restrictions.le("start", rightNow),
                        Restrictions.ge("end", rightNow)
                )).list();
    }

    private int getRandomNumber(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }
}
