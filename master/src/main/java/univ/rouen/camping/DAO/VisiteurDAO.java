package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Visiteur;

@Repository
public class VisiteurDAO extends SimpleEntityManager<Visiteur, Long>{

    public VisiteurDAO() {
        this(Visiteur.class);
    }

    public VisiteurDAO(Class<Visiteur> entityClass) {
        super(entityClass);
    }

}
