package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Activite;

@Repository
public class ActiviteDAO extends SimpleEntityManager<Activite, Long>{

    public ActiviteDAO() {
        this(Activite.class);
    }

    public ActiviteDAO(Class<Activite> entityClass) {
        super(entityClass);
    }

}
