package univ.rouen.camping.DAO;


import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Service;

@Repository
public class ServiceDAO extends SimpleEntityManager<Service, Long>{

    public ServiceDAO() {
        this(Service.class);
    }

    public ServiceDAO(Class<Service> entityClass) {
        super(entityClass);
    }

}
