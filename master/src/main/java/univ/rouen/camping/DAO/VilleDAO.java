package univ.rouen.camping.DAO;


import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Ville;

import java.util.List;

@Repository
public class VilleDAO extends SimpleEntityManager<Ville, Long>{

    public VilleDAO() {
        this(Ville.class);
    }

    public VilleDAO(Class<Ville> entityClass) {
        super(entityClass);
    }

    public List<Ville> searchByCP(String cp) {
        return getSession().createCriteria(Ville.class)
                .add(Restrictions.like("codePostal", cp, MatchMode.ANYWHERE))
                .list();
    }

    public List<Ville> searchByName(String nom) {
        return getSession().createCriteria(Ville.class)
                .add(Restrictions.ilike("nom", nom, MatchMode.EXACT))
                .list();
    }
}
