package univ.rouen.camping.DAO;

import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.AvisQuestionnaire;

@Repository
public class AvisQuestionnaireDAO extends SimpleEntityManager<AvisQuestionnaire, Long>{

    public AvisQuestionnaireDAO() {
        this(AvisQuestionnaire.class);
    }

    public AvisQuestionnaireDAO(Class<AvisQuestionnaire> entityClass) {
        super(entityClass);
    }
}
