package univ.rouen.camping.DAO;

import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.TypeLocation;

@Repository
public class TypeLocationDAO extends SimpleEntityManager<TypeLocation, Long>{

    public TypeLocationDAO() {
        this(TypeLocation.class);
    }

    public TypeLocationDAO(Class<TypeLocation> entityClass) {
        super(entityClass);
    }

}
