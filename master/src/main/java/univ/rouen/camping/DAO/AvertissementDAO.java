package univ.rouen.camping.DAO;

import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.Avertissement;

@Repository
public class AvertissementDAO extends SimpleEntityManager<Avertissement, Long>{

    public AvertissementDAO() {
        this(Avertissement.class);
    }

    public AvertissementDAO(Class<Avertissement> entityClass) {
        super(entityClass);
    }

}
