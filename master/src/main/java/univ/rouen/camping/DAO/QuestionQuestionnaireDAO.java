package univ.rouen.camping.DAO;

import org.springframework.stereotype.Repository;
import univ.rouen.camping.model.QuestionQuestionnaire;

@Repository
public class QuestionQuestionnaireDAO extends SimpleEntityManager<QuestionQuestionnaire, Long>{

    public QuestionQuestionnaireDAO() {
        this(QuestionQuestionnaire.class);
    }

    public QuestionQuestionnaireDAO(Class<QuestionQuestionnaire> entityClass) {
        super(entityClass);
    }

}
