'use strict';

var myModule = angular.module('myServices', ['ngResource']);

/** SERVICES CRUD **/

myModule.factory('Accueillant', function($resource) {
	return $resource('rest/accueillant/:id', {}, {
		'update' : { method:'PUT'}
	});
});

myModule.factory('Publicitaire', function($resource) {
	return $resource('rest/publicitaire/:id', {}, {
		'update' : { method:'PUT'}
	});
});

myModule.factory('Location', function($resource) {
    return $resource('rest/location/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('TypeLocation', function($resource) {
    return $resource('rest/typeLocation/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('AnnonceLocation', function($resource) {
    return $resource('rest/annonceLocation/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Etat', function($resource) {
    return $resource('rest/etat/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Photo', function($resource) {
    return $resource('rest/photo/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Service', function($resource) {
    return $resource('rest/service/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Activite', function($resource) {
    return $resource('rest/activite/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('ListeNoireAccueillant', function($resource) {
    return $resource('rest/listeNoireAccueillant/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('ListeNoireLocation', function($resource) {
    return $resource('rest/listeNoireLocation/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Mail', function($resource) {
    return $resource('rest/mail/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Planning', function($resource) {
    return $resource('rest/planning/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('Ville', function($resource) {
    return $resource('rest/ville/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('AvisLocation', function($resource) {
    return $resource('rest/avislocation/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('QuestionQuestionnaire', function($resource) {
    return $resource('rest/questionQuestionnaire/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('AvisQuestionnaire', function($resource) {
    return $resource('rest/avisQuestionnaire/:id', {}, {
        'update' : { method:'PUT'}
    });
});

myModule.factory('AnnoncePublicitaire', function($resource) {
    return $resource('rest/annoncePublicitaire/:id', {}, {
        'update' : { method:'PUT'}
    });
});


/** SERVICES PERSONNELS **/

/**
 * Service de messages.
 */
myModule.factory('MessageService', function() {
    function MessageService() {
        var messages = [];

        this.addMessage = function(message) {
            messages.push(message);
        };

        this.hasMessages = function() {
            return (messages.length > 0);
        };

        this.consumMessages = function() {
            var messagesToReturn = [];
            while (messages.length > 0) {
                messagesToReturn.push(messages.shift());
            }
            return messagesToReturn;
        };
    }

    return new MessageService();
});

/**
 * Permet de mettre en place un mécanisme de session.
 */
myModule.factory('UserService', [function() {
    // PUBLIC
    var sdo = {
        isLogged: false,
        id: null,
        role: 'VISITEUR' // rôle par défaut
    };

    return sdo;
}]);

/**
 * Persistance des coordonnées géographiques d'un utilisateur.
 */
myModule.factory('GeoService', [function() {
    var latitude = null;
    var longitude = null;

    var geo = {
        setLatitude: function(lat) {
            latitude = lat;
        },
        setLongitude: function(lon) {
            longitude = lon;
        },
        getLatitude: function() {
            return latitude;
        },
        getLongitude: function() {
            return longitude;
        }
    };

    return geo;
}]);

/**
 *  Hôtes utilisés.
 */
myModule.factory('HostService', [function() {
    var prefix = {
        localhost: 'http://localhost:8080/',
        cloudbees: 'http://camping.glaitozen.eu.cloudbees.net/'
    };

    return prefix;
}]);
