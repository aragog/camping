'use strict';

angular.module('camping', ['ngRoute', 'myServices', 'myDirectives', 'ui.bootstrap']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/home.html',
                controller: MenuCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/connexion', {
                templateUrl: 'partials/connexion.html',
                controller: ConnexionCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/accueillant/create', {
                templateUrl: 'partials/accueillant-new.html',
                controller: AccueillantCreateController,
                access: {
                    isFree: true
                }
            }).
            when('/publicitaire/create', {
                templateUrl: 'partials/publicitaire-new.html',
                controller: PublicitaireCreateCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/accueil', {
                templateUrl: 'partials/accAdm.html',
                controller: MenuCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/planning/:id', {
                templateUrl: 'partials/planning.html',
                controller: PlanningCtrl,
                access: {
                    role: 'ACCUEILLANT'
                }
            }).
            when('/location/create', {
                templateUrl: 'partials/ajouter-offre.html',
                controller: LocationCreateCtrl,
                access: {
                    role: 'ACCUEILLANT'
                }
            }).
            when('/accueillant/edit/:id', {
                templateUrl: 'partials/accueillant-edit.html',
                controller: AccueillantEditController,
                access: {
                    role: 'ACCUEILLANT'
                }
            }).
            when('/publicitaire/edit/:id', {
                templateUrl: 'partials/publicitaire-edit.html',
                controller: PublicitaireEditController,
                access: {
                    role: 'PUBLICITAIRE'
                }
            }).
            when('/location/edit/:id', {
                templateUrl: 'partials/editer-offre.html',
                controller: EditerOffreCtrl,
                access: {
                    role: 'ACCUEILLANT'
                }
            }).
            when('/location/ajoutquestionnaire/:id', {
                templateUrl: 'partials/ajout-questionnaire.html',
                controller: AjoutQuestionnaireCtrl,
                access: {
                    role: 'ACCUEILLANT'
                }
            }).
            when('/accueillant/list', {
                templateUrl: 'partials/accueillant-list.html',
                controller: ListeAccueillantCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/annoncePublicitaire/list/attente', {
                templateUrl: 'partials/pub-list-attente.html',
                controller: PubListAttenteCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/location/list/all', {
                templateUrl: 'partials/location-list.html',
                controller: ListeLocationCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/location/list/attente', {
                templateUrl: 'partials/offre-list-attente.html',
                controller: LocationListAttenteController,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/location/list/:id', {
                templateUrl: 'partials/offre-list-accueillant.html',
                controller: LocationListAccueillantController,
                access: {
                    role: 'ACCUEILLANT'
                }
            }).
            when('/location/questionnaire/list/:id', {
                templateUrl: 'partials/questionnaire-list.html',
                controller: QuestionnaireListCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/questionnaire/details/:id', {
                templateUrl: 'partials/questionnaire-details.html',
                controller: QuestionnaireDetailsCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/location/edit/admin/:id', {
                templateUrl:'partials/editer-offre-admin.html',
                controller:EditerOffreAdminCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/annoncePublicitaire/edit/:id', {
                templateUrl:'partials/editer-pub.html',
                controller:EditerPubCtrl,
                access: {
                    role: 'PUBLICITAIRE'
                }
            }).
            when('/annoncePublicitaire/edit/admin/:id', {
                templateUrl:'partials/editer-pub-admin.html',
                controller:EditerPubAdminCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/publicite/create', {
                templateUrl: 'partials/ajouter-pub.html',
                controller: PubCreateCtrl,
                access: {
                    role: 'PUBLICITAIRE'
                }
            }).
            when('/contacterAccueillant/:id', {
                templateUrl: 'partials/contacter-accueillant.html',
                controller: ContacterAccueillantCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/contacterAccueillantVisiteur/:id', {
                templateUrl: 'partials/contacter-accueillant-visiteur.html',
                controller: ContacterAccueillantCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/bannir/:id', {
                templateUrl: 'partials/bannir-accueillant.html',
                controller: BannirAccueillantController,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/bannir/location/:id', {
                templateUrl: 'partials/bannir-location.html',
                controller: BannirLocationController,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/avertir/:id', {
                templateUrl: 'partials/avertir-accueillant.html',
                controller: AvertirAccueillantController,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/menu', {
                templateUrl: 'partials/menu.html',
                controller: MenuCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/details-offre/:id', {
                templateUrl: 'partials/details-offre.html',
                controller: DetailsOffreCtrl,
                access: {
                    isFree : true
                }
            }).
            when('/details-pub/:id', {
                templateUrl: 'partials/details-pub.html',
                controller: DetailsPubCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/details-offre-admin/:id', {
                templateUrl: 'partials/details-offre-admin.html',
                controller: DetailsOffreCtrl,
                access: {
                    role: 'ADMINISTRATEUR'
                }
            }).
            when('/location/questionnaire/:id', {
                templateUrl: 'partials/questionnaire.html',
                controller: QuestionnaireCtrl,
                access: {
                    isFree: true
                }
            }).
            when('/accueillant/supprimer/:id', {
                templateUrl: 'partials/confirm-suppression.html',
                controller: ConfirmSuppressionCtrl,
                access: {
                    isFree: true
                }
            }).
            otherwise({redirectTo:'/'});
    }]).run(['$rootScope', 'UserService', '$location', function(root, UserService, $location) {
        root.$on('$routeChangeStart', function(scope, currRoute, prevRoute) {
            // Route libre
            if (!currRoute.access || currRoute.access.isFree) {
                return;
            }

            // Route à accès restreint
            if (UserService.isLogged && (currRoute.access.role == UserService.role)) {
                return;
            }

            // Redirection par défaut
            $location.path('/');
        });
    }]);