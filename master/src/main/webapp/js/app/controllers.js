'use strict';

/**
 * Contrôleur dédié à l'inscription d'un nouvel utilisateur en tant qu'accueillant.
 * @param $scope
 * @param $location
 * @param Accueillant
 * @param MessageService
 */
function AccueillantCreateController($scope, $location, Accueillant, MessageService) {
	$scope.password = {display:'password'};
	$scope.master = {};

	/**
	 * Enregistre 'user' en BDD.
	 * En cas de succès, redirection.
	 * Sinon, traitement de l'erreur.
	 */
	$scope.create = function(user) {
        $scope.clicking = true;
		Accueillant.save(user, function(data) {
			MessageService.addMessage('Inscription réussie, vous pouvez vous connecter maintenant !');
            $scope.clicking = false;
			$location.path('/');
		}, function(err) {
            if (err.status == 401) {
                $scope.errorMessage = 'Ce pseudonyme existe déjà'
            } else {
                $scope.errorMessage = err.data;
            }
            $scope.error = true;
            $scope.clicking = false;
		});
	};

	/**
	 * Réinitialisation du procédé d'inscription.
	 */
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);
	};
	
	/**
	 * Cache/affiche les mots de passe.
	 */
	$scope.switchPasswordDisplaying = function(display) {
		$scope.password.display = (display) ? 'password' : 'text';
	};

	$scope.reset();
}

/**
 * Contrôleur de la creation d'une offre de publicite.
 * @param $scope
 * @param $location
 * @param Publicitaire
 * @param UserService
 * @param MessageService
 */
function PubCreateCtrl($scope, $location, Publicitaire, UserService, MessageService) {
    $scope.master = {};

    $('#photo').change(function(e) {
        var file = e.target.files[0];
        if (file.size > 1000000) {
            $scope.errorPhotoMessage = 'Taille limitée à 1Mo';
            $scope.errorPhoto = true;
            return;
        }
        if (!file.type.match(/image\/jpeg|image\/png/)) {
            $scope.errorPhotoMessage = 'Formats supportés : JPEG, PNG';
            $scope.errorPhoto = true;
            return;
        };

        addAsDataURL(file);
        $scope.$apply();
    });

    function addAsDataURL(file) {
        var reader = new FileReader();

        // affichage d'une miniature retaillée
        reader.onload = function(e) {
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: '/rest/photo/resize',
                data: JSON.stringify(e.target.result),
                dataType: "json",
                success: function(result) {
                    $scope.photoURL = result;
                    $scope.$apply();
                },
                error: function(err){
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.$apply();
                }
            });
        }

        reader.readAsDataURL(file);
    }

    function getEtatAttente() {
        return $.getJSON('/rest/etat/nom/Attente');
    }

    $scope.create = function(pub) {
        $scope.clicking = true;
        $.when(getEtatAttente()).done(
            function(etatAttente) {
                pub.etat = etatAttente;
                pub.publicitaireId = UserService.id;
                Publicitaire.get({id: pub.publicitaireId}, function(publicitaire) {
                    pub.photo = $scope.photoURL;
                    if (publicitaire.publicites) {
                        publicitaire.publicites.push(pub);
                    } else {
                        publicitaire.publicites = [ pub ];
                    }

                    Publicitaire.update(publicitaire, function(data) {
                        MessageService.addMessage('Offre publicitaire créée');
                        $location.path('/');
                    }, function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                        $scope.clicking = false;
                    });
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };

    $scope.deletePhoto = function() {
       $scope.photoURL = null;
    };

    $scope.reset = function() {
        $scope.pub = angular.copy($scope.master);
        $scope.photoURL = null;
    };

    $scope.reset();
}

/**
 * Contrôleur du listing des questionnaires d'un accueillant.
 * @param $scope
 * @param $routeParams
 * @param $filter
 * @param $location
 * @param MessageService
 * @param Accueillant
 */
function QuestionnaireListCtrl($scope, $routeParams, $filter, $location, MessageService, Accueillant) {
    $scope.accueillant = Accueillant.get({id: $routeParams.id}, function() {
        $scope.questionnaires = [];
        for (var k = 0, a; a = $scope.accueillant.annonceLocations[k]; ++k) {
            for (var m = 0, q; q = a.questionnaires[m]; ++m) {
                $scope.questionnaires.push(q);
            }
        }
        $scope.search();
        $scope.loaded = true;
    });

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    $scope.questionnairesByPage = 10;

    $scope.sort = {
        column: 'id',
        descending: false
    };

    $scope.selectedCls = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    }

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column === column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        $scope.search();
    }

    var searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // calculate page in place
    $scope.groupToPages = function() {
        $scope.pagedQuestionnaires = [];

        for (var i = 0; i < $scope.filteredQuestionnaires.length; i++) {
            if (i % $scope.questionnairesByPage === 0) {
                $scope.pagedQuestionnaires[Math.floor(i / $scope.questionnairesByPage)] = [ $scope.filteredQuestionnaires[i] ];
            } else {
                $scope.pagedQuestionnaires[Math.floor(i / $scope.questionnairesByPage)].push($scope.filteredQuestionnaires[i]);
            }
        }
    };

    // init the filtered accueillants
    $scope.search = function() {
        $scope.filteredQuestionnaires = $filter('filter')($scope.questionnaires, function(questionnaire) {
            return searchMatch(questionnaire.id, $scope.query);
        });
        // take care of the sorting order
        $scope.filteredQuestionnaires = $filter('orderBy')($scope.filteredQuestionnaires, $scope.sort.column, $scope.sort.descending);
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    // Previous page
    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    // Next page
    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedQuestionnaires.length - 1) {
            $scope.currentPage++;
        }
    };

    // First page
    $scope.firstPage = function() {
        $scope.currentPage = 0;
    };

    // Last page
    $scope.lastPage = function() {
        $scope.currentPage = $scope.pagedQuestionnaires.length - 1;
    };

    $scope.remove = function(accueillant, questionnaire) {

        function removeQuestionnaires(questionnaire, annonceLocations) {
            var index = -1;
            var a;
            for (var k = 0; (index === -1) && (a = annonceLocations[k]); ++k) {
                index = a.questionnaires.indexOf(questionnaire);
            }

            if (index !== -1) {
                a.questionnaires.splice(index, 1);
            }
        }

        if (!window.confirm('Etes-vous certain de vouloir supprimer ce questionnaire ?')) {
            return;
        }

        removeQuestionnaires(questionnaire, accueillant.annonceLocations);

        $scope.clicking = true;
        Accueillant.update(accueillant, function(data) {
            MessageService.addMessage('Questionnaire supprimé avec succès');
            $location.path('/accueillant/list');
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}

/**
 * Contrôleur pour l'affichage des résultats d'un questionnaire.
 * @param $scope
 * @param $routeParams
 * @param AvisQuestionnaire
 */
function QuestionnaireDetailsCtrl($scope, $routeParams, AvisQuestionnaire) {
    $scope.questionnaire = AvisQuestionnaire.get({id: $routeParams.id});
}

/**
 * Contrôleur du listing des accueillants.
 * @param $scope
 * @param $filter
 * @param Accueillant
 * @param ListeNoireAccueillant
 * @param MessageService
 * @param Mail
 */
function ListeAccueillantCtrl($scope, $filter, Accueillant, ListeNoireAccueillant, MessageService, Mail) {
    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    $scope.accueillantsByPage = 10;
    $scope.accueillants = Accueillant.query(function() {
        $scope.search();
        $scope.loaded = true;
    });

    $scope.sort = {
        column: 'pseudo',
        descending: false
    };

    $scope.selectedCls = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    }

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column === column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        $scope.search();
    }

    var searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // calculate page in place
    $scope.groupToPages = function() {
        $scope.pagedAccueillants = [];

        for (var i = 0; i < $scope.filteredAccueillants.length; i++) {
            if (i % $scope.accueillantsByPage === 0) {
                $scope.pagedAccueillants[Math.floor(i / $scope.accueillantsByPage)] = [ $scope.filteredAccueillants[i] ];
            } else {
                $scope.pagedAccueillants[Math.floor(i / $scope.accueillantsByPage)].push($scope.filteredAccueillants[i]);
            }
        }
    };

    // init the filtered accueillants
    $scope.search = function() {
        $scope.filteredAccueillants = $filter('filter')($scope.accueillants, function(accueillant) {
            return searchMatch(accueillant.pseudo, $scope.query);
        });
        // take care of the sorting order
        $scope.filteredAccueillants = $filter('orderBy')($scope.filteredAccueillants, $scope.sort.column, $scope.sort.descending);
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    // Previous page
    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    // Next page
    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedAccueillants.length - 1) {
            $scope.currentPage++;
        }
    };

    // First page
    $scope.firstPage = function() {
        $scope.currentPage = 0;
    };

    // Last page
    $scope.lastPage = function() {
        $scope.currentPage = $scope.pagedAccueillants.length - 1;
    };

    $scope.debannir = function(accueillant) {
        var idL = accueillant.listeNoireAccueillant.id;
        accueillant.listeNoireAccueillant = null;

        $scope.clicking = true;
        Accueillant.update(accueillant, function(data) {
            // Cascade (voir ListeNoireAccueillantTest)
            ListeNoireAccueillant.delete({id: idL}, function(data) {
                $scope.info = true;
                $scope.infoMessage = 'Utilisateur retiré de la liste des utilisateurs bannis';
                var mail = {
                    subject: 'Débannissement',
                    text: 'Votre compte a été débanni',
                    to: accueillant.email
                };

                Mail.save(mail, function(data) {
                    $scope.clicking = false;
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }, function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };

    $scope.checkMarks = function(accueillant) {
        var limit = 2;
        var errorColor = 'red';
        var infMark = averageInformal(accueillant);
        var forMark = averageFormal(accueillant);

        return ((infMark && (infMark < limit))
            || (forMark && (forMark < limit))) ? errorColor : 'default';
    };

    function averageInformal(accueillant) {
        var sum = 0;
        var nbComments = 0;

        for (var k = 0, a; a = accueillant.annonceLocations[k]; ++k) {
            for (var m = 0, c; c = a.avis[m]; ++m) {
                sum += c.note;
                ++nbComments;
            }
        }

        return (nbComments) ? (sum / nbComments) : 0;
    }

    function averageFormal(accueillant) {
        var sum = 0;
        var nbAnswers = 0;

        for (var p = 0, a; a = accueillant.annonceLocations[p]; ++p) {
            for (var k = 0, q; q = a.questionnaires[k]; ++k) {
                for (var m = 0, r; r = q.reponseQuestionnaire[m]; ++m) {
                    sum += r.reponse;
                    ++nbAnswers;
                }
            }
        }

        return (nbAnswers) ? (sum / nbAnswers) : 0;
    }
}

/**
 * Contrôleur du bannissement d'une offre de location.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param MessageService
 * @param Mail
 * @param AnnonceLocation
 * @param Accueillant
 */
function BannirLocationController($scope, $routeParams, $location, MessageService, Mail, AnnonceLocation, Accueillant) {
    $scope.annonceLocation = AnnonceLocation.get({id:$routeParams.id}, function() {
        $scope.accueillant = Accueillant.get({id:$scope.annonceLocation.accueillantId});
    });

    function createMail(annonceLocation) {
        var mail = {};
        mail.subject = 'Bannissement de votre offre de location';
        mail.to = $scope.accueillant.email;
        mail.text = 'Bonjour, votre offre de location située à ' + annonceLocation.ville.nom
            + ' a été suspendue pour le motif suivant :\n\n'
            + annonceLocation.listeNoireLocation.motif;

        if (annonceLocation.listeNoireLocation.fin) {
            mail.text += '\n\nLe bannissement sera automatiquement levé le '
                + annonceLocation.listeNoireLocation.fin.toLocaleString()
                + '.';
        }
        return mail;
    }

    $scope.bannir = function(annonceLocation) {
        if (!window.confirm('Bannir la location ?')) {
            return;
        }

        AnnonceLocation.update(annonceLocation, function(data) {
            $scope.clicking = true;
            Mail.save(createMail(annonceLocation), function(data) {
                MessageService.addMessage('Offre de location ajoutée à la liste noire');
                $location.path('/location/list/all');
            }, function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}

/**
 * Contrôleur du listing de toutes les annonces de location.
 * @param $scope
 * @param $filter
 * @param MessageService
 * @param AnnonceLocation
 * @param ListeNoireLocation
 * @param Mail
 * @param Accueillant
 */
function ListeLocationCtrl($scope, $filter, MessageService, AnnonceLocation, ListeNoireLocation, Mail, Accueillant) {
    $scope.annonceLocations = AnnonceLocation.query(function() {
        $scope.loaded = true;
        $scope.search();
    });

    $scope.annonceLocationsByPage = 10;
    $scope.currentPage = 0;

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    $scope.sort = {
        column: 'ville.nom',
        descending: false
    };

    $scope.selectedCls = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    }

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column === column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        $scope.search();
    }

    var searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // calculate page in place
    $scope.groupToPages = function() {
        $scope.pagedAnnonceLocations = [];

        for (var i = 0; i < $scope.filteredAnnonceLocations.length; i++) {
            if (i % $scope.annonceLocationsByPage === 0) {
                $scope.pagedAnnonceLocations[Math.floor(i / $scope.annonceLocationsByPage)] = [ $scope.filteredAnnonceLocations[i] ];
            } else {
                $scope.pagedAnnonceLocations[Math.floor(i / $scope.annonceLocationsByPage)].push($scope.filteredAnnonceLocations[i]);
            }
        }
    };

    // init the filtered locations
    $scope.search = function() {
        $scope.filteredAnnonceLocations = $filter('filter')($scope.annonceLocations, function(annonceLocation) {
            return searchMatch(annonceLocation.ville.nom, $scope.query);
        });
        // take care of the sorting order
        $scope.filteredAnnonceLocations = $filter('orderBy')($scope.filteredAnnonceLocations, $scope.sort.column, $scope.sort.descending);
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    // Previous page
    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    // Next page
    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedAnnonceLocations.length - 1) {
            $scope.currentPage++;
        }
    };

    // First page
    $scope.firstPage = function() {
        $scope.currentPage = 0;
    };

    // Last page
    $scope.lastPage = function() {
        $scope.currentPage = $scope.pagedAnnonceLocations.length - 1;
    };

    $scope.debannir = function(annonceLocation) {
        var accueillant = Accueillant.get({id:annonceLocation.accueillantId});
        var idL = annonceLocation.listeNoireLocation.id;
        annonceLocation.listeNoireLocation = null;

        $scope.clicking = true;
        AnnonceLocation.update(annonceLocation, function(data) {
            // Cascade
            ListeNoireLocation.delete({id: idL}, function(data) {
                $scope.info = true;
                $scope.infoMessage = 'Offre de location retirée de la liste des locations bannies';
                var mail = {
                    subject: 'Débannissement de votre offre de location',
                    text: 'Bonjour, votre offre de location située à ' + annonceLocation.ville.nom
                        + ' a été débannie',
                    to: accueillant.email
                };

                Mail.save(mail, function(data) {
                    $scope.clicking = false;
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }, function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}

/**
 * Contrôleur de bannissement d'un accueillant.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param Accueillant
 * @param MessageService
 * @param Mail
 */
function BannirAccueillantController($scope, $routeParams, $location, Accueillant, MessageService, Mail) {
    $scope.accueillant = Accueillant.get({id: $routeParams.id});

    function createMail(accueillant) {
        var mail = {};
        mail.to = accueillant.email;
        mail.subject = 'Bannissement';
        mail.text = 'Bonjour, votre compte a été suspendu du site de camping de particuliers à particuliers.\n';
        if (accueillant.listeNoireAccueillant.fin) {
            mail.text += 'Le bannissement sera automatiquement levé le '
                + accueillant.listeNoireAccueillant.fin.toLocaleString()
                + '.';
        }
        mail.text += ('\nMotif : ' + accueillant.listeNoireAccueillant.motif);
        return mail;
    }

    $scope.bannir = function(accueillant) {
        if (!window.confirm('Bannir l\'accueillant ?')) {
            return;
        }

        Accueillant.update(accueillant, function(data) {
            $scope.clicking = true;
            Mail.save(createMail(accueillant), function(data) {
                MessageService.addMessage('Utilisateur ajouté à la liste noire');
                $location.path('/accueillant/list');
            }, function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}

/**
 * Contrôleur de dépôt d'un avertissement.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param Accueillant
 * @param MessageService
 * @param Mail
 */
function AvertirAccueillantController($scope, $routeParams, $location, Accueillant, MessageService, Mail) {
    $scope.accueillant = Accueillant.get({id: $routeParams.id});

    function createMail(accueillant, avertissement) {
        var mail = { subject: 'Avertissement', to: accueillant.email };
        mail.text = 'Bonjour, votre compte a reçu un avertissement sur le site de camping de particuliers à particuliers.\n';
        mail.text += ('\nMotif : ' + avertissement.motif);

        return mail;
    }

    $scope.avertir = function(accueillant, avertissement) {
        if (!accueillant.avertissements) {
            accueillant.avertissements = [];
        }
        accueillant.avertissements.push(avertissement);

        Accueillant.update(accueillant, function(data) {
            $scope.clicking = true;
            Mail.save(createMail(accueillant, avertissement), function(data) {
                MessageService.addMessage('Avertissement déposé');
                $location.path('/accueillant/list');
            }, function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}

/**
 * Contrôleur pour la modification du compte d'un publicitaire.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param $http
 * @param Publicitaire
 * @param MessageService
 * @param Mail
 * @param HostService
 */
function PublicitaireEditController($scope, $routeParams, $location, $http, Publicitaire, MessageService, Mail, HostService) {
    $scope.password = {display:'password'};
    $scope.user = Publicitaire.get({id:$routeParams.id}, function() {
        $scope.loaded = true;
        $scope.user.password = null;
    });

    /**
     * MAJ des informations personnelles de l'utilisateur 'user'.
     */
    $scope.update = function(user) {
        $scope.clicking = true;
        $http.put('/rest/publicitaire/profil/', user)
            .success(function(result) {
                $scope.info = true;
                $scope.infoMessage = 'Mise à jour effectuée';
                $scope.clicking = false;
            }).error(function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
    };

    function createMail(user) {
        var mail = {};
        mail.subject = 'Confirmation de la suppression de votre compte';
        mail.to = user.email;
        mail.text = 'Bonjour, nous vous envoyons un email car vous souhaitez supprimer votre compte.\n'
        mail.text += 'Pour confirmer la suppression, suivez le lien ci-dessous puis renseignez vos identifiants :\n\n';
        mail.text += HostService.localhost + '#/publicitaire/supprimer/' + user.id;
        return mail;
    }

    $scope.delete = function(user) {
        if (!window.confirm('Etes-vous certain de vouloir supprimer votre compte ?')) {
            return;
        }
        $scope.loaded = false;
        Mail.save(createMail(user), function(data) {
            MessageService.addMessage('Vous venez de reçevoir un email. Suivez le lien contenu dans ' +
                'le corps du message afin de confirmer la suppression de votre compte.');
            $location.path('/');
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };

    /**
     * Cache/affiche les mots de passe.
     */
    $scope.switchPasswordDisplaying = function(display) {
        $scope.password.display = (display) ? 'password' : 'text';
    };
}

/**
 * Contrôleur pour la modification du compte d'un accueillant.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param $http
 * @param Accueillant
 * @param MessageService
 * @param Mail
 * @param HostService
 */
function AccueillantEditController($scope, $routeParams, $location, $http, Accueillant, MessageService, Mail, HostService) {
	$scope.password = {display:'password'};
    $scope.user = Accueillant.get({id:$routeParams.id}, function() {
        $scope.loaded = true;
        $scope.user.password = null;
    });
    
    /**
     * MAJ des informations personnelles de l'utilisateur 'user'.
     */
	$scope.update = function(user) {
        $scope.clicking = true;
        $http.put('/rest/accueillant/profil/', user)
            .success(function(result) {
                $scope.info = true;
                $scope.infoMessage = 'Mise à jour effectuée';
                $scope.clicking = false;
            }).error(function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
	};

    function createMail(user) {
        var mail = {};
        mail.subject = 'Confirmation de la suppression de votre compte';
        mail.to = user.email;
        mail.text = 'Bonjour, nous vous envoyons un email car vous souhaitez supprimer votre compte.\n'
        mail.text += 'Pour confirmer la suppression, suivez le lien ci-dessous puis renseignez vos identifiants :\n\n';
        mail.text += HostService.localhost + '#/accueillant/supprimer/' + user.id;
        return mail;
    }

    $scope.delete = function(user) {
        if (!window.confirm('Etes-vous certain de vouloir supprimer votre compte ?')) {
            return;
        }
        $scope.loaded = false;
        Mail.save(createMail(user), function(data) {
            MessageService.addMessage('Vous venez de reçevoir un email. Suivez le lien contenu dans ' +
                'le corps du message afin de confirmer la suppression de votre compte.');
            $location.path('/');
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
	};
	
	/**
	 * Cache/affiche les mots de passe.
	 */
	$scope.switchPasswordDisplaying = function(display) {
		$scope.password.display = (display) ? 'password' : 'text';
	};
}

/**
 * Contrôleur du planning d'un accueillant.
 * @param $scope
 * @param $routeParams
 * @param $route
 * @param AnnonceLocation
 * @param Planning
 */
function PlanningCtrl($scope, $routeParams, $route, AnnonceLocation, Planning) {

    function retrieveAllAnnonceLocation() {
        return $.getJSON('/rest/annonceLocation/eager/' + $routeParams.id);
    }

    $.when(retrieveAllAnnonceLocation()).done(
        function(result) {
            $scope.annonceLocation = result;
            if (!$scope.annonceLocation.plannings) {
                $scope.annonceLocation.plannings = [];
            }
            $scope.loaded = true;
            $scope.$apply();
            displayCalendar();
        }
    );

    function displayCalendar() {
        $(document).ready(function() {
            // On désactive les clics droits, par défaut, de la fenêtre
            $('#calendar:not(".fc-event")').on('contextmenu', function(e) {
                e.preventDefault();
            });

            var userEvents = [];
            for (var k = 0, e; e = $scope.annonceLocation.plannings[k]; ++k) {
                userEvents.push({id: e.id, start: e.start, end: e.end});
            }

            var calendar = $('#calendar').fullCalendar({
                // en-têtes
                header: {
                    left: 'prev,prevYear today',
                    center: 'title',
                    right: 'nextYear,next'
                },
                // événements prédéfinis
                events: userEvents,
                // sélections
                selectable: true,
                selectHelper: true,
                // création d'un nouvel événement
                select: function(s, e, allDay) {
                    calendar.fullCalendar('renderEvent',
                        {
                            start: s,
                            end: e,
                            allDay: allDay
                        },
                        true // événement persistant
                    );

                    $scope.annonceLocation.plannings.push({start: s, end: e});

                    AnnonceLocation.update($scope.annonceLocation, function(data) {
                        $route.reload();
                    }, function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                    });
                    calendar.fullCalendar('unselect');
                },
                // déplacement / retaillage
                editable: true,
                // suppression d'un événement
                eventRender: function(event, element, view) {
                    element.bind('mousedown', function(e) {
                        if (e.which == 3) {
                            var retval = confirm('Voulez-vous vraiment supprimer cet événement ?');
                            if (retval == 1) {
                                var p;
                                for (var k = 0; p = $scope.annonceLocation.plannings[k]; ++k) {
                                    if (p.id == event.id) {
                                        $scope.annonceLocation.plannings.splice(k, 1);
                                        break;
                                    }
                                }

                                AnnonceLocation.update($scope.annonceLocation, function(data) {
                                    Planning.delete({id: p.id}, function(data) {
                                        calendar.fullCalendar('removeEvents', event.id);
                                    }, function(err) {
                                        $scope.error = true;
                                        $scope.errorMessage = err.data;
                                    });
                                }, function(err) {
                                    $scope.error = true;
                                    $scope.errorMessage = err.data;
                                });
                            }
                        }
                    });
                },
                // retaillage d'un événement
                eventResize: function(event) {
                    var e = {id: event.id, start: event.start, end: event.end || event.start};
                    Planning.update(e, function(data) {
                    }, function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                    });
                },
                // glisser-déposer d'un événement
                eventDrop: function(event) {
                    var e = {id: event.id, start: event.start, end: event.end || event.start};
                    Planning.update(e, function(data) {
                    }, function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                    });
                },
                // textes francisés
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
                    'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
                dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                buttonText: {
                    today: 'Aujourd\'hui'
                }
            });
        });
    }
}

/**
 * Contrôleur de la création d'un compte pour les publicitaires.
 * @param $scope
 * @param $location
 * @param Publicitaire
 * @param MessageService
 */
function PublicitaireCreateCtrl($scope, $location, Publicitaire, MessageService) {
    $scope.password = {display:'password'};
    $scope.master = {};

    /**
     * Enregistre 'user' en BDD.
     * En cas de succès, redirection.
     * Sinon, traitement de l'erreur.
     */
    $scope.create = function(user) {
        $scope.clicking = true;
        Publicitaire.save(user, function(data) {
            MessageService.addMessage('Inscription réussie, vous pouvez vous connecter maintenant !');
            $scope.clicking = false;
            $location.path('/');
        }, function(err) {
            if (err.status == 401) {
                $scope.errorMessage = 'Ce pseudonyme existe déjà'
            } else {
                $scope.errorMessage = err.data;
            }
            $scope.error = true;
            $scope.clicking = false;
        });
    };

    /**
     * Réinitialisation du procédé d'inscription.
     */
    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };

    /**
     * Cache/affiche les mots de passe.
     */
    $scope.switchPasswordDisplaying = function(display) {
        $scope.password.display = (display) ? 'password' : 'text';
    };

    $scope.reset();
}

/**
 * Contrôleur de création d'une nouvelle offre de location.
 * @param $scope
 * @param $location
 * @param Service
 * @param MessageService
 * @param Activite
 * @param Accueillant
 * @param UserService
 * @param Ville
 */
function LocationCreateCtrl($scope, $location, Service, MessageService, Activite, Accueillant, UserService, Ville) {
    $scope.services = Service.query();
    $scope.activites = Activite.query();
	$scope.master = {};
    $scope.noSelection = [];

    $scope.marker = null;
    $scope.map = null;

    $scope.removeMarker = function() {
        if (!$scope.marker) {
            return;
        }
        $scope.marker.setMap(null);
        $scope.marker = null;
    };

    function initializeGoogleMap() {
        var mapOptions = {
            zoom: 6,
            center: new google.maps.LatLng(48,2),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        $scope.map = new google.maps.Map($('#map-canvas').get(0), mapOptions);

        google.maps.event.addListener($scope.map, 'click', function() {
            placeMarker(event.latLng);
            $scope.$apply();
        });

        function placeMarker(location) {
            if ($scope.marker) {
                return;
            }

            $scope.marker = new google.maps.Marker({
                position: location,
                map: $scope.map,
                draggable: true
            });

            $scope.marker.setMap($scope.map);

            var infowindow = new google.maps.InfoWindow({
                content: 'Latitude: ' + location.lat() +
                    '<br>Longitude: ' + location.lng()
            });

            google.maps.event.addListener($scope.marker, 'dragend', function(){
                var point = $scope.marker.getPosition();
                infowindow.content = 'Latitude: ' + point.lat() +
                        '<br>Longitude: ' + point.lng();
                infowindow.close();
                infowindow.open($scope.map, $scope.marker);
            });

            infowindow.open($scope.map, $scope.marker);
        }
    }

//    initializeGoogleMap();

    $('#photos').change(function(e) {
        if ($scope.photosURL.length == 10) {
            $scope.errorPhotoMessage = 'Limite du nombre de photos atteinte';
            $scope.errorPhoto = true;
            return;
        }

        var files = e.target.files;
        var lastFile = files[files.length - 1];
        if (lastFile.size > 1000000) {
            $scope.errorPhotoMessage = 'Taille limitée à 1Mo';
            $scope.errorPhoto = true;
            return;
        }
        if (!lastFile.type.match(/image\/jpeg|image\/png/)) {
            $scope.errorPhotoMessage = 'Formats supportés : JPEG, PNG';
            $scope.errorPhoto = true;
            return;
        };

        addAsDataURL(lastFile);
        $scope.$apply();
    });

    function addAsDataURL(file) {
        var reader = new FileReader();

        // affichage d'une miniature retaillée
        reader.onload = function(e) {
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: '/rest/photo/resize',
                data: JSON.stringify(e.target.result),
                dataType: "json",
                success: function(result) {
                    $scope.photosURL.push(result);
                    $scope.$apply();
                },
                error: function(err){
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.$apply();
                }
            });
        }

        reader.readAsDataURL(file);
    }

    $scope.deletePhoto = function(index) {
        $scope.photosURL.splice(index, 1);
    };

    $scope.changeService = function(service) {
        var index = $scope.selectedServices.indexOf(service);
        if (index == -1) {
            $scope.selectedServices.push(service);
        } else {
            $scope.selectedServices.splice(index, 1);
        }
    };

    $scope.changeActivite = function(activite) {
        var index = $scope.selectedActivites.indexOf(activite);
        if (index == -1) {
            $scope.selectedActivites.push(activite);
        } else {
            $scope.selectedActivites.splice(index, 1);
        }
    };

    function checkTente() {
        if ($scope.tenteShow) {
            return $.getJSON('/rest/typeLocation/nom/Tente');
        }
    }

    function checkCampingCar() {
        if ($scope.CCShow) {
            return $.getJSON('/rest/typeLocation/nom/Camping-car');
        }
    }

    function checkCaravane() {
        if ($scope.caravaneShow) {
            return $.getJSON('/rest/typeLocation/nom/Caravane');
        }
    }

    function getEtatAttente() {
        return $.getJSON('/rest/etat/nom/Attente');
    }

	$scope.create = function(annonceLocation) {
        $scope.clicking = true;
	    var locations = [];

        $.when(checkTente(), checkCampingCar(), checkCaravane(), getEtatAttente()).done(
            function(tente, campingCar, caravane, etatAttente) {
                if (tente) {
                    locations.push($scope.locationTente);
                    $scope.locationTente.type = tente[0];
                }
                if (campingCar) {
                    locations.push($scope.locationCC);
                    $scope.locationCC.type = campingCar[0];
                }
                if (caravane) {
                    locations.push($scope.locationCaravane);
                    $scope.locationCaravane.type = caravane[0];
                }

                function searchByName(name) {
                    return $.getJSON('/rest/ville/name/' + name);
                }

                function save() {
                    annonceLocation.locations = locations;
                    annonceLocation.photos = $scope.photosURL;
                    annonceLocation.services = $scope.selectedServices;
                    annonceLocation.activites = $scope.selectedActivites;
                    annonceLocation.etat = etatAttente[0];
                    annonceLocation.accueillantId = UserService.id;

                    Accueillant.get({id: UserService.id}, function(accueillant) {
                        if (accueillant.annonceLocations) {
                            accueillant.annonceLocations.push(annonceLocation);
                        } else {
                            accueillant.annonceLocations = [ annonceLocation ];
                        }
                        Accueillant.update(accueillant, function(data) {
                            MessageService.addMessage('Offre de location créée');
                            $location.path('/accAdm');
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    }, function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                        $scope.clicking = false;
                    });
                }

                if ($scope.marker) {
                    $.when(searchByName(annonceLocation.ville.toUpperCase())).done(function(result) {
                        if (result.length > 0) {
                            $scope.errorMap = true;
                            $scope.errorMapMessage = 'Impossible de redéfinir les coordonnées d\'une ville existante';
                            $scope.clicking = false;
                            $scope.$apply();
                        } else {
                            var p = $scope.marker.getPosition();
                            var ville = {nom: annonceLocation.ville, latitude: p.lat(), longitude: p.lng(),
                                codePostal: annonceLocation.codePostal, departement: ''};
                            Ville.save(ville, function(data) {
                                $.when(searchByName(ville.nom)).done(function(result) {
                                    $scope.annonceLocation.ville = result[0];
                                    save();
                                });
                            }, function(err) {
                                $scope.error = true;
                                $scope.errorMessage = err.data;
                            });
                        }
                    });
                } else {
                    $.when(searchByName(annonceLocation.ville)).done(function(result) {
                        if (result.length == 0) {
                            $scope.errorMap = true;
                            $scope.errorMapMessage = 'Ville inexistante ! Choisissez-en une depuis la liste ' +
                                'ou marquez ses coordonnées sur la carte';
                            $scope.clicking = false;
                            $scope.$apply();
                        } else {
                            annonceLocation.ville = $scope.selectedCity;
                            save();
                        }
                    });
                }
            }
        );
	};

	$scope.reset = function() {
        $scope.clicking = true;
		$scope.annonceLocation = angular.copy($scope.master);
        $scope.locationTente = angular.copy($scope.master);
        $scope.locationCC = angular.copy($scope.master);
        $scope.locationCaravane = angular.copy($scope.master);
        $scope.selectedCity = angular.copy($scope.master);
        $scope.photosURL = angular.copy($scope.noSelection);
        $scope.selectedServices = angular.copy($scope.noSelection);
        $scope.selectedActivites = angular.copy($scope.noSelection);
        $scope.villes = angular.copy($scope.noSelection);
        $('input[type=checkbox]').prop('checked', false);
        $scope.clicking = false;
	};

	$scope.reset();

    $scope.$watch('annonceLocation.codePostal', function(cp) {
        if (!cp) {
            $scope.villes = [];
            $scope.cpInvalid = true;
            return;
        }
        $scope.cpInvalid = false;

        function getVillesByCP() {
            return $.getJSON('/rest/ville/CP/' + cp);
        }

        $.when(getVillesByCP()).done(function(villes) {
            if (villes.length == 0) {
                $scope.cpInvalid = true;
            }
            $scope.villes = villes;
            $scope.$apply();
        });
    });

    $scope.$watch('selectedCity', function(city) {
        if (!city) {
            $scope.annonceLocation.ville = '';
            return;
        }

        $scope.annonceLocation.ville = city.nom;
        $scope.selectedCity = city;
    });
}

 /**
 * Contrôleur du listing des annonces publicitaires en attente de validation.
 * @param $scope
 * @param $filter
 * @param $route
 * @param AnnoncePublicitaire
 * @param MessageService
 * @param Publicitaire
 * @param Mail
 */
function PubListAttenteCtrl($scope, $filter, $route, AnnoncePublicitaire, MessageService, Publicitaire, Mail) {
    $scope.annoncePublicitairesByPage = 10;
    $scope.currentPage = 0;

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    function retrieveAllAnnoncePublicitairesAttente() {
        return $.getJSON('/rest/annoncePublicitaire/etat/Attente');
    }

    $.when(retrieveAllAnnoncePublicitairesAttente()).done(
        function(result) {
            $scope.annoncePublicitaires = result;
            $scope.loaded = true;
            $scope.search();
            $scope.$apply();
        }
    );

    $scope.sort = {
        column: 'url',
        descending: false
    };

    $scope.selectedCls = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    }

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column === column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        $scope.search();
    }

    var searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // calculate page in place
    $scope.groupToPages = function() {
        $scope.pagedAnnoncePublicitaires = [];

        for (var i = 0; i < $scope.filteredAnnoncePublicitaires.length; i++) {
            if (i % $scope.annoncePublicitairesByPage === 0) {
                $scope.pagedAnnoncePublicitaires[Math.floor(i / $scope.annoncePublicitairesByPage)] = [ $scope.filteredAnnoncePublicitaires[i] ];
            } else {
                $scope.pagedAnnoncePublicitaires[Math.floor(i / $scope.annoncePublicitairesByPage)].push($scope.filteredAnnoncePublicitaires[i]);
            }
        }
    };

    // init the filtered locations
    $scope.search = function() {
        $scope.filteredAnnoncePublicitaires = $filter('filter')($scope.annoncePublicitaires, function(annoncePublicitaire) {
            return searchMatch(annoncePublicitaire.url, $scope.query);
        });
        // take care of the sorting order
        $scope.filteredAnnoncePublicitaires = $filter('orderBy')($scope.filteredAnnoncePublicitaires, $scope.sort.column, $scope.sort.descending);
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    // Previous page
    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    // Next page
    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedAnnoncePublicitaires.length - 1) {
            $scope.currentPage++;
        }
    };

    // First page
    $scope.firstPage = function() {
        $scope.currentPage = 0;
    };

    // Last page
    $scope.lastPage = function() {
        $scope.currentPage = $scope.pagedAnnoncePublicitaires.length - 1;
    };

    function getEtatValider() {
        return $.getJSON('/rest/etat/nom/Valider');
    }

    $scope.valider = function(annoncePublicitaire) {
        $scope.clicking = true;
        $.when(getEtatValider()).done(
            function(valider) {
                annoncePublicitaire.etat = valider;

                AnnoncePublicitaire.update(annoncePublicitaire, function(data) {
                    Publicitaire.get({id: annoncePublicitaire.publicitaireId}, function(user) {
                        var mail = {
                            text: 'Votre annonce a été validée.',
                            subject: 'Annonce validée',
                            to: user.email
                        };

                        Mail.save(mail, function(data) {
                            MessageService.addMessage('Validation effectuée');
                            $scope.clicking = false;
                            $route.reload();
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    });
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };
}

/**
 * Contrôleur du listing des offres de location en attente de validation.
 * @param $scope
 * @param $filter
 * @param $route
 * @param AnnonceLocation
 * @param MessageService
 * @param Accueillant
 * @param Mail
 */
function LocationListAttenteController($scope, $filter, $route, AnnonceLocation, MessageService, Accueillant, Mail) {
    $scope.annonceLocationsByPage = 10;
    $scope.currentPage = 0;

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    function retrieveAllAnnonceLocationsAttente() {
        return $.getJSON('/rest/annonceLocation/etat/Attente');
    }

    $.when(retrieveAllAnnonceLocationsAttente()).done(
        function(result) {
            $scope.annonceLocations = result;
            $scope.loaded = true;
            $scope.search();
            $scope.$apply();
        }
    );

    $scope.sort = {
        column: 'ville.nom',
        descending: false
    };

    $scope.selectedCls = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    }

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column === column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        $scope.search();
    }

    var searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // calculate page in place
    $scope.groupToPages = function() {
        $scope.pagedAnnonceLocations = [];

        for (var i = 0; i < $scope.filteredAnnonceLocations.length; i++) {
            if (i % $scope.annonceLocationsByPage === 0) {
                $scope.pagedAnnonceLocations[Math.floor(i / $scope.annonceLocationsByPage)] = [ $scope.filteredAnnonceLocations[i] ];
            } else {
                $scope.pagedAnnonceLocations[Math.floor(i / $scope.annonceLocationsByPage)].push($scope.filteredAnnonceLocations[i]);
            }
        }
    };

    // init the filtered locations
    $scope.search = function() {
        $scope.filteredAnnonceLocations = $filter('filter')($scope.annonceLocations, function(annonceLocation) {
            return searchMatch(annonceLocation.ville.nom, $scope.query);
        });
        // take care of the sorting order
        $scope.filteredAnnonceLocations = $filter('orderBy')($scope.filteredAnnonceLocations, $scope.sort.column, $scope.sort.descending);
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    // Previous page
    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    // Next page
    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedAnnonceLocations.length - 1) {
            $scope.currentPage++;
        }
    };

    // First page
    $scope.firstPage = function() {
        $scope.currentPage = 0;
    };

    // Last page
    $scope.lastPage = function() {
        $scope.currentPage = $scope.pagedAnnonceLocations.length - 1;
    };

    function getEtatValider() {
        return $.getJSON('/rest/etat/nom/Valider');
    }

    function retrieveAllAnnonceLocation(id) {
        return $.getJSON('/rest/annonceLocation/eager/' + id);
    }

    $scope.valider = function(annonceLocation) {
        $scope.clicking = true;
        $.when(retrieveAllAnnonceLocation(annonceLocation.id), getEtatValider()).done(
            function(annonce, valider) {
                annonceLocation = annonce[0];
                annonceLocation.etat = valider[0];

                AnnonceLocation.update(annonceLocation, function(data) {
                    Accueillant.get({id: annonceLocation.accueillantId}, function(user) {
                        var mail = {
                            text: 'Votre annonce concernant l\'offre de location '
                            + 'située à ' + annonceLocation.ville.nom + ' a été validée.',
                            subject: 'Annonce validée',
                            to: user.email
                        };

                        Mail.save(mail, function(data) {
                            MessageService.addMessage('Validation effectuée');
                            $scope.clicking = false;
                            $route.reload();
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    });
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };
}

/**
 * Contrôleur de la liste des locations d'un accueillant.
 * @param $scope
 * @param $filter
 * @param MessageService
 * @param Accueillant
 * @param UserService
 */
function LocationListAccueillantController($scope, $filter, MessageService, Accueillant, UserService) {
    $scope.annonceLocationsByPage = 10;
    $scope.currentPage = 0;

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    Accueillant.get({id: UserService.id}, function(accueillant) {
        $scope.annonceLocations = accueillant.annonceLocations;
        $scope.loaded = true;
        $scope.search();
    }, function(err) {
        $scope.error = true;
        $scope.errorMessage = err.data;
    });

    $scope.sort = {
        column: 'ville.nom',
        descending: false
    };

    $scope.selectedCls = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    }

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column === column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        $scope.search();
    }

    var searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // calculate page in place
    $scope.groupToPages = function() {
        $scope.pagedAnnonceLocations = [];

        for (var i = 0; i < $scope.filteredAnnonceLocations.length; i++) {
            if (i % $scope.annonceLocationsByPage === 0) {
                $scope.pagedAnnonceLocations[Math.floor(i / $scope.annonceLocationsByPage)] = [ $scope.filteredAnnonceLocations[i] ];
            } else {
                $scope.pagedAnnonceLocations[Math.floor(i / $scope.annonceLocationsByPage)].push($scope.filteredAnnonceLocations[i]);
            }
        }
    };

    // init the filtered locations
    $scope.search = function() {
        $scope.filteredAnnonceLocations = $filter('filter')($scope.annonceLocations, function(annonceLocation) {
            return searchMatch(annonceLocation.ville.nom, $scope.query);
        });
        // take care of the sorting order
        $scope.filteredAnnonceLocations = $filter('orderBy')($scope.filteredAnnonceLocations, $scope.sort.column, $scope.sort.descending);
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    // Previous page
    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    // Next page
    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedAnnonceLocations.length - 1) {
            $scope.currentPage++;
        }
    };

    // First page
    $scope.firstPage = function() {
        $scope.currentPage = 0;
    };

    // Last page
    $scope.lastPage = function() {
        $scope.currentPage = $scope.pagedAnnonceLocations.length - 1;
    };
}

/**
 * Contrôleur d'une page détaillée d'annonce publicitaire.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnoncePublicitaire
 * @param Publicitaire
 * @param Mail
 * @param MessageService
 */
function DetailsPubCtrl($scope, $routeParams, $location, AnnoncePublicitaire, Publicitaire, Mail, MessageService) {
    $scope.pub = AnnoncePublicitaire.get({id:$routeParams.id}, function(result) {
        $scope.loaded = true;
        $scope.photoURL = result.photo.data;
        $scope.clickStart = $scope.clickEnd = true;
        $scope.pub = result;
    });

    function getEtatValider() {
        return $.getJSON('/rest/etat/nom/Valider');
    }

    $scope.valider = function(annoncePublicitaire) {
        $scope.clicking = true;
        $.when(getEtatValider()).done(
            function(valider) {
                annoncePublicitaire.etat = valider;

                AnnoncePublicitaire.update(annoncePublicitaire, function(data) {
                    Publicitaire.get({id: annoncePublicitaire.publicitaireId}, function(user) {
                        var mail = {
                            text: 'Votre annonce a été validée.',
                            subject: 'Annonce validée',
                            to: user.email
                        };

                        Mail.save(mail, function(data) {
                            MessageService.addMessage('Validation effectuée');
                            $scope.clicking = false;
                            $location.path('/annoncePublicitaire/list/attente');
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    });
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };
}

/**
 * Modification d'une mauvaise offre publicitaire par un admin.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnoncePublicitaire
 * @param MessageService
 * @param Publicitaire
 * @param Mail
 * @param HostService
 */
function EditerPubAdminCtrl($scope, $routeParams, $location, AnnoncePublicitaire, MessageService, Publicitaire, Mail, HostService) {
    $scope.pub = AnnoncePublicitaire.get({id:$routeParams.id}, function(result) {
        $scope.loaded = true;
        $scope.photoURL = result.photo.data;
        $scope.clickStart = $scope.clickEnd = true;
        $scope.pub = result;
    });

    $scope.master = {};

    $('#photo').change(function(e) {
        var file = e.target.files[0];
        if (file.size > 1000000) {
            $scope.errorPhotoMessage = 'Taille limitée à 1Mo';
            $scope.errorPhoto = true;
            return;
        }
        if (!file.type.match(/image\/jpeg|image\/png/)) {
            $scope.errorPhotoMessage = 'Formats supportés : JPEG, PNG';
            $scope.errorPhoto = true;
            return;
        };

        addAsDataURL(file);
        $scope.$apply();
    });

    function addAsDataURL(file) {
        var reader = new FileReader();

        // affichage d'une miniature retaillée
        reader.onload = function(e) {
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: '/rest/photo/resize',
                data: JSON.stringify(e.target.result),
                dataType: "json",
                success: function(result) {
                    $scope.photoURL = result;
                    $scope.$apply();
                },
                error: function(err){
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.$apply();
                }
            });
        }

        reader.readAsDataURL(file);
    }

    function getEtatBloquer() {
        return $.getJSON('/rest/etat/nom/Bloquer');
    }

    $scope.update = function(pub) {
        $scope.clicking = true;

        $.when(getEtatBloquer()).done(
            function(bloquer) {
                pub.etat = bloquer;

                AnnoncePublicitaire.update(pub, function(data) {
                    Publicitaire.get({id: pub.publicitaireId}, function(user) {
                        var mail = {text: 'Votre annonce n\'est pas conforme.\n', subject: 'Annonce non conforme', to: user.email};
                        mail.text += (HostService.localhost + '#/annoncePublicitaire/edit/' + pub.id);
                        Mail.save(mail, function(data) {
                            MessageService.addMessage('Refus effectué');
                            $location.path('/annoncePublicitaire/list/attente');
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    });
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };

    $scope.deletePhoto = function() {
        $scope.photoURL = null;
        if ($scope.pub.photo) {
            $scope.pub.photo = null;
        }
    };

    $scope.reset = function() {
        $scope.pub = angular.copy($scope.master);
        $scope.photoURL = null;
    };

    $scope.reset();
}

/**
 * Modification d'une mauvaise offre publicitaire par un publicitaire.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnoncePublicitaire
 * @param MessageService
 * @param Publicitaire
 */
function EditerPubCtrl($scope, $routeParams, $location, AnnoncePublicitaire, MessageService, Publicitaire) {
    $scope.pub = AnnoncePublicitaire.get({id:$routeParams.id}, function(result) {
        $scope.loaded = true;
        if (result.photo) {
            $scope.photoURL = result.photo.data;
        }
        $scope.clickStart = $scope.clickEnd = true;
        $scope.pub = result;

        if ($scope.pub.etat.name == 'Bloquer') {
            for (var key in $scope.form) {
                if (key.indexOf("$") === -1) {
                    $scope.form[key].$dirty = true;
                }
            }
            if (!result.photo) {
                $scope.errorPhoto = true;
                $scope.errorPhotoMessage = 'Photo invalide';
            }
        }
    });

    $scope.master = {};

    $('#photo').change(function(e) {
        var file = e.target.files[0];
        if (file.size > 1000000) {
            $scope.errorPhotoMessage = 'Taille limitée à 1Mo';
            $scope.errorPhoto = true;
            return;
        }
        if (!file.type.match(/image\/jpeg|image\/png/)) {
            $scope.errorPhotoMessage = 'Formats supportés : JPEG, PNG';
            $scope.errorPhoto = true;
            return;
        };

        $scope.errorPhoto = false;

        addAsDataURL(file);
        $scope.$apply();
    });

    function addAsDataURL(file) {
        var reader = new FileReader();

        // affichage d'une miniature retaillée
        reader.onload = function(e) {
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: '/rest/photo/resize',
                data: JSON.stringify(e.target.result),
                dataType: "json",
                success: function(result) {
                    $scope.photoURL = result;
                    $scope.$apply();
                },
                error: function(err){
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.$apply();
                }
            });
        }

        reader.readAsDataURL(file);
    }

    function getEtatAttente() {
        return $.getJSON('/rest/etat/nom/Attente');
    }

    $scope.update = function(pub) {
        $scope.clicking = true;

        $.when(getEtatAttente()).done(
            function(attente) {
                if (pub.etat.name == 'Bloquer') {
                    pub.etat = attente;
                }
                pub.photo = $scope.photoURL;
                AnnoncePublicitaire.update(pub, function(data) {
                    MessageService.addMessage('Modification effectuée');
                    $location.path('/');
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };

    $scope.deletePhoto = function() {
        $scope.photoURL = null;
        if ($scope.pub.photo) {
            $scope.pub.photo = null;
        }
    };

    $scope.reset = function() {
        $scope.pub = angular.copy($scope.master);
        $scope.photoURL = null;
    };

    $scope.reset();
}

/**
 * Modification d'une mauvaise annonce de location par un admin.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnonceLocation
 * @param Service
 * @param Activite
 * @param MessageService
 * @param Accueillant
 * @param Mail
 * @param HostService
 */
function EditerOffreAdminCtrl($scope, $routeParams, $location, AnnonceLocation, Service, Activite, MessageService, Accueillant, Mail, HostService) {
    $scope.services = Service.query();
    $scope.activites = Activite.query();
    $scope.master = {};
    $scope.noSelection = [];

    $scope.reset = function() {
        $scope.clicking = true;
        $scope.annonceLocation = angular.copy($scope.master);
        $scope.locationTente = angular.copy($scope.master);
        $scope.locationCC = angular.copy($scope.master);
        $scope.locationCaravane = angular.copy($scope.master);
        $scope.photosURL = angular.copy($scope.noSelection);
        $scope.selectedServices = angular.copy($scope.noSelection);
        $scope.selectedActivites = angular.copy($scope.noSelection);
        $scope.villes = angular.copy($scope.noSelection);
        $('input[type=checkbox]').prop('checked', false);
        $scope.clicking = false;
    };

    function retrieveAllAnnonceLocation() {
        return $.getJSON('/rest/annonceLocation/eager/' + $routeParams.id);
    }

    $.when(retrieveAllAnnonceLocation()).done(
        function (result) {
            $scope.reset();
            $scope.annonceLocation = result;

            angular.forEach($scope.annonceLocation.locations, function(location) {
                if (location.type.name == 'Tente') {
                    $scope.tenteShow = true;
                    $scope.locationTente.places = location.places;
                    $scope.locationTente.prix = location.prix;
                }
                if (location.type.name == 'Camping-car') {
                    $scope.CCShow = true;
                    $scope.locationCC.places = location.places;
                    $scope.locationCC.prix = location.prix;
                }
                if (location.type.name == 'Caravane') {
                    $scope.caravaneShow = true;
                    $scope.locationCaravane.places = location.places;
                    $scope.locationCaravane.prix = location.prix;
                }
            });

            angular.forEach($scope.annonceLocation.photos, function(photo) {
                $scope.photosURL.push(photo.data);
                $scope.form.photos.$dirty = true;
            });

            // Conflit AngularJS/JQuery
            setTimeout(function() {
                angular.forEach($scope.annonceLocation.services, function(service) {
                    $('#service' + service.id).prop('checked', true);
                    $scope.selectedServices.push(service);
                });
            }, 1000)

            setTimeout(function() {
                angular.forEach($scope.annonceLocation.activites, function(activite) {
                    $('#activite' + activite.id).prop('checked', true);
                    $scope.selectedActivites.push(activite);
                });
                $scope.loaded = true;
                $scope.$apply();
            }, 1000);

            $scope.$apply();
        }
    );

    function checkTente() {
        if ($scope.tenteShow) {
            return $.getJSON('/rest/typeLocation/nom/Tente');
        }
    }

    function checkCampingCar() {
        if ($scope.CCShow) {
            return $.getJSON('/rest/typeLocation/nom/Camping-car');
        }
    }

    function checkCaravane() {
        if ($scope.caravaneShow) {
            return $.getJSON('/rest/typeLocation/nom/Caravane');
        }
    }

    $scope.update = function(annonceLocation) {
        $scope.clicking = true;
        var locations = [];

        $.when(checkTente(), checkCampingCar(), checkCaravane()).done(
            function(tente, campingCar, caravane) {
                if (tente) {
                    locations.push($scope.locationTente);
                    $scope.locationTente.type = tente[0];
                }
                if (campingCar) {
                    locations.push($scope.locationCC);
                    $scope.locationCC.type = campingCar[0];
                }
                if (caravane) {
                    locations.push($scope.locationCaravane);
                    $scope.locationCaravane.type = caravane[0];
                }

                annonceLocation.locations = locations;
                annonceLocation.photos = $scope.photosURL;
                annonceLocation.services = $scope.selectedServices;
                annonceLocation.activites = $scope.selectedActivites;

                function getEtatBloquer() {
                    return $.getJSON('/rest/etat/nom/Bloquer');
                }

                $.when(getEtatBloquer()).done(
                    function(bloquer) {
                        annonceLocation.etat = bloquer;

                        AnnonceLocation.update(annonceLocation, function(data) {
                            Accueillant.get({id: annonceLocation.accueillantId}, function(user) {
                                var mail = {
                                    text: 'Votre annonce n\'est pas conforme.\n'
                                        + 'Veuillez vous connecter puis accéder à cette adresse '
                                        + 'pour corriger les champs incorrects : ',
                                    subject: 'Annonce non conforme',
                                    to: user.email
                                };
                                mail.text += (HostService.localhost + '#/location/edit/' + annonceLocation.id);
                                Mail.save(mail, function(data) {
                                    MessageService.addMessage('Refus effectué');
                                    $location.path('/location/list/attente');
                                }, function(err) {
                                    $scope.error = true;
                                    $scope.errorMessage = err.data;
                                    $scope.clicking = false;
                                });
                            });
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    }
                );
            }
        );
    };

    $scope.deletePhoto = function(index) {
        $scope.photosURL.splice(index, 1);
    };

    $scope.$watch('annonceLocation.codePostal', function(cp) {
        if (!cp) {
            $scope.villes = [];
            $scope.cpInvalid = true;
            return;
        }
        $scope.cpInvalid = false;

        function getVillesByCP() {
            return $.getJSON('/rest/ville/CP/' + cp);
        }

        $.when(getVillesByCP()).done(function(villes) {
            if (villes.length == 0) {
                $scope.cpInvalid = true;
            }
            $scope.villes = villes;
            $scope.$apply();
        });
    });

    $scope.changeService = function(service) {
        function myIndexOf(service) {
            for (var k = 0, s; s = $scope.selectedServices[k]; ++k) {
                if (s.name == service.name) {
                    return k;
                }
            }
            return -1;
        }
        var index = myIndexOf(service);
        if (index == -1) {
            $scope.selectedServices.push(service);
        } else {
            $scope.selectedServices.splice(index, 1);
        }
    };

    $scope.changeActivite = function(activite) {
        function myIndexOf(activite) {
            for (var k = 0, s; s = $scope.selectedActivites[k]; ++k) {
                if (s.name == activite.name) {
                    return k;
                }
            }
            return -1;
        }
        var index = myIndexOf(activite);
        if (index == -1) {
            $scope.selectedActivites.push(activite);
        } else {
            $scope.selectedActivites.splice(index, 1);
        }
    };
}

/**
 * Contrôleur de modification d'une offre de location.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnonceLocation
 * @param Service
 * @param Activite
 * @param MessageService
 * @param UserService
 * @param Accueillant
 */
function EditerOffreCtrl($scope, $routeParams, $location, AnnonceLocation, Service, Activite, MessageService, UserService, Accueillant) {
    $scope.services = Service.query();
    $scope.activites = Activite.query();
    $scope.master = {};
    $scope.noSelection = [];

    $scope.marker = null;
    $scope.map = null;

    $scope.removeMarker = function() {
        if (!$scope.marker) {
            return;
        }
        $scope.marker.setMap(null);
        $scope.marker = null;
    };

    function initializeGoogleMap() {
        var mapOptions = {
            zoom: 6,
            center: new google.maps.LatLng(48,2),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        $scope.map = new google.maps.Map($('#map-canvas').get(0), mapOptions);

        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);
            $scope.$apply();
        });

        function placeMarker(location) {
            if ($scope.marker) {
                return;
            }

            $scope.marker = new google.maps.Marker({
                position: location,
                map: $scope.map,
                draggable: true
            });

            $scope.marker.setMap($scope.map);

            var infowindow = new google.maps.InfoWindow({
                content: 'Latitude: ' + location.lat() +
                    '<br>Longitude: ' + location.lng()
            });

            google.maps.event.addListener($scope.marker, 'dragend', function(){
                var point = $scope.marker.getPosition();
                infowindow.content = 'Latitude: ' + point.lat() +
                    '<br>Longitude: ' + point.lng();
                infowindow.close();
                infowindow.open($scope.map, $scope.marker);
            });
            infowindow.open($scope.map, $scope.marker);
        }
    }

//    initializeGoogleMap();

    $('#photos').change(function(e) {
        if ($scope.photosURL.length == 10) {
            $scope.errorPhotoMessage = 'Limite du nombre de photos atteinte';
            $scope.errorPhoto = true;
            return;
        }

        var files = e.target.files;
        var lastFile = files[files.length - 1];
        if (lastFile.size > 1000000) {
            $scope.errorPhotoMessage = 'Taille limitée à 1Mo';
            $scope.errorPhoto = true;
            return;
        }
        if (!lastFile.type.match(/image\/jpeg|image\/png/)) {
            $scope.errorPhotoMessage = 'Formats supportés : JPEG, PNG';
            $scope.errorPhoto = true;
            return;
        };

        addAsDataURL(lastFile);
        $scope.$apply();
    });

    function addAsDataURL(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $scope.photosURL.push(e.target.result);
            $scope.$apply();
        }

        reader.readAsDataURL(file);
    }

    $scope.deletePhoto = function(index) {
        $scope.photosURL.splice(index, 1);
    };

    $scope.changeService = function(service) {
        function myIndexOf(service) {
            for (var k = 0, s; s = $scope.selectedServices[k]; ++k) {
                if (s.name == service.name) {
                    return k;
                }
            }
            return -1;
        }
        var index = myIndexOf(service);
        if (index == -1) {
            $scope.selectedServices.push(service);
        } else {
            $scope.selectedServices.splice(index, 1);
        }
    };

    $scope.changeActivite = function(activite) {
        function myIndexOf(activite) {
            for (var k = 0, s; s = $scope.selectedActivites[k]; ++k) {
                if (s.name == activite.name) {
                    return k;
                }
            }
            return -1;
        }
        var index = myIndexOf(activite);
        if (index == -1) {
            $scope.selectedActivites.push(activite);
        } else {
            $scope.selectedActivites.splice(index, 1);
        }
    };

    $scope.reset = function() {
        $scope.clicking = true;
        var etat = null;
        if ($scope.annonceLocation) {
            etat = $scope.annonceLocation.etat;
        }
        $scope.annonceLocation = angular.copy($scope.master);
        $scope.annonceLocation.id = $routeParams.id;
        if (etat) {
            $scope.annonceLocation.etat = etat;
        }
        $scope.locationTente = angular.copy($scope.master);
        $scope.locationCC = angular.copy($scope.master);
        $scope.locationCaravane = angular.copy($scope.master);
        $scope.photosURL = angular.copy($scope.noSelection);
        $scope.selectedServices = angular.copy($scope.noSelection);
        $scope.selectedActivites = angular.copy($scope.noSelection);
        $scope.villes = angular.copy($scope.noSelection);
        $('input[type=checkbox]').prop('checked', false);
        $scope.clicking = false;
    };

    $scope.reset();

    function retrieveAllAnnonceLocation() {
        return $.getJSON('/rest/annonceLocation/eager/' + $routeParams.id);
    }

    $.when(retrieveAllAnnonceLocation()).done(
        function (result) {
            $scope.annonceLocation = result;
            $scope.selectedCity = $scope.annonceLocation.ville;
            $scope.annonceLocation.codePostal = null;

            if ($scope.annonceLocation.etat.name == 'Bloquer') {
                for (var k = 0, loc; loc = $scope.annonceLocation.locations[k]; ++k) {
                    if (loc.type.name == 'Tente' && (!loc.places || !loc.prix)) {
                        $scope.correctTente = true;
                    }
                    if (loc.type.name == 'Camping-car' && (!loc.places || !loc.prix)) {
                        $scope.correctCC = true;
                    }
                    if (loc.type.name == 'Caravane' && (!loc.places || !loc.prix)) {
                        $scope.correctCaravane = true;
                    }
                }

                for (var key in $scope.form) {
                    if (key.indexOf("$") === -1) {
                        $scope.form[key].$dirty = true;
                    }
                }
            }

            angular.forEach($scope.annonceLocation.locations, function(location) {
                if (location.type.name == 'Tente') {
                    $scope.tenteShow = true;
                    $scope.locationTente.places = location.places;
                    $scope.locationTente.prix = location.prix;
                }
                if (location.type.name == 'Camping-car') {
                    $scope.CCShow = true;
                    $scope.locationCC.places = location.places;
                    $scope.locationCC.prix = location.prix;
                }
                if (location.type.name == 'Caravane') {
                    $scope.caravaneShow = true;
                    $scope.locationCaravane.places = location.places;
                    $scope.locationCaravane.prix = location.prix;
                }
            });

            angular.forEach($scope.annonceLocation.photos, function(photo) {
                $scope.photosURL.push(photo.data);
                $scope.form.photos.$dirty = true;
            });

            // Conflit AngularJS/JQuery
            setTimeout(function() {
                angular.forEach($scope.annonceLocation.services, function(service) {
                    $('#service' + service.id).prop('checked', true);
                    $scope.selectedServices.push(service);
                });
            }, 1000)

            setTimeout(function() {
                angular.forEach($scope.annonceLocation.activites, function(activite) {
                    $('#activite' + activite.id).prop('checked', true);
                    $scope.selectedActivites.push(activite);
                });
                $scope.loaded = true;
                $scope.$apply();
            }, 1000);

            $scope.$apply();
        }
    );

    function checkTente() {
        if ($scope.tenteShow) {
            return $.getJSON('/rest/typeLocation/nom/Tente');
        }
    }

    function checkCampingCar() {
        if ($scope.CCShow) {
            return $.getJSON('/rest/typeLocation/nom/Camping-car');
        }
    }

    function checkCaravane() {
        if ($scope.caravaneShow) {
            return $.getJSON('/rest/typeLocation/nom/Caravane');
        }
    }

    function getEtatAttente() {
        return $.getJSON('/rest/etat/nom/Attente');
    }

    $scope.update = function(annonceLocation) {
        $scope.clicking = true;
        var locations = [];

        $.when(checkTente(), checkCampingCar(), checkCaravane(), getEtatAttente()).done(
            function(tente, campingCar, caravane, etatAttente) {
                if (tente) {
                    locations.push($scope.locationTente);
                    $scope.locationTente.type = tente[0];
                }
                if (campingCar) {
                    locations.push($scope.locationCC);
                    $scope.locationCC.type = campingCar[0];
                }
                if (caravane) {
                    locations.push($scope.locationCaravane);
                    $scope.locationCaravane.type = caravane[0];
                }

                function searchByName(name) {
                    return $.getJSON('/rest/ville/name/' + name);
                }

                function save() {
                    annonceLocation.locations = locations;
                    annonceLocation.photos = $scope.photosURL;
                    annonceLocation.services = $scope.selectedServices;
                    annonceLocation.activites = $scope.selectedActivites;

                    if (annonceLocation.etat.name == 'Bloquer') {
                        annonceLocation.etat = etatAttente[0];
                        $scope.correctTente = false;
                        $scope.correctCC = false;
                        $scope.correctCaravane = false;
                    }

                    AnnonceLocation.update(annonceLocation, function(data) {
                        $scope.info = true;
                        $scope.infoMessage = 'Offre de location modifiée';
                        $scope.clicking = false;
                    }, function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                        $scope.clicking = false;
                    });
                }

                if ($scope.marker) {
                    $.when(searchByName(annonceLocation.ville.toUpperCase())).done(function(result) {
                        if (result.length > 0) {
                            $scope.errorMap = true;
                            $scope.errorMapMessage = 'Impossible de redéfinir les coordonnées d\'une ville existante';
                            $scope.clicking = false;
                            $scope.$apply();
                        } else {
                            var p = $scope.marker.getPosition();
                            var ville = {nom: annonceLocation.ville, latitude: p.lat(), longitude: p.lng(),
                                codePostal: annonceLocation.codePostal, departement: ''};
                            Ville.save(ville, function(data) {
                                $.when(searchByName(ville.nom)).done(function(result) {
                                    $scope.annonceLocation.ville = result[0];
                                    save();
                                });
                            }, function(err) {
                                $scope.error = true;
                                $scope.errorMessage = err.data;
                            });
                        }
                    });
                } else {
                    $.when(searchByName(annonceLocation.ville)).done(function(result) {
                        if (result.length == 0) {
                            $scope.errorMap = true;
                            $scope.errorMapMessage = 'Ville inexistante ! Choisissez-en une depuis la liste ' +
                                'ou marquez ses coordonnées sur la carte';
                            $scope.clicking = false;
                            $scope.$apply();
                        } else {
                            annonceLocation.ville = $scope.selectedCity;
                            save();
                            $scope.$apply();
                        }
                    });
                }
            }
        );
    }

    $scope.delete = function(annonceLocation) {
        function myIndexOf(accueillant) {
            for (var k = 0, a; a = accueillant.annonceLocations[k]; ++k) {
                if (a.id == annonceLocation.id) {
                    return k;
                }
            }
            return -1;
        }

        Accueillant.get({id:UserService.id}, function(data) {
            if (!window.confirm('Etes-vous certain de vouloir supprimer cette offre ?')) {
                return;
            }

            $scope.clicking = true;

            var accueillant = data;
            accueillant.annonceLocations.splice(myIndexOf(accueillant), 1);
            Accueillant.update(accueillant);

            AnnonceLocation.delete({id:annonceLocation.id}, function(data) {
                MessageService.addMessage('Location supprimée !');
                $location.path('/location/list/' + UserService.id);
            }, function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
        });
    };

    $scope.$watch('annonceLocation.codePostal', function(cp) {
        if (!cp) {
            $scope.villes = [];
            $scope.cpInvalid = true;
            return;
        }
        $scope.cpInvalid = false;

        function getVillesByCP() {
            return $.getJSON('/rest/ville/CP/' + cp);
        }

        $.when(getVillesByCP()).done(function(villes) {
            if (villes.length == 0) {
                $scope.cpInvalid = true;
            }
            $scope.villes = villes;
            $scope.$apply();
        });
    });

    $scope.$watch('selectedCity', function(city) {
        if (!city) {
            return;
        }

        $scope.annonceLocation.ville = city.nom;
    });
}

/**
 * Contrôleur de contact d'un accueillant.
 * @param $scope
 * @param $routeParams
 * @param Accueillant
 * @param Mail
 */
function ContacterAccueillantCtrl($scope, $routeParams, Accueillant, Mail) {
	$scope.user = Accueillant.get({id:$routeParams.id});

	$scope.sendMail = function(user, mail) {
        $scope.clicking = true;
        mail.to = user.email;

        Mail.save(mail, function(data) {
            $scope.info = true;
            $scope.infoMessage = 'Le mail a bien été envoyé';
            $scope.clicking = false;
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
        });
	};
}

/**
 * Contrôleur pour la confirmation de suppression d'un compte utilisateur.
 * @param $scope
 * @param $routeParams
 * @param $http
 * @param $location
 * @param MessageService
 * @param UserService
 * @param Accueillant
 */
function ConfirmSuppressionCtrl($scope, $routeParams, $http, $location, MessageService, UserService, Accueillant) {
    $scope.password = {display:'password'};
    $scope.user = Accueillant.get({id:$routeParams.id}, function() {
        $scope.user.pseudo = $scope.user.password = null;
    });

    $scope.delete = function(user) {
        if (!window.confirm('Etes-vous certain de vouloir supprimer votre compte ?')) {
            return;
        }
        $scope.clicking = true;
        $http.delete('/rest/accueillant/' + user.id)
            .success(function() {
                MessageService.addMessage('Compte supprimé !');
                UserService.isLogged = false;
                $http.post('/rest/deconnexion');
                $location.path('/');
            }).error(function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
    };

    /**
     * Cache/affiche les mots de passe.
     */
    $scope.switchPasswordDisplaying = function(display) {
        $scope.password.display = (display) ? 'password' : 'text';
    };
}

/**
 * Contrôleur de connexion d'un utilisateur.
 * @param $scope
 * @param $http
 * @param $location
 * @param MessageService
 * @param UserService
 */
function ConnexionCtrl($scope, $http, $location, MessageService, UserService) {
    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    $scope.connect = function(user) {
        $scope.clicking = true;
        $http.post('/rest/connexion/' + user.pseudo + '/' + user.password)
            .success(function(data) {
                UserService.isLogged = true;
                UserService.id = data.id;
                UserService.role = data.role;
                MessageService.addMessage('Vous êtes dorénavant connecté');

                $location.path('/accueil');
            }).error(function(err, status) {
                $scope.error = true;
                if (status == 404) {
                    $scope.errorMessage = 'Utilisateur ou mot de passe invalide';
                } else if (status == 401) {
                    $scope.errorMessage = 'Votre compte a été banni';
                } else {
                    $scope.errorMessage = 'Erreur interne au serveur';
                }
                $scope.clicking = false;
            });
    };
}

/**
 * Contrôleur de la page de menu.
 * @param $scope
 * @param $http
 * @param $location
 * @param UserService
 * @param MessageService
 * @param TypeLocation
 * @param Service
 * @param GeoService
 */
function MenuCtrl($scope, $http, $location, UserService, MessageService, TypeLocation, Service, GeoService) {
    $scope.master = {};
    $scope.noSelection = [];
    $scope.services = Service.query();
    $scope.typesLocation = TypeLocation.query();

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    $.when(getPublicite()).done(function(result) {
        $scope.ad = result;
        $scope.$apply();
    });

    function getAllValiderEtDisponibles() {
        return $.getJSON('/rest/annonceLocation/disponible');
    }

    function getPublicite() {
        return $.getJSON('/rest/annoncePublicitaire/displayable');
    }

    function applyFilterOnOccup(filter) {
        function isBetween(date1, date2, date3) {
            // évite les erreurs de calcul dues aux heures, minutes et ms
            date2 = new Date(date2);
            if ((date1.getYear() == date2.getYear()) && (date1.getMonth() == date2.getMonth())
                && (date1.getDay() == date2.getDay())) {
                return true;
            }
            return (Date.parse(date2) <= Date.parse(date1)) && (Date.parse(date1) <= Date.parse(date3));
        }

        $scope.alreadyTaken = [];

        //Si pas de filtre, on se base sur la date actuelle
        if (!filter) {
            var rightNow = new Date();
            for (var k = 0, a; a = $scope.annonceLocations[k]; ++k) {
                for (var m = 0, p; p = a.plannings[m]; ++m) {
                    if (isBetween(rightNow, p.start, p.end)) {
                        $scope.alreadyTaken.push(a);
                        break;
                    }
                }
            }
        } else {
            //Si le filtre contient une date de début ou de fin, une annonce avec un planning
            //qui englobe l'une de ces dates est notée comme prise
            if (filter.dateDebut) {
                for (var k = 0, a; a = $scope.annonceLocations[k]; ++k) {
                    for (var m = 0, p; p = a.plannings[m]; ++m) {
                        if (isBetween(filter.dateDebut, p.start, p.end)) {
                            $scope.alreadyTaken.push(a);
                            break;
                        }
                    }
                }
            } else {
                if (filter.dateFin) {
                    for (var k = 0, a; a = $scope.annonceLocations[k]; ++k) {
                        for (var m = 0, p; p = a.plannings[m]; ++m) {
                            if (isBetween(filter.dateFin, p.start, p.end)) {
                                $scope.alreadyTaken.push(a);
                                break;
                            }
                        }
                    }
                }
                //Si le filtre ne contient pas de contraintes de dates, tout est bon
            }
        }

        var totalLength = $scope.annonceLocations.length;

        for (var k = 0, a; a = $scope.alreadyTaken[k]; ++k) {
            $scope.annonceLocations.splice($scope.annonceLocations.indexOf(a), 1);
        }

        return totalLength;
    }

    $.when(getAllValiderEtDisponibles()).done(function(result) {
        $scope.annonceLocations = result;

        initializeGoogleMap();
        applyFilterOnOccup(null);

        $scope.$apply();
    });

    function initializeGoogleMap() {
        var mapOptions = {
            zoom: 6,
            center: new google.maps.LatLng(48,2),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map($('#map-canvas').get(0), mapOptions);

        for (var k = 0, a; a = $scope.annonceLocations[k]; ++k) {
            ajouteMarqueur({lat: a.ville.latitude, lng: a.ville.longitude}, a);
        }

        var options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };

        navigator.geolocation.getCurrentPosition(success, error, options);

        function ajouteMarqueur(latlng, location) {
            var optionsMarqueur = {
                map: map,
                position: new google.maps.LatLng(latlng.lat, latlng.lng),
                draggable: true,
                icon: 'css/css/images/location.png'
            };

            var marqueur = new google.maps.Marker(optionsMarqueur);
            var zoneMarqueurs = new google.maps.LatLngBounds();
            var infowindow = null;
            zoneMarqueurs.extend(marqueur.getPosition());

            google.maps.event.addListener(marqueur, 'click', function() {
                $location.path('/details-offre/' + location.id);
                $scope.$apply();
            });

            google.maps.event.addListener(marqueur, 'mouseover', function() {
                $http.post('/rest/annonceLocation/ville/', location.ville)
                    .success(function(result) {
                        infowindow = new google.maps.InfoWindow({
                            content: location.ville.nom + '(' + (result - 1) + ' offre(s) similaire(s))'
                        });
                        infowindow.open(map, marqueur);
                    }).error(function(err) {
                        $scope.error = true;
                        $scope.errorMessage = err.data;
                        $scope.clicking = false;
                    });
            });

            google.maps.event.addListener(marqueur, 'mouseout', function() {
                infowindow.close();
            });
        }

        function success(pos) {
            GeoService.setLatitude(pos.coords.latitude);
            GeoService.setLongitude(pos.coords.longitude);

            var optionsMarqueur = {
                map: map,
                position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                draggable: true,
                title:"Vous êtes ici",
                icon: "css/css/images/smiley_happy.png",
                zIndex: 9999
            };

            var marqueur = new google.maps.Marker(optionsMarqueur);
            var zoneMarqueurs = new google.maps.LatLngBounds();
            zoneMarqueurs.extend(marqueur.getPosition());
            $scope.$apply();
        }

        function error(err) {
//            $scope.error = true;
//            $scope.errorMessage = err.message;
        }
    }

    if (!UserService.isLogged) {
        $http.get('/rest/getSession')
            .success(function(session) {
                if (session) {
                    $scope.isLogged = UserService.isLogged = true;
                    $scope.id = UserService.id = session.id;
                    $scope.role = UserService.role = session.role;
                } else {
                    $scope.isLogged = false;
                }
            }).error(function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
            });
    } else {
        $scope.isLogged = UserService.isLogged;
        $scope.role = UserService.role;
        $scope.id = UserService.id;
    }

    $scope.changeService = function(service) {
        var index = $scope.selectedServices.indexOf(service);
        if (index == -1) {
            $scope.selectedServices.push(service);
        } else {
            $scope.selectedServices.splice(index, 1);
        }
    };

    $scope.disconnect = function() {
        $http.post('/rest/deconnexion')
            .success(function() {
                MessageService.addMessage('Déconnecté');
                $scope.isLogged = UserService.isLogged = false;
                $location.path('/');
            }).error(function(err) {
                $scope.errorMessage = err.data;
                $scope.error = true;
            });
    };

    $scope.search = function(filter) {
        var btn = $('#btn-search-advance');
        btn.button('loading');

        filter.services = $scope.selectedServices;
        $scope.clicking = true;

        $http.post('/rest/annonceLocation/filter', filter)
            .success(function(result) {
                $scope.annonceLocations = result;
                $scope.clicking = false;
                var annonceNb = applyFilterOnOccup(filter);
                initializeGoogleMap();
                $scope.info = true;
                $scope.infoMessage = annonceNb + ' location(s) trouvée(s)';
                btn.button('reset');
            }).error(function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
    };

    $scope.filterFromDistance = function(filter) {
        var btn = $('#btn-search-distance');
        btn.button('loading');

        $scope.clicking = true;
        $http.get('/rest/annonceLocation/rayon/'
                + filter.rayon + '/'
                + GeoService.getLatitude() + '/'
                + GeoService.getLongitude()
            )
            .success(function(result) {
                $scope.annonceLocations = result;
                $scope.clicking = false;
                $scope.info = true;
                $scope.infoMessage = result.length + ' location(s) trouvée(s)';
                applyFilterOnOccup(filter);
                initializeGoogleMap();
                btn.button('reset');
            }).error(function(err) {
                $scope.error = true;
                $scope.errorMessage = err.data;
                $scope.clicking = false;
            });
    };

    $scope.canFilterAgainstCoords = function() {
        return GeoService.getLatitude()
            && GeoService.getLongitude();
    };

    $scope.reset = function() {
        $scope.filter = angular.copy($scope.master);
        $scope.selectedServices = angular.copy($scope.noSelection);
        $scope.filteredSearch = $scope.filteredDistance = false;
    };

    $scope.reset();
}

/**
 * Contrôleur des détails d'une offre de location.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param UserService
 * @param AnnonceLocation
 * @param MessageService
 * @param QuestionQuestionnaire
 * @param Accueillant
 * @param Mail
 */
function DetailsOffreCtrl($scope, $routeParams, $location, UserService, AnnonceLocation, MessageService, QuestionQuestionnaire, Accueillant, Mail) {
    $scope.avertos = [];
    $scope.location = AnnonceLocation.get({id:$routeParams.id}, function() {
        function retrieveAvertissements() {
            return $.getJSON('/rest/accueillant/averto/' + $scope.location.accueillantId);
        }

        $.when(retrieveAvertissements()).done(
            function(result) {
                $scope.avertos = result;
                $scope.loaded = true;
                $scope.$apply();
            }
        );

        averageAllQuestions();
    });

    $scope.questions = QuestionQuestionnaire.query();

    if (MessageService.hasMessages()) {
        $scope.info = true;
        var messages = MessageService.consumMessages();
        $scope.infoMessage = messages[0];
    }

    $scope.rate = 0;
    $scope.max = 5;
    $scope.isReadonly = false;

    if (UserService.isLogged) {
        $scope.isLogged = UserService.isLogged;
        $scope.role = UserService.role;
        $scope.id = UserService.id;
    }

    $scope.hoveringOver = function(value) {
        $scope.overStar = value;
        $scope.percent = 100 * (value / $scope.max);
    };

    $scope.save = function(avis) {
        avis.note = $scope.rate;
        $scope.location.avis.push(avis);

        $scope.clicking = true;
        AnnonceLocation.update($scope.location, function(data) {
            $scope.clicking = false;
            $scope.info = true;
            $scope.infoMessage = 'Commentaire posté. N\'oubliez pas que ' +
                'les administrateurs se réservent le droit de modérer votre avis.';
            $scope.avis = {};
            $scope.form.avis.$dirty = false;
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };

    $scope.remove = function(avis) {
        function myIndexOf(avis) {
            for (var k = 0, a; a = $scope.location.avis[k]; ++k) {
                if (a.id == avis.id) {
                    return k;
                }
            }
            return -1;
        }
        if (!window.confirm('Etes-vous certain de vouloir supprimer ce commentaire ?')) {
            return;
        }
        $scope.location.avis.splice(myIndexOf(avis), 1);

        $scope.clicking = true;
        AnnonceLocation.update($scope.location, function(data) {
            $scope.clicking = false;
            $scope.info = true;
            $scope.infoMessage = 'Commentaire supprimé';
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };

    $scope.average = function() {
        if (!$scope.location.avis || !$scope.location.avis.length) {
            return 0;
        } else {
            var sum = 0;
            for (var k = 0, a; a = $scope.location.avis[k]; ++k) {
                sum += a.note;
            }
            return sum / $scope.location.avis.length;
        }
    };

    function averageAllQuestions() {
        $scope.averagePerQuestion = [];
        for (var k = 0, length = $scope.questions.length; k < length; ++k) {
            $scope.averagePerQuestion[k] = 0;
        }

        for (var k = 0, q; q = $scope.location.questionnaires[k]; ++k) {
            for (var m = 0, r; r = q.reponseQuestionnaire[m]; ++m) {
                $scope.averagePerQuestion[m] += r.reponse;
            }
        }

        for (var k = 0, av; av = $scope.averagePerQuestion[k]; ++k) {
            $scope.averagePerQuestion[k] /= $scope.location.questionnaires.length;
        }
    }

    function getEtatValider() {
        return $.getJSON('/rest/etat/nom/Valider');
    }

    function retrieveAllAnnonceLocation(id) {
        return $.getJSON('/rest/annonceLocation/eager/' + id);
    }

    $scope.valider = function(annonceLocation) {
        $scope.clicking = true;
        $.when(retrieveAllAnnonceLocation(annonceLocation.id), getEtatValider()).done(
            function(annonce, valider) {
                annonceLocation = annonce[0];
                annonceLocation.etat = valider[0];

                AnnonceLocation.update(annonceLocation, function(data) {
                    Accueillant.get({id: annonceLocation.accueillantId}, function(user) {
                        var mail = {
                            text: 'Votre annonce concernant l\'offre de location '
                                + 'située à ' + annonceLocation.ville.nom + ' a été validée.',
                            subject: 'Annonce validée',
                            to: user.email
                        };

                        Mail.save(mail, function(data) {
                            MessageService.addMessage('Validation effectuée');
                            $scope.clicking = false;
                            $location.path('/location/list/attente');
                        }, function(err) {
                            $scope.error = true;
                            $scope.errorMessage = err.data;
                            $scope.clicking = false;
                        });
                    });
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err.data;
                    $scope.clicking = false;
                });
            }
        );
    };
}

/**
 * Contrôleur d'envoi de questionnaires.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnonceLocation
 * @param Mail
 * @param MessageService
 * @param UserService
 * @param HostService
 */
function AjoutQuestionnaireCtrl($scope, $routeParams, $location, AnnonceLocation, Mail, MessageService, UserService, HostService) {
    $scope.location = AnnonceLocation.get({id:$routeParams.id});

    function createMessage(adresse) {
        var mail = {};
        mail.subject = 'Questionnaire de satisfaction - ' + $scope.location.ville.nom;
        mail.to = adresse.to;
        mail.text = 'Le site de camping de particuliers à particuliers vous propose de répondre à un questionnaire.\n';
        mail.text += 'Il vous suffit de suivre le lien ci-dessous, merci !\n\n';
        mail.text += HostService.localhost + '#/location/questionnaire/' + $scope.location.id;
        return mail;
    }

    $scope.sendQuestionnaire = function(adresse) {
        $scope.clicking = true;
        Mail.save(createMessage(adresse), function(data) {
            MessageService.addMessage('Questionnaire envoyé');
            $location.path('/location/list/' + UserService.id);
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}

/**
 * Contrôleur des questionnaires portant sur une offre de location.
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param AnnonceLocation
 * @param QuestionQuestionnaire
 * @param MessageService
 */
function QuestionnaireCtrl($scope, $routeParams, $location, AnnonceLocation, QuestionQuestionnaire, MessageService) {
    $scope.location = AnnonceLocation.get({id:$routeParams.id});
    $scope.questions = QuestionQuestionnaire.query();
    $scope.notes = [];

    $scope.soumettre = function() {
        var reponses = [];
        var length = $scope.questions.length;

        for (var k = 0;k < length; ++k) {
            var q = $scope.questions[k];
            reponses.push({
                reponse: $scope.notes[k],
                questionQuestionnaire: {
                    id: q.id,
                    question: q.question
                }
            });
        }

        var avisQuestionnaire = {};
        avisQuestionnaire.reponseQuestionnaire = reponses;
        if (!$scope.location.questionnaires) {
            $scope.location.questionnaires = [];
        }


        $scope.location.questionnaires.push(avisQuestionnaire);

        $scope.clicking = true;
        AnnonceLocation.update($scope.location, function(data) {
            MessageService.addMessage('Merci d\'avoir répondu au questionnaire !');
            $location.path('/');
        }, function(err) {
            $scope.error = true;
            $scope.errorMessage = err.data;
            $scope.clicking = false;
        });
    };
}
