package univ.rouen.camping.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.camping.service.ServiceService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring.xml" })
public class ServiceTest {
    @Autowired
    private ServiceService serviceService;

    @Test
    public void createTestFullServiceTest() {
        // GIVEN
        Service service = new Service("name");
        Long id = null;

        // WHEN
        try {
            id = serviceService.create(service);
        } catch (Exception e) {
            Assert.fail("Cannot create a full service");
        }

        // THEN
        Assert.assertEquals(service.getName(), "name");
    }
}
