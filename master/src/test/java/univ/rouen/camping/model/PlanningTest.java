package univ.rouen.camping.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.camping.service.PlanningService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring.xml" })
public class PlanningTest {
    @Autowired
    private PlanningService planningService;

    @Test
    public void createFullPlanningTest() throws ParseException {
        // GIVEN
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = simpleDateFormat.parse("09/03/2014");
        Date date2 = simpleDateFormat.parse("10/03/2014");
        Planning planning = new Planning(date1, date2);

        // WHEN
        Long id = planningService.create(planning);

        // THEN
        Assert.assertEquals(planningService.getById(id).getStart(), date1);
        Assert.assertEquals(planningService.getById(id).getEnd(), date2);
    }

    @Test
    public void calculSurDateTest() throws ParseException {
        // GIVEN
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateRef = simpleDateFormat.parse("11/03/2014");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateRef);

        // WHEN
        calendar.add(Calendar.DATE, -22);
        Date date1 = calendar.getTime();
        calendar.add(Calendar.DATE, 22);
        Date date2 = calendar.getTime();

        // THEN
        Assert.assertEquals(date1, simpleDateFormat.parse("17/02/2014"));
        Assert.assertEquals(date2, dateRef);
    }
}
