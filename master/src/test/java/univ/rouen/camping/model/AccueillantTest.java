package univ.rouen.camping.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.camping.service.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring.xml" })
public class AccueillantTest {
    @Autowired
    private AccueillantService accueillantService;

    @Test
    public void testCreateMain() throws Exception {
        // Given
        final String lastname = "lastname";
        final String firstname = "firstname";
        final String email = "email";
        final String phonenumber = "phonenumber";
        final String password = "password";
        final String pseudo = "pseudo";

        Accueillant accueillant = new Accueillant(lastname, firstname,
                email, phonenumber, password, pseudo);

        // When
        Long id = accueillantService.create(accueillant);

        // Then
        accueillant = accueillantService.getById(id);

        Assertions.assertThat(accueillant.getId()).isEqualTo(id);
        Assertions.assertThat(accueillant.getLastname()).isEqualTo(lastname);
        Assertions.assertThat(accueillant.getFirstname()).isEqualTo(firstname);
        Assertions.assertThat(accueillant.getEmail()).isEqualTo(email);
        Assertions.assertThat(accueillant.getPhonenumber()).isEqualTo(phonenumber);
        Assertions.assertThat(accueillant.getPseudo()).isEqualTo(pseudo);
    }
}
