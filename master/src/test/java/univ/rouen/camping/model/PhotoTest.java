package univ.rouen.camping.model;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.camping.service.PhotoService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring.xml" })
public class PhotoTest  {
    @Autowired
    private PhotoService photoService;

    @Test
    public void testCreateFullTest() throws URISyntaxException, IOException {
        // GIVEN
        Path path = Paths.get(getClass().getResource("/images/JS_prototype_chain.png").toURI());
        byte[] data = Files.readAllBytes(path);
        Photo photo = new Photo(Base64.encode(data));

        // WHEN
        Long id = photoService.create(photo);
        photo = photoService.getById(id);

        // THEN
        Assert.assertArrayEquals(Base64.decode(photo.getData()), data);
    }
}
