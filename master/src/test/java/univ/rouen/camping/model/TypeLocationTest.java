package univ.rouen.camping.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.camping.service.TypeLocationService;

import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring.xml" })
public class TypeLocationTest {
    @Autowired
    private TypeLocationService service;

    @Test
    public void equalTest() {
        // GIVEN
        TypeLocation typeLocation1 = new TypeLocation("type");
        TypeLocation typeLocation2 = new TypeLocation("type");

        // WHEN

        // THEN
        Assert.assertEquals(typeLocation1, typeLocation2);
    }

    @Test
    public void notEqualTest() {
        // GIVEN
        TypeLocation typeLocation1 = new TypeLocation("type1");
        TypeLocation typeLocation2 = new TypeLocation("type2");

        // WHEN

        // THEN
        Assert.assertNotEquals(typeLocation1, typeLocation2);
    }

    @Test
    public void BDDVideTest() {
        // GIVEN
        List<TypeLocation> types = service.getAll();

        // WHEN

        // THEN
        Assert.assertEquals(types.size(), 0);
    }

    @Test
    public void ajoutTypeTest() {
        // GIVEN
        TypeLocation typeLocation = new TypeLocation("type");

        // WHEN
        Long id = service.create(typeLocation);

        // THEN
        Assert.assertEquals(service.getAll().size(), 1);
        Assert.assertEquals(service.getById(id).getName(), "type");
    }

    @Test
    public void uniciteNomTest() {
        // GIVEN
        TypeLocation typeLocation1 = new TypeLocation("type");
        TypeLocation typeLocation2 = new TypeLocation("type");
        service.create(typeLocation1);

        // WHEN
        try {
            service.create(typeLocation2);
            Assert.fail("Test must fail because of unique constraint on name");
        } catch (Exception e) {
            // THEN OK
        }
    }

    @Test
    public void nonNullNomTest() {
        // GIVEN
        TypeLocation typeLocation = new TypeLocation(null);

        // WHEN
        try {
            service.create(typeLocation);
            Assert.fail("Test must fail because of not null constraint on name");
        } catch (Exception e) {
            // THEN OK
        }
    }

    @Test
    public void MAJNomTest() {
        // GIVEN
        TypeLocation typeLocation = new TypeLocation("type");
        Long id = service.create(typeLocation);

        // WHEN
        typeLocation = service.getById(id);
        typeLocation.setName("typeModif");
        service.update(typeLocation);

        // THEN
        Assert.assertEquals(service.getAll().size(), 1);
        Assert.assertEquals(typeLocation.getName(), "typeModif");
    }

    @After
    public void clearDatabase() {
        final List<TypeLocation> types = service.getAll();

        for (TypeLocation t : types) {
            service.delete(t.getId());
        }
    }
}
