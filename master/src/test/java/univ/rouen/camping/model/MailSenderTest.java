package univ.rouen.camping.model;

import org.junit.Assert;
import org.junit.Test;
import univ.rouen.camping.utils.MailSender;

import javax.mail.MessagingException;

public class MailSenderTest {
    @Test
    public void sendMessageTest() throws MessagingException {
        // GIVEN

        // WHEN
        try {
            MailSender.sendMessage("sujet test", "mon texte à moi", "jcaragog@gmail.co");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Impossible to send the mail");
        }

        // THEN OK
    }
}
