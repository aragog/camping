package univ.rouen.camping.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.camping.service.AdministrateurService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring.xml" })
public class AdministrateurTest {
    @Autowired
    private AdministrateurService administrateurService;

    @Test
    public void testCreateMain() throws Exception {
        // Given
        final String pseudo = "pseudo";
        final String password = "password";
        Administrateur administrateur = new Administrateur(pseudo, password);

        // When
        Long id = administrateurService.create(administrateur);

        // Then
        administrateur = administrateurService.getById(id);

        Assertions.assertThat(administrateur.getId()).isEqualTo(id);
        Assertions.assertThat(administrateur.getPseudo()).isEqualTo(pseudo);
    }
}
